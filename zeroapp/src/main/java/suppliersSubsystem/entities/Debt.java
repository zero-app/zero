package suppliersSubsystem.entities;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.time.LocalDate;

public class Debt {

    private IntegerProperty idProperty;
    private LocalDate lastEditDate;
    private DoubleProperty debtAmount;
    private DoubleProperty fineAmount;

    public Debt(int idProperty, LocalDate lastEditDate, double debtAmount, double fineAmount) {
        this.idProperty = new SimpleIntegerProperty(idProperty);
        this.lastEditDate = lastEditDate;
        this.debtAmount = new SimpleDoubleProperty(debtAmount);
        this.fineAmount = new SimpleDoubleProperty(fineAmount);
    }

    public int getIdProperty() {
        return idProperty.get();
    }

    public IntegerProperty idPropertyProperty() {
        return idProperty;
    }

    public LocalDate getLastEditDate() {
        return lastEditDate;
    }

    public double getDebtAmount() {
        return debtAmount.get();
    }

    public DoubleProperty debtAmountProperty() {
        return debtAmount;
    }

    public double getFineAmount() {
        return fineAmount.get();
    }

    public DoubleProperty fineAmountProperty() {
        return fineAmount;
    }

    public void setDebtAmount(double debtAmount) {
        this.debtAmount.set(debtAmount);
    }

    public void setFineAmount(double fineAmount) {
        this.fineAmount.set(fineAmount);
    }
}
