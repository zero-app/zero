package suppliersSubsystem.entities;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.time.LocalDate;

public class Turnover {

    private IntegerProperty idProperty;
    private DoubleProperty minTurnoverProperty;
    private DoubleProperty maxTurnoverProperty;
    private LocalDate lastEditedDate;

    public Turnover(int idProperty, double minTurnoverProperty, double maxTurnoverProperty, LocalDate lastEditedDate) {
        this.idProperty = new SimpleIntegerProperty(idProperty);
        this.minTurnoverProperty = new SimpleDoubleProperty(minTurnoverProperty);
        this.maxTurnoverProperty = new SimpleDoubleProperty(maxTurnoverProperty);
        this.lastEditedDate = lastEditedDate;
    }

    public int getIdProperty() {
        return idProperty.get();
    }

    public IntegerProperty idPropertyProperty() {
        return idProperty;
    }

    public double getMinTurnoverProperty() {
        return minTurnoverProperty.get();
    }

    public DoubleProperty minTurnoverPropertyProperty() {
        return minTurnoverProperty;
    }

    public double getMaxTurnoverProperty() {
        return maxTurnoverProperty.get();
    }

    public DoubleProperty maxTurnoverPropertyProperty() {
        return maxTurnoverProperty;
    }

    public LocalDate getLastEditedDate() {
        return lastEditedDate;
    }

    public void setMinTurnoverProperty(double minTurnoverProperty) {
        this.minTurnoverProperty.set(minTurnoverProperty);
    }

    public void setMaxTurnoverProperty(double maxTurnoverProperty) {
        this.maxTurnoverProperty.set(maxTurnoverProperty);
    }
}
