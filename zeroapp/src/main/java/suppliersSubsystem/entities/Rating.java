package suppliersSubsystem.entities;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Rating {

    private IntegerProperty idProperty;
    private IntegerProperty countOfPeopleWhoRated;
    private DoubleProperty rating;

    public Rating(int idProperty, int countOfPeopleWhoRated, double rating) {
        this.idProperty = new SimpleIntegerProperty(idProperty);
        this.countOfPeopleWhoRated = new SimpleIntegerProperty(countOfPeopleWhoRated);
        this.rating = new SimpleDoubleProperty(rating);
    }

    public int getIdProperty() {
        return idProperty.get();
    }

    public IntegerProperty idPropertyProperty() {
        return idProperty;
    }

    public int getCountOfPeopleWhoRated() {
        return countOfPeopleWhoRated.get();
    }

    public IntegerProperty countOfPeopleWhoRatedProperty() {
        return countOfPeopleWhoRated;
    }

    public double getRating() {
        return rating.get();
    }

    public DoubleProperty ratingProperty() {
        return rating;
    }

    public void setCountOfPeopleWhoRated(int countOfPeopleWhoRated) {
        this.countOfPeopleWhoRated.set(countOfPeopleWhoRated);
    }

    public void setRating(double rating) {
        this.rating.set(rating);
    }
}
