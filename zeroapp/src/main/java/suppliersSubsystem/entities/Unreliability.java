package suppliersSubsystem.entities;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Unreliability {

    private IntegerProperty id;
    private StringProperty unreliabilityLevel;

    public Unreliability(int id, String unreliabilityLevel) {
        this.id = new SimpleIntegerProperty(id);
        this.unreliabilityLevel = new SimpleStringProperty(unreliabilityLevel);
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getUnreliabilityLevel() {
        return unreliabilityLevel.get();
    }

    public StringProperty unreliabilityLevelProperty() {
        return unreliabilityLevel;
    }

    @Override
    public String toString() {
        return unreliabilityLevel.get();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Unreliability && ((Unreliability) obj).id.get() == this.id.get();
    }
}
