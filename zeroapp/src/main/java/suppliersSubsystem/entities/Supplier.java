package suppliersSubsystem.entities;

import buyersSub.objects.Contact;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Supplier {

    private StringProperty companyCode;
    private StringProperty VATCode;
    private StringProperty manager;
    private StringProperty name;
    private StringProperty description;
    private Contact companyContact;
    private StringProperty fax;
    private StringProperty website;
    private IntegerProperty employeeCount;
    private StringProperty insurance;
    private Turnover turnover;
    private StringProperty businessCategory;
    private Transport transport;
    private Rating rating;
    private Debt debt;
    private Currency currency;

    public Supplier(String companyCode, String VATCode, String manager, String name, String description, Contact companyContact,
                    String fax, String website, Integer employeeCount, String insurance, Turnover turnover, String businessCategory,
                    Transport transport, Rating rating, Debt debt, Currency currency) {
        this.companyCode = new SimpleStringProperty(companyCode);
        this.VATCode = new SimpleStringProperty(VATCode);
        this.manager = new SimpleStringProperty(manager);
        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(description);
        this.companyContact = companyContact;
        this.fax = new SimpleStringProperty(fax);
        this.website = new SimpleStringProperty(website);
        this.employeeCount = new SimpleIntegerProperty(employeeCount);
        this.insurance = new SimpleStringProperty(insurance);
        this.turnover = turnover;
        this.businessCategory = new SimpleStringProperty(businessCategory);
        this.transport = transport;
        this.rating = rating;
        this.debt = debt;
        this.currency = currency;
    }

    public String getCompanyCode() {
        return companyCode.get();
    }

    public StringProperty companyCodeProperty() {
        return companyCode;
    }

    public String getVATCode() {
        return VATCode.get();
    }

    public StringProperty VATCodeProperty() {
        return VATCode;
    }

    public String getManager() {
        return manager.get();
    }

    public StringProperty managerProperty() {
        return manager;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getDescription() {
        return description.get();
    }

    public Contact getCompanyContact() {
        return companyContact;
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public String getFax() {
        return fax.get();
    }

    public StringProperty faxProperty() {
        return fax;
    }

    public String getWebsite() {
        return website.get();
    }

    public StringProperty websiteProperty() {
        return website;
    }

    public int getEmployeeCount() {
        return employeeCount.get();
    }

    public IntegerProperty employeeCountProperty() {
        return employeeCount;
    }

    public String getInsurance() {
        return insurance.get();
    }

    public StringProperty insuranceProperty() {
        return insurance;
    }

    public Turnover getTurnover() {
        return turnover;
    }

    public String getBusinessCategory() {
        return businessCategory.get();
    }

    public StringProperty businessCategoryProperty() {
        return businessCategory;
    }

    public Transport getTransport() {
        return transport;
    }

    public Rating getRating() {
        return rating;
    }

    public Debt getDebt() {
        return debt;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setVATCode(String VATCode) {
        this.VATCode.set(VATCode);
    }

    public void setManager(String manager) {
        this.manager.set(manager);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public void setFax(String fax) {
        this.fax.set(fax);
    }

    public void setWebsite(String website) {
        this.website.set(website);
    }

    public void setEmployeeCount(int employeeCount) {
        this.employeeCount.set(employeeCount);
    }

    public void setInsurance(String insurance) {
        this.insurance.set(insurance);
    }

    public void setBusinessCategory(String businessCategory) {
        this.businessCategory.set(businessCategory);
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
