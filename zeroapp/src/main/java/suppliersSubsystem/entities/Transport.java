package suppliersSubsystem.entities;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.time.LocalDate;

public class Transport {

    private IntegerProperty idProperty;
    private LocalDate lastEditDate;
    private IntegerProperty totalNumberOfCars;
    private IntegerProperty ownedCars;
    private IntegerProperty rentedCars;

    public Transport(int idProperty, LocalDate lastEditDate, int totalNumberOfCars,
                     int ownedCars, int rentedCars) {
        this.idProperty = new SimpleIntegerProperty(idProperty);
        this.lastEditDate = lastEditDate;
        this.totalNumberOfCars = new SimpleIntegerProperty(totalNumberOfCars);
        this.ownedCars = new SimpleIntegerProperty(ownedCars);
        this.rentedCars = new SimpleIntegerProperty(rentedCars);
    }

    public int getIdProperty() {
        return idProperty.get();
    }

    public IntegerProperty idPropertyProperty() {
        return idProperty;
    }

    public LocalDate getLastEditDate() {
        return lastEditDate;
    }

    public int getTotalNumberOfCars() {
        return totalNumberOfCars.get();
    }

    public IntegerProperty totalNumberOfCarsProperty() {
        return totalNumberOfCars;
    }

    public int getOwnedCars() {
        return ownedCars.get();
    }

    public IntegerProperty ownedCarsProperty() {
        return ownedCars;
    }

    public int getRentedCars() {
        return rentedCars.get();
    }

    public IntegerProperty rentedCarsProperty() {
        return rentedCars;
    }

    public void setOwnedCars(int ownedCars) {
        this.ownedCars.set(ownedCars);
    }

    public void setRentedCars(int rentedCars) {
        this.rentedCars.set(rentedCars);
    }
}
