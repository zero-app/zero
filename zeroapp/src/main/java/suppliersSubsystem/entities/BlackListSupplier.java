package suppliersSubsystem.entities;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

public class BlackListSupplier {

    private StringProperty companyCode;
    private StringProperty blacklistedCompanyName;
    private StringProperty reasonForBlacklisting;
    private StringProperty dateWhenAdded;
    private LocalDate dateWhenSupplierWasAdded;
    private Unreliability unreliability;

    public BlackListSupplier(String companyCode, String blacklistedCompanyName, String reasonForBlacklisting,
                             LocalDate dateWhenAdded, Unreliability unreliability) {
        this.companyCode = new SimpleStringProperty(companyCode);
        this.blacklistedCompanyName = new SimpleStringProperty(blacklistedCompanyName);
        this.reasonForBlacklisting = new SimpleStringProperty(reasonForBlacklisting);
        this.dateWhenSupplierWasAdded = dateWhenAdded;
        this.dateWhenAdded = new SimpleStringProperty(dateWhenAdded.toString());
        this.unreliability = unreliability;
    }

    public BlackListSupplier(Supplier supplier) {
        this.companyCode = supplier.companyCodeProperty();
        this.blacklistedCompanyName = supplier.nameProperty();
        this.reasonForBlacklisting = new SimpleStringProperty();
    }

    public String getCompanyCode() {
        return companyCode.get();
    }

    public StringProperty companyCodeProperty() {
        return companyCode;
    }

    public String getBlacklistedCompanyName() {
        return blacklistedCompanyName.get();
    }

    public StringProperty blacklistedCompanyNameProperty() {
        return blacklistedCompanyName;
    }

    public String getReasonForBlacklisting() {
        return reasonForBlacklisting.get();
    }

    public StringProperty reasonForBlacklistingProperty() {
        return reasonForBlacklisting;
    }

    public StringProperty dateWhenAddedProperty() {
        return dateWhenAdded;
    }

    public Unreliability getUnreliability() {
        return unreliability;
    }

    public LocalDate getDateWhenSupplierWasAdded() {
        return dateWhenSupplierWasAdded;
    }

    public void setBlacklistedCompanyName(String blacklistedCompanyName) {
        this.blacklistedCompanyName.set(blacklistedCompanyName);
    }

    public void setReasonForBlacklisting(String reasonForBlacklisting) {
        this.reasonForBlacklisting.set(reasonForBlacklisting);
    }

    public void setUnreliability(Unreliability unreliability) {
        this.unreliability = unreliability;
    }
}
