package suppliersSubsystem.entities;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Currency {

    private IntegerProperty idProperty;
    private StringProperty currencyCodeProperty;

    public Currency(int idProperty, String currencyCodeProperty) {
        this.idProperty = new SimpleIntegerProperty(idProperty);
        this.currencyCodeProperty = new SimpleStringProperty(currencyCodeProperty);
    }

    public int getIdProperty() {
        return idProperty.get();
    }

    public IntegerProperty idPropertyProperty() {
        return idProperty;
    }

    public String getCurrencyCodeProperty() {
        return currencyCodeProperty.get();
    }

    public StringProperty currencyCodePropertyProperty() {
        return currencyCodeProperty;
    }

    @Override
    public String toString() {
        return "Currency code: " + currencyCodeProperty.get();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Currency && this.idProperty.get() == ((Currency) obj).idProperty.get();
    }
}
