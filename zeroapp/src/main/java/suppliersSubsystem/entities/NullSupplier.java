package suppliersSubsystem.entities;


import buyersSub.objects.Contact;

import java.time.LocalDate;

public class NullSupplier extends Supplier {

    public NullSupplier() {
        super("123456798", "LT000000000", "Name Surname", "Company",
                "Describe the company", new Contact("+370 999 12345", "email@gmail.com"),
                "+370 999 99999", "Website.com", 123, "789123",
                new Turnover(0, 0d, 0d, LocalDate.now()), "Cars",
                new Transport(0, LocalDate.now(), 3, 2, 1),
                new Rating(0, 10, 9.5),
                new Debt(0, LocalDate.now(), 1500, 100.99),
                new Currency(1, "EUR"));
    }
}
