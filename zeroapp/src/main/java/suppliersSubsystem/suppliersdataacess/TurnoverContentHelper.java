package suppliersSubsystem.suppliersdataacess;

import suppliersSubsystem.entities.Turnover;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class TurnoverContentHelper {

    private static final String COMMA = ",";
    private static final String QUOTE = "'";
    private static final String ID_COLUMN = "id_apyvarta";
    private static final String MIN_TURNOVER_COLUMN = "apyvartaNuo";
    private static final String MAX_TURNOVER_COLUMN = "apyvartaIki";
    private static final String LAST_EDIT_DATE_COLUMN = "paskutiniKartaAtnaujintaApyvarta";

    private TurnoverContentHelper() {

    }

    static Turnover parseTurnoverFromResultSet(ResultSet resultSet) throws SQLException {
        return new Turnover(resultSet.getInt(ID_COLUMN), resultSet.getDouble(MIN_TURNOVER_COLUMN),
                resultSet.getDouble(MAX_TURNOVER_COLUMN), resultSet.getDate(LAST_EDIT_DATE_COLUMN).toLocalDate());
    }

    static long insertNewTurnover(Turnover turnover) throws SQLException, ClassNotFoundException {
        String insertStatement = "INSERT INTO `Apyvartos`(`apyvartaNuo`, `apyvartaIki`, `paskutiniKartaAtnaujintaApyvarta`) VALUES (" +
                turnover.getMinTurnoverProperty() + COMMA + turnover.getMaxTurnoverProperty() + COMMA +
                QUOTE + turnover.getLastEditedDate() + QUOTE + ")";

        return Database.executeUpdateReturnKey(insertStatement);
    }

    static void updateTurnoverData(Turnover turnover) throws SQLException, ClassNotFoundException {
        String updateStatement = "UPDATE `Apyvartos` SET `apyvartaNuo`=" + turnover.getMinTurnoverProperty() + COMMA +
                "`apyvartaIki`=" + turnover.getMaxTurnoverProperty() + COMMA +
                "`paskutiniKartaAtnaujintaApyvarta`=" + QUOTE + LocalDate.now() + QUOTE +
                " WHERE `id_apyvarta`=" + turnover.getIdProperty();

        Database.executeUpdate(updateStatement);
    }

    static void removeTurnover(Turnover turnover) throws SQLException, ClassNotFoundException {
        String removeTurnover = "DELETE FROM `Apyvartos` WHERE `id_apyvarta`=" + turnover.getIdProperty();

        Database.executeUpdate(removeTurnover);
    }
}
