package suppliersSubsystem.suppliersdataacess;

import suppliersSubsystem.entities.Rating;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RatingContentHelper {

    private static final String COMMA = ",";
    private static final String ID_COLUMN = "id_reitingas";
    private static final String RATED_PEOPLE_COUNT_COLUMN = "ivertinusiuZmoniuKiekis";
    private static final String RATING_COLUMN = "reitingas";

    private RatingContentHelper() {

    }

    static Rating parseRatingFromResultSet(ResultSet resultSet) throws SQLException {
        return new Rating(resultSet.getInt(ID_COLUMN), resultSet.getInt(RATED_PEOPLE_COUNT_COLUMN),
                resultSet.getDouble(RATING_COLUMN));
    }

    static long insertNewRating(Rating rating) throws SQLException, ClassNotFoundException {
        String insertStatement = "INSERT INTO `Reitingai`(`ivertinusiuZmoniuKiekis`, `reitingas`) VALUES (" +
                rating.getCountOfPeopleWhoRated() + COMMA + rating.getRating() + ")";

        return Database.executeUpdateReturnKey(insertStatement);
    }

    static void updateRatingData(Rating rating) throws SQLException, ClassNotFoundException {
        String updateStatement = "UPDATE `Reitingai` SET " +
                "`ivertinusiuZmoniuKiekis`=" + rating.getCountOfPeopleWhoRated() + COMMA +
                "`reitingas`=" + rating.getRating() +
                " WHERE `id_reitingas`=" + rating.getIdProperty();

        Database.executeUpdate(updateStatement);
    }

    static void removeRating(Rating rating) throws SQLException, ClassNotFoundException {
        String removeRating = "DELETE FROM `Reitingai` WHERE `id_reitingas`=" + rating.getIdProperty();

        Database.executeUpdate(removeRating);
    }
}
