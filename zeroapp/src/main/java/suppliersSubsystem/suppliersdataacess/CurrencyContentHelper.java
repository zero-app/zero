package suppliersSubsystem.suppliersdataacess;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import suppliersSubsystem.entities.Currency;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CurrencyContentHelper {

    private static final String ID_COLUMN = "id_valiuta";
    private static final String CURRENCY_CODE_COLUMN = "valiutosPavadinimas";


    private CurrencyContentHelper() {

    }

    public static ObservableList<Currency> getAllAvailableCurrencies() {
        try {
            ResultSet resultSet = Database.executeQuery("SELECT * FROM `Valiutos` WHERE 1");

            return getAllAvailableCurrencies(resultSet);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static ObservableList<Currency> getAllAvailableCurrencies(ResultSet resultSet) throws SQLException {
        ObservableList<Currency> allAvailableCurrencies = FXCollections.observableArrayList();

        while (resultSet.next()) {
            allAvailableCurrencies.add(parseCurrencyFromResultSet(resultSet));
        }

        return allAvailableCurrencies;
    }

    static Currency parseCurrencyFromResultSet(ResultSet resultSet) throws SQLException {
        return new Currency(resultSet.getInt(ID_COLUMN), resultSet.getString(CURRENCY_CODE_COLUMN));
    }
}
