package suppliersSubsystem.suppliersdataacess;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import suppliersSubsystem.entities.Unreliability;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UnreliabilityContentHelper {

    private static final String ID_COLUMN = "id_vengiamumas";
    private static final String NAME_COLUMN = "vengiamumas";

    private UnreliabilityContentHelper() {
    }

    public static ObservableList<Unreliability> getAllAvailableUnreliabiltyLevels() {
        try {
            ResultSet resultSet = Database.executeQuery("SELECT * FROM `Vengiamumai` WHERE 1");

            return getAllAvailableUreliabilities(resultSet);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static ObservableList<Unreliability> getAllAvailableUreliabilities(ResultSet resultSet) throws SQLException {
        ObservableList<Unreliability> allAvailableLevels = FXCollections.observableArrayList();

        while (resultSet.next()) {
            allAvailableLevels.add(parseUnreliabilityFromResultSet(resultSet));
        }

        return allAvailableLevels;
    }

    static Unreliability parseUnreliabilityFromResultSet(ResultSet resultSet) throws SQLException {
        return new Unreliability(resultSet.getInt(ID_COLUMN), resultSet.getString(NAME_COLUMN));
    }
}
