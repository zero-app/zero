package suppliersSubsystem.suppliersdataacess;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import suppliersSubsystem.entities.BlackListSupplier;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class BlacklistedSuppliersContentHelper {

    private static final String COMMA = ",";
    private static final String QUOTE = "'";
    private static final String ID_COLUMN = "registracijosKodas";
    private static final String NAME_COLUMN = "pavadinimas";
    private static final String REASON_COLUMN = "priezastis";
    private static final String DATE_ADDED_COLUMN = "itraukimoData";

    private BlacklistedSuppliersContentHelper() {

    }

    public static ObservableList<BlackListSupplier> getAllAvailableBlacklistedSuppliers() {
        try {
            ResultSet allSuppliers = Database.executeQuery("SELECT * FROM `Juodojo_saraso_tiekejai`, `Vengiamumai` " +
                    "WHERE `fk_vengiamumas`=`id_vengiamumas`");

            return getAllAvailableSuppliers(allSuppliers);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void updateBlackListSupplierData(BlackListSupplier supplier) {
        try {
            String updateSupplier = "UPDATE `Juodojo_saraso_tiekejai` SET " +
                    "`pavadinimas`=" + QUOTE + supplier.getBlacklistedCompanyName() + QUOTE + COMMA +
                    "`priezastis`=" + QUOTE + supplier.getReasonForBlacklisting() + QUOTE + COMMA +
                    "`fk_vengiamumas`=" + supplier.getUnreliability().getId() +
                    " WHERE `registracijosKodas`=" + supplier.getCompanyCode();

            Database.executeUpdate(updateSupplier);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void removeBlackListSupplier(BlackListSupplier supplier) {
        try {
            String removeStatement = "DELETE FROM `Juodojo_saraso_tiekejai` " +
                    "WHERE `registracijosKodas`=" + supplier.getCompanyCode();

            Database.executeUpdate(removeStatement);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void insertNewBlackListSupplier(BlackListSupplier supplier) {
        try {
            String insertStatement = "INSERT INTO `Juodojo_saraso_tiekejai`(`registracijosKodas`, `pavadinimas`, " +
                    "`priezastis`, `itraukimoData`, `fk_vengiamumas`) VALUES (" +
                    supplier.getCompanyCode() + COMMA +
                    QUOTE + supplier.getBlacklistedCompanyName() + QUOTE + COMMA +
                    QUOTE + supplier.getReasonForBlacklisting() + QUOTE + COMMA +
                    QUOTE + LocalDate.now().toString() + QUOTE + COMMA +
                    supplier.getUnreliability().getId() + ")";

            Database.executeUpdate(insertStatement);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static ObservableList<BlackListSupplier> getAllAvailableSuppliers(ResultSet allSuppliers) throws SQLException {
        ObservableList<BlackListSupplier> allAvailableSuppliers = FXCollections.observableArrayList();

        while (allSuppliers.next()) {
            allAvailableSuppliers.add(parseBlacklistedSupplierFromResultSet(allSuppliers));
        }

        return allAvailableSuppliers;
    }

    private static BlackListSupplier parseBlacklistedSupplierFromResultSet(ResultSet resultSet) throws SQLException {
        return new BlackListSupplier(resultSet.getString(ID_COLUMN), resultSet.getString(NAME_COLUMN),
                resultSet.getString(REASON_COLUMN), resultSet.getDate(DATE_ADDED_COLUMN).toLocalDate(),
                UnreliabilityContentHelper.parseUnreliabilityFromResultSet(resultSet));
    }
}
