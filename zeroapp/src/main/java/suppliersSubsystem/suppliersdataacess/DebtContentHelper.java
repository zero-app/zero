package suppliersSubsystem.suppliersdataacess;

import suppliersSubsystem.entities.Debt;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class DebtContentHelper {

    private static final String COMMA = ",";
    private static final String QUOTE = "'";
    private static final String ID_COLUMN = "id_skola";
    private static final String LAST_EDIT_DATE_COLUMN = "paskutiniKartaAtnaujintaSkola";
    private static final String DEBT_AMOUNT_COLUMN = "skolosSodraiDydis";
    private static final String FINE_AMOUNT_COLUMN = "baudosDydis";

    private DebtContentHelper() {

    }

    static Debt parseDebtFromResultSet(ResultSet resultSet) throws SQLException {
        return new Debt(resultSet.getInt(ID_COLUMN), resultSet.getDate(LAST_EDIT_DATE_COLUMN).toLocalDate(),
                resultSet.getDouble(DEBT_AMOUNT_COLUMN), resultSet.getDouble(FINE_AMOUNT_COLUMN));
    }

    static long insertNewDebtData(Debt debt) throws SQLException, ClassNotFoundException {
        String insertStatement = "INSERT INTO `Skolos`(`paskutiniKartaAtnaujintaSkola`, `skolosSodraiDydis`, `baudosDydis`) VALUES (" +
                QUOTE + debt.getLastEditDate() + QUOTE + COMMA + debt.getDebtAmount() + COMMA + debt.getFineAmount() + ")";

        return Database.executeUpdateReturnKey(insertStatement);
    }

    static void updateDebtData(Debt debt) throws SQLException, ClassNotFoundException {
        String updateStatement = "UPDATE `Skolos` SET " +
                "`paskutiniKartaAtnaujintaSkola`=" + QUOTE + LocalDate.now() + QUOTE + COMMA +
                "`skolosSodraiDydis`=" + debt.getDebtAmount() + COMMA +
                "`baudosDydis`=" + debt.getFineAmount() +
                " WHERE `id_skola`=" + debt.getIdProperty();

        Database.executeUpdate(updateStatement);
    }

    static void removeDebt(Debt debt) throws SQLException, ClassNotFoundException {
        String removeDebt = "DELETE FROM `Skolos` WHERE `id_skola`=" + debt.getIdProperty();

        Database.executeUpdate(removeDebt);
    }
}
