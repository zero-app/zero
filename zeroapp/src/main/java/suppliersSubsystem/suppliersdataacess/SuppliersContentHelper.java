package suppliersSubsystem.suppliersdataacess;

import buyersSub.objects.Contact;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import suppliersSubsystem.entities.NullSupplier;
import suppliersSubsystem.entities.Supplier;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SuppliersContentHelper {

    private static final String COMMA = ",";
    private static final String QUOTE = "'";
    private static final String COMPANY_CODE_COLUMN = "registracijosKodas";
    private static final String VAT_CODE_COLUMN = "VATKodas";
    private static final String MANAGER_COLUMN = "vadovas";
    private static final String NAME_COLUMN = "pavadinimas";
    private static final String DESCRIPTION_COLUMN = "imonesAprasymas";
    private static final String FAX_COLUMN = "faksas";
    private static final String WEBSITE_COLUMN = "svetaine";
    private static final String EMPLOYEE_COUNT_COLUMN = "darbuotojuKiekis";
    private static final String INSURER_COLUMN = "draudejoKodas";
    private static final String BUSINESS_CATEGORY_COLUMN = "veiklosSritis";
    private static final String CONTACT_ID_COLUMN = "id_Kontaktas";
    private static final String CONTACT_PHONE_COLUMN = "telefonas";
    private static final String CONTACT_EMAIL_COLUMN = "epastas";

    private SuppliersContentHelper() {

    }

    public static boolean doesSupplierWithTheSameCodeExist(String companyCode) {
        String statement = "SELECT * FROM `Tiekejai` WHERE `registracijosKodas`=" + companyCode;

        try {
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean doesSupplierBelongToTheBlackList(String companyCode) {
        String statement = "SELECT * FROM `Juodojo_saraso_tiekejai` WHERE `registracijosKodas`=" + companyCode;

        try {
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void insertNewSupplier(Supplier supplier) {
        try {
            Contact companyContact = supplier.getCompanyContact();
            String insertContactStatement = "INSERT INTO `Kontaktai`(`telefonas`, `epastas`) VALUES (" +
                    QUOTE + companyContact.getPhoneNumber() + QUOTE + COMMA +
                    QUOTE + companyContact.getEmail() + QUOTE + ")";
            long contactId = Database.executeUpdateReturnKey(insertContactStatement);
            long turnoverId = TurnoverContentHelper.insertNewTurnover(supplier.getTurnover());
            long transportId = TransportContentHelper.insertNewTransportData(supplier.getTransport());
            long ratingId = RatingContentHelper.insertNewRating(supplier.getRating());
            long debtId = DebtContentHelper.insertNewDebtData(supplier.getDebt());

            String insertStatement = "INSERT INTO `Tiekejai`(`registracijosKodas`, `VATKodas`, `vadovas`, `pavadinimas`, " +
                    "`imonesAprasymas`, `fk_kontaktas`, `faksas`, `svetaine`, `darbuotojuKiekis`, `draudejoKodas`, " +
                    "`fk_apyvarta`, `veiklosSritis`, `fk_transportas`, `fk_reitingas`, `fk_skola`, `fk_valiuta`) " +
                    "VALUES (" + supplier.getCompanyCode() + COMMA + QUOTE + supplier.getVATCode() + QUOTE + COMMA +
                    QUOTE + supplier.getManager() + QUOTE + COMMA + QUOTE + supplier.getName() + QUOTE + COMMA +
                    QUOTE + supplier.getDescription() + QUOTE + COMMA + contactId + COMMA + QUOTE + supplier.getFax() + QUOTE + COMMA +
                    QUOTE + supplier.getWebsite() + QUOTE + COMMA + supplier.getEmployeeCount() + COMMA + supplier.getInsurance() + COMMA +
                    turnoverId + COMMA + QUOTE + supplier.getBusinessCategory() + QUOTE + COMMA + transportId + COMMA +
                    ratingId + COMMA + debtId + COMMA + supplier.getCurrency().getIdProperty() + ")";

            Database.executeUpdate(insertStatement);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void updateSupplierData(Supplier supplier) {
        try {
            Contact contact = supplier.getCompanyContact();
            String updateContacts = "UPDATE `Kontaktai` SET `telefonas`=" + QUOTE + contact.getPhoneNumber() + QUOTE + COMMA +
                    "`epastas`=" + QUOTE + contact.getEmail() + QUOTE + " WHERE `id_Kontaktas`=" + contact.getId();
            String updateSupplier = "UPDATE `Tiekejai` SET `VATKodas`=" + QUOTE + supplier.getVATCode() + QUOTE + COMMA +
                    "`vadovas`=" + QUOTE + supplier.getManager() + QUOTE + COMMA +
                    "`pavadinimas`=" + QUOTE + supplier.getName() + QUOTE + COMMA +
                    "`imonesAprasymas`=" + QUOTE + supplier.getDescription() + QUOTE + COMMA +
                    "`faksas`=" + QUOTE + supplier.getFax() + QUOTE + COMMA +
                    "`svetaine`=" + QUOTE + supplier.getWebsite() + QUOTE + COMMA +
                    "`darbuotojuKiekis`=" + supplier.getEmployeeCount() + COMMA +
                    "`draudejoKodas`=" + QUOTE + supplier.getInsurance() + QUOTE + COMMA +
                    "`veiklosSritis`=" + QUOTE + supplier.getBusinessCategory() + QUOTE + COMMA +
                    "`fk_valiuta`=" + supplier.getCurrency().getIdProperty() +
                    " WHERE `registracijosKodas`=" + supplier.getCompanyCode();

            Database.executeUpdate(updateContacts);
            TurnoverContentHelper.updateTurnoverData(supplier.getTurnover());
            TransportContentHelper.updateTransportData(supplier.getTransport());
            RatingContentHelper.updateRatingData(supplier.getRating());
            DebtContentHelper.updateDebtData(supplier.getDebt());
            Database.executeUpdate(updateSupplier);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static ObservableList<Supplier> getAllAvailableSuppliers() {
        try {
            ResultSet allSuppliers = Database.executeQuery("SELECT * FROM " +
                    "`Kontaktai`, `Tiekejai`, `Apyvartos`, `Valiutos`, `Transportas`, `Reitingai`, `Skolos` " +
                    "WHERE `fk_kontaktas`=`id_Kontaktas` AND " +
                    "`fk_apyvarta`=`id_apyvarta` AND " +
                    "`fk_transportas`=`id_transportas` AND " +
                    "`fk_reitingas`=`id_reitingas` AND " +
                    "`fk_skola`=`id_skola` AND " +
                    "`fk_valiuta`=`id_valiuta`");

            return getAllAvailableSuppliers(allSuppliers);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Supplier getAvailableSupplier(String companyCode) {
        try {
            String selectSupplier = "SELECT * FROM " +
                    "`Kontaktai`, `Tiekejai`, `Apyvartos`, `Valiutos`, `Transportas`, `Reitingai`, `Skolos` " +
                    "WHERE `registracijosKodas`=" + QUOTE + companyCode + QUOTE + "AND " +
                    "`fk_kontaktas`=`id_Kontaktas` AND " +
                    "`fk_apyvarta`=`id_apyvarta` AND " +
                    "`fk_transportas`=`id_transportas` AND " +
                    "`fk_reitingas`=`id_reitingas` AND " +
                    "`fk_skola`=`id_skola` AND " +
                    "`fk_valiuta`=`id_valiuta`";
            ResultSet result = Database.executeQuery(selectSupplier);

            return result.next() ? parseSupplierFromResultSet(result) : new NullSupplier();
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void removeSupplier(Supplier supplier) {
        try {
            String removeContact = "DELETE FROM `Kontaktai` WHERE `id_Kontaktas`=" + supplier.getCompanyContact().getId();

            Database.executeUpdate(removeContact);
            TurnoverContentHelper.removeTurnover(supplier.getTurnover());
            TransportContentHelper.removeTransport(supplier.getTransport());
            RatingContentHelper.removeRating(supplier.getRating());
            DebtContentHelper.removeDebt(supplier.getDebt());
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static ObservableList<Supplier> getAllAvailableSuppliers(ResultSet allSuppliersSet) throws SQLException {
        ObservableList<Supplier> allAvailableSuppliers = FXCollections.observableArrayList();

        while (allSuppliersSet.next()) {
            allAvailableSuppliers.add(parseSupplierFromResultSet(allSuppliersSet));
        }

        return allAvailableSuppliers;
    }

    private static Supplier parseSupplierFromResultSet(ResultSet result) throws SQLException {
        return new Supplier(result.getString(COMPANY_CODE_COLUMN), result.getString(VAT_CODE_COLUMN), result.getString(MANAGER_COLUMN),
                result.getString(NAME_COLUMN), result.getString(DESCRIPTION_COLUMN), parseContactFromResultSet(result),
                result.getString(FAX_COLUMN), result.getString(WEBSITE_COLUMN), result.getInt(EMPLOYEE_COUNT_COLUMN),
                result.getString(INSURER_COLUMN), TurnoverContentHelper.parseTurnoverFromResultSet(result),
                result.getString(BUSINESS_CATEGORY_COLUMN), TransportContentHelper.parseTransportFromResultSet(result),
                RatingContentHelper.parseRatingFromResultSet(result), DebtContentHelper.parseDebtFromResultSet(result),
                CurrencyContentHelper.parseCurrencyFromResultSet(result));
    }

    private static Contact parseContactFromResultSet(ResultSet resultSet) throws SQLException {
        return new Contact(resultSet.getInt(CONTACT_ID_COLUMN), resultSet.getString(CONTACT_PHONE_COLUMN),
                resultSet.getString(CONTACT_EMAIL_COLUMN));
    }
}
