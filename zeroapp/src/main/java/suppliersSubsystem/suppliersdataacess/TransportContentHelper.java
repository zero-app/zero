package suppliersSubsystem.suppliersdataacess;

import suppliersSubsystem.entities.Transport;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class TransportContentHelper {

    private static final String COMMA = ",";
    private static final String QUOTE = "'";
    private static final String ID_COLUMN = "id_transportas";
    private static final String LAST_EDIT_DATE_COLUMN = "paskutiniKartaAtnaujintasTransportas";
    private static final String TOTAL_CARS_COLUMN = "visoAutomobiliu";
    private static final String OWNED_CARS_COLUMN = "nuosaviAutomobiliai";
    private static final String RENTED_CARS_COLUMN = "nuomojamiAutomobiliai";

    private TransportContentHelper() {

    }

    static Transport parseTransportFromResultSet(ResultSet resultSet) throws SQLException {
        return new Transport(resultSet.getInt(ID_COLUMN), resultSet.getDate(LAST_EDIT_DATE_COLUMN).toLocalDate(),
                resultSet.getInt(TOTAL_CARS_COLUMN), resultSet.getInt(OWNED_CARS_COLUMN), resultSet.getInt(RENTED_CARS_COLUMN));
    }

    static long insertNewTransportData(Transport transport) throws SQLException, ClassNotFoundException {
        String insertStatement = "INSERT INTO `Transportas`(`paskutiniKartaAtnaujintasTransportas`, `visoAutomobiliu`, " +
                "`nuosaviAutomobiliai`, `nuomojamiAutomobiliai`) VALUES (" +
                QUOTE + transport.getLastEditDate() + QUOTE + COMMA + transport.getTotalNumberOfCars() + COMMA +
                transport.getOwnedCars() + COMMA + transport.getRentedCars() + ")";

        return Database.executeUpdateReturnKey(insertStatement);
    }

    static void updateTransportData(Transport transport) throws SQLException, ClassNotFoundException {
        int owned = transport.getOwnedCars();
        int rented = transport.getRentedCars();
        int total = owned + rented;
        String updateStatement = "UPDATE `Transportas` SET " +
                "`paskutiniKartaAtnaujintasTransportas`=" + QUOTE + LocalDate.now() + QUOTE + COMMA +
                "`visoAutomobiliu`=" + total + COMMA +
                "`nuosaviAutomobiliai`=" + owned + COMMA +
                "`nuomojamiAutomobiliai`=" + rented +
                " WHERE `id_transportas`=" + transport.getIdProperty();

        Database.executeUpdate(updateStatement);
    }

    static void removeTransport(Transport transport) throws SQLException, ClassNotFoundException {
        String removeTransport = "DELETE FROM `Transportas` WHERE `id_transportas`=" + transport.getIdProperty();

        Database.executeUpdate(removeTransport);
    }
}
