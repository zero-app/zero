package suppliersSubsystem.controllers;

import buyersSub.objects.Contact;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sharedControllers.ContentSwitcher;
import suppliersSubsystem.entities.*;
import suppliersSubsystem.suppliersdataacess.SuppliersContentHelper;
import utils.AlertUtil;

public class ViewSuppliersController {

    public TableView<Supplier> viewSuppliersTable;
    public TableColumn<Supplier, String> companyCode;
    public TableColumn<Supplier, String> VATCodeColumn;
    public TableColumn<Supplier, String> managerColumn;
    public TableColumn<Supplier, String> nameColumn;
    public TableColumn<Supplier, String> descriptionColumn;
    public TableColumn<Supplier, String> faxColumn;
    public TableColumn<Supplier, String> websiteColumn;
    public TableColumn<Supplier, Integer> employeeCountColumn;
    public TableColumn<Supplier, String> insuranceColumn;
    public TableColumn<Supplier, String> businessCategoryColumn;
    public Label phoneNumberLabel;
    public Label emailLabel;
    public Label minTurnoverLabel;
    public Label maxTurnoverLabel;
    public Label turnoverLastEditedLabel;
    public Label ownedVehiclesLabel;
    public Label rentedVehiclesLabel;
    public Label totalVehiclesLabel;
    public Label vehiclesLastEditedLabel;
    public Label votesLabel;
    public Label ratingLabel;
    public Label socialDebtLabel;
    public Label fineLabel;
    private ContentSwitcher contentSwitcher;

    private class SupplierLabelDataUpdater implements ChangeListener<Supplier> {

        @Override
        public void changed(ObservableValue<? extends Supplier> observable,
                            Supplier oldValue, Supplier newValue) {
            Supplier supplier = observable.getValue();

            if (supplier == null) {
                return;
            }

            Contact contact = supplier.getCompanyContact();
            Turnover turnover = supplier.getTurnover();
            Transport transport = supplier.getTransport();
            Rating rating = supplier.getRating();
            Debt debt = supplier.getDebt();
            String currencyCode = " " + supplier.getCurrency().getCurrencyCodeProperty();

            phoneNumberLabel.setText(contact.getPhoneNumber());
            emailLabel.setText(contact.getEmail());
            minTurnoverLabel.setText(turnover.getMinTurnoverProperty() + currencyCode);
            maxTurnoverLabel.setText(turnover.getMaxTurnoverProperty() + currencyCode);
            turnoverLastEditedLabel.setText(turnover.getLastEditedDate().toString());
            ownedVehiclesLabel.setText(String.valueOf(transport.getOwnedCars()));
            rentedVehiclesLabel.setText(String.valueOf(transport.getRentedCars()));
            totalVehiclesLabel.setText(String.valueOf(transport.getTotalNumberOfCars()));
            vehiclesLastEditedLabel.setText(transport.getLastEditDate().toString());
            votesLabel.setText(String.valueOf(rating.getCountOfPeopleWhoRated()));
            ratingLabel.setText(String.valueOf(rating.getRating()));
            socialDebtLabel.setText(debt.getDebtAmount() + currencyCode);
            fineLabel.setText(debt.getFineAmount() + currencyCode);
        }

    }

    @FXML
    private void initialize() {
        contentSwitcher = new ContentSwitcher();

        companyCode.setCellValueFactory(callBack -> callBack.getValue().companyCodeProperty());
        VATCodeColumn.setCellValueFactory(callBack -> callBack.getValue().VATCodeProperty());
        managerColumn.setCellValueFactory(callBack -> callBack.getValue().managerProperty());
        nameColumn.setCellValueFactory(callBack -> callBack.getValue().nameProperty());
        descriptionColumn.setCellValueFactory(callBack -> callBack.getValue().descriptionProperty());
        faxColumn.setCellValueFactory(callBack -> callBack.getValue().faxProperty());
        websiteColumn.setCellValueFactory(callBack -> callBack.getValue().websiteProperty());
        employeeCountColumn.setCellValueFactory(callBack -> callBack.getValue().employeeCountProperty().asObject());
        insuranceColumn.setCellValueFactory(callBack -> callBack.getValue().insuranceProperty());
        businessCategoryColumn.setCellValueFactory(callBack -> callBack.getValue().businessCategoryProperty());
        viewSuppliersTable.getSelectionModel().selectedItemProperty().addListener(new SupplierLabelDataUpdater());
        populateSuppliersTable();
    }

    private void populateSuppliersTable() {
        try {
            viewSuppliersTable.setItems(SuppliersContentHelper.getAllAvailableSuppliers());
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error",
                    "Error parsing all available suppliers: " + e.getMessage());
        }
    }

    public void addNewSupplier(ActionEvent actionEvent) {
        contentSwitcher.switchToAddNewSupplierWindow();
    }

    public void addSupplierToBlackList(ActionEvent actionEvent) {
        Supplier selectedSupplier = viewSuppliersTable.getSelectionModel().getSelectedItem();

        if (selectedSupplier != null) {
            contentSwitcher.switchToAddBlacklistSupplier(selectedSupplier);
        } else {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected",
                    "In order to add supplier to the black list, you have to select it first");
        }
    }

    public void editSupplier(ActionEvent actionEvent) {
        Supplier selectedSupplier = viewSuppliersTable.getSelectionModel().getSelectedItem();

        if (selectedSupplier != null) {
            contentSwitcher.switchToEditSupplier(selectedSupplier);
        } else {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected",
                    "In order to edit, you have to select a supplier first");
        }
    }

    public void deleteSupplier(ActionEvent actionEvent) {
        Supplier selectedSupplier = viewSuppliersTable.getSelectionModel().getSelectedItem();

        if (selectedSupplier != null) {
            if (AlertUtil.confirmationAlert("Confirmation", "Are you sure you want to remove?", "")) {
                if (removeSupplier(selectedSupplier)) {
                    AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Supplier was successfully removed");
                    populateSuppliersTable();
                }
            }
        } else {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected",
                    "In order to remove, you have to select a supplier first");
        }
    }

    private boolean removeSupplier(Supplier supplier) {
        try {
            SuppliersContentHelper.removeSupplier(supplier);

            return true;
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Remove error",
                    "Unable to remove supplier data: " + e.getMessage());
        }

        return false;
    }
}
