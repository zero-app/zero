package suppliersSubsystem.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import sharedControllers.ContentSwitcher;
import suppliersSubsystem.entities.BlackListSupplier;
import suppliersSubsystem.entities.Supplier;
import suppliersSubsystem.entities.Unreliability;
import suppliersSubsystem.suppliersdataacess.BlacklistedSuppliersContentHelper;
import suppliersSubsystem.suppliersdataacess.SuppliersContentHelper;
import suppliersSubsystem.suppliersdataacess.UnreliabilityContentHelper;
import utils.AlertUtil;

public class AddSupplierToBlacklistController {

    public TextField companyCodeField;
    public TextField companyNameField;
    public TextField reasonForBlackListingField;
    public ChoiceBox<Unreliability> unreliabilityChoice;
    private BlackListSupplier blackListSupplier;

    public void initialize(Supplier supplier) {
        try {
            blackListSupplier = new BlackListSupplier(supplier);

            companyCodeField.setText(blackListSupplier.getCompanyCode());
            companyNameField.setText(blackListSupplier.getBlacklistedCompanyName());
            unreliabilityChoice.setItems(UnreliabilityContentHelper.getAllAvailableUnreliabiltyLevels());
            unreliabilityChoice.getSelectionModel().select(0);
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Initialization error",
                    "Could not parse available unreliability levels: " + e.getMessage());
        }
    }

    public void addSupplierToBlackList(ActionEvent actionEvent) {
        if (areAllFieldsValid() && !doesSupplierExistInTheBlackList()) {
            modifyBlacklistSupplierFromInputFields();

            if (addSupplierToTheBlackList(blackListSupplier)) {
                AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success",
                        "Supplier has been successfully added to the black list");
                new ContentSwitcher().switchToViewBlacklistedSuppliers();
            }
        }
    }

    private boolean doesSupplierExistInTheBlackList() {
        try {
            if (SuppliersContentHelper.doesSupplierBelongToTheBlackList(companyCodeField.getText())) {
                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Already exists", "Supplier already exists in the black list");
                return true;
            }
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error",
                    "Error while checking if already supplier belongs to the black list: " + e.getMessage());
        }

        return false;
    }

    private boolean addSupplierToTheBlackList(BlackListSupplier blackListSupplier) {
        try {
            BlacklistedSuppliersContentHelper.insertNewBlackListSupplier(blackListSupplier);

            return true;
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Insertion error",
                    "Unable to add supplier to the black list: " + e.getMessage());
        }

        return false;
    }

    private boolean areAllFieldsValid() {
        if (!isNameInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Company name is not valid");
            return false;
        } else if (!isUnreliabilitySelected()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected", "Unreliability is not selected");
            return false;
        }

        return true;
    }

    private boolean isNameInputValid() {
        return !companyNameField.getText().isEmpty();
    }

    private boolean isUnreliabilitySelected() {
        return unreliabilityChoice.getSelectionModel().getSelectedIndex() >= 0;
    }

    private void modifyBlacklistSupplierFromInputFields() {
        blackListSupplier.setBlacklistedCompanyName(companyNameField.getText());
        blackListSupplier.setReasonForBlacklisting(reasonForBlackListingField.getText());
        blackListSupplier.setUnreliability(unreliabilityChoice.getSelectionModel().getSelectedItem());
    }

    public void returnToSuppliersView(ActionEvent actionEvent) {
        new ContentSwitcher().switchToViewSuppliersWindow();
    }
}
