package suppliersSubsystem.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import sharedControllers.ContentSwitcher;
import suppliersSubsystem.entities.BlackListSupplier;
import suppliersSubsystem.entities.Unreliability;
import suppliersSubsystem.suppliersdataacess.BlacklistedSuppliersContentHelper;
import suppliersSubsystem.suppliersdataacess.UnreliabilityContentHelper;
import utils.AlertUtil;

public class EditBlackListSupplierController {

    public TextField companyCodeField;
    public TextField companyNameField;
    public TextField reasonForBlackListingField;
    public TextField dateAddedField;
    public ChoiceBox<Unreliability> unreliabilityChoice;
    public Button cancelButton;
    public Button okButton;
    private BlackListSupplier blackListSupplier;

    public void initialize(BlackListSupplier blackListSupplier) {
        try {
            this.blackListSupplier = blackListSupplier;

            companyCodeField.setText(blackListSupplier.getCompanyCode());
            companyNameField.setText(blackListSupplier.getBlacklistedCompanyName());
            reasonForBlackListingField.setText(blackListSupplier.getReasonForBlacklisting());
            dateAddedField.setText(blackListSupplier.getDateWhenSupplierWasAdded().toString());
            unreliabilityChoice.setItems(UnreliabilityContentHelper.getAllAvailableUnreliabiltyLevels());
            unreliabilityChoice.getSelectionModel().select(blackListSupplier.getUnreliability());
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Initialization error",
                    "Could not parse editable blacklist supplier, input fields are not set: " + e.getMessage());
        }
    }

    public void updateBlacklistSupplierData(ActionEvent event) {
        if (areAllFieldsValid()) {
            modifyBlacklistSupplierFromInputFields();

            if (updateBlacklistSupplierData(blackListSupplier)) {
                AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success",
                        "Supplier has been successfully updated");
                new ContentSwitcher().switchToViewBlacklistedSuppliers();
            }
        }
    }

    public void returnToBlackListSuppliersView(ActionEvent actionEvent) {
        new ContentSwitcher().switchToViewBlacklistedSuppliers();
    }

    private boolean areAllFieldsValid() {
        if (!isNameInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Company name is not valid");
            return false;
        } else if (!isUnreliabilitySelected()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected", "Unreliability is not selected");
            return false;
        }

        return true;
    }

    private boolean isNameInputValid() {
        return !companyNameField.getText().isEmpty();
    }

    private boolean isUnreliabilitySelected() {
        return unreliabilityChoice.getSelectionModel().getSelectedIndex() >= 0;
    }

    private void modifyBlacklistSupplierFromInputFields() {
        blackListSupplier.setBlacklistedCompanyName(companyNameField.getText());
        blackListSupplier.setReasonForBlacklisting(reasonForBlackListingField.getText());
        blackListSupplier.setUnreliability(unreliabilityChoice.getSelectionModel().getSelectedItem());
    }

    private boolean updateBlacklistSupplierData(BlackListSupplier blackListSupplier) {
        try {
            BlacklistedSuppliersContentHelper.updateBlackListSupplierData(blackListSupplier);

            return true;
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Insertion error",
                    "Unable to update black list supplier data: " + e.getMessage());
        }

        return false;
    }
}
