package suppliersSubsystem.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sharedControllers.ContentSwitcher;
import suppliersSubsystem.entities.BlackListSupplier;
import suppliersSubsystem.suppliersdataacess.BlacklistedSuppliersContentHelper;
import utils.AlertUtil;

public class ViewBlackListSuppliersController {

    public TableView<BlackListSupplier> viewBlacklistSuppliersTable;
    public TableColumn<BlackListSupplier, String> companyCodeColumn;
    public TableColumn<BlackListSupplier, String> nameColumn;
    public TableColumn<BlackListSupplier, String> reasonColumn;
    public TableColumn<BlackListSupplier, String> dateAddedColumn;
    public TableColumn<BlackListSupplier, String> unreliabilityColumn;

    @FXML
    public void initialize() {
        companyCodeColumn.setCellValueFactory(callBack -> callBack.getValue().companyCodeProperty());
        nameColumn.setCellValueFactory(callBack -> callBack.getValue().blacklistedCompanyNameProperty());
        reasonColumn.setCellValueFactory(callBack -> callBack.getValue().reasonForBlacklistingProperty());
        dateAddedColumn.setCellValueFactory(callBack -> callBack.getValue().dateWhenAddedProperty());
        unreliabilityColumn.setCellValueFactory(callBack -> callBack.getValue().getUnreliability().unreliabilityLevelProperty());
        populateBlacklistTable();
    }

    private void populateBlacklistTable() {
        try {
            viewBlacklistSuppliersTable.setItems(BlacklistedSuppliersContentHelper
                    .getAllAvailableBlacklistedSuppliers());
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error",
                    "Error parsing all available blacklisted suppliers: " + e.getMessage());
        }

    }

    public void editBlacklistSupplier(ActionEvent actionEvent) {
        BlackListSupplier selectedSupplier = viewBlacklistSuppliersTable.getSelectionModel().getSelectedItem();

        if (selectedSupplier != null) {
            new ContentSwitcher().switchToEditBlacklistSupplier(selectedSupplier);
        } else {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected",
                    "In order to edit, you have to select a supplier first");
        }
    }

    public void deleteBlacklistSupplier(ActionEvent actionEvent) {
        BlackListSupplier selectedSupplier = viewBlacklistSuppliersTable.getSelectionModel().getSelectedItem();

        if (selectedSupplier != null) {
            if (AlertUtil.confirmationAlert("Confirmation", "Are you sure you want to remove?", "")) {
                if (removeBlackListSupplier(selectedSupplier)) {
                    AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Supplier was successfully removed");
                    populateBlacklistTable();
                }
            }
        } else {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected",
                    "In order to remove, you have to select a supplier first");
        }
    }

    private boolean removeBlackListSupplier(BlackListSupplier supplier) {
        try {
            BlacklistedSuppliersContentHelper.removeBlackListSupplier(supplier);

            return true;
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Remove error",
                    "Unable to remove blacklisted supplier data: " + e.getMessage());
        }

        return false;
    }
}
