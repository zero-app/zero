package suppliersSubsystem.controllers;

import buyersSub.objects.Contact;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import sharedControllers.ContentSwitcher;
import suppliersSubsystem.entities.*;
import suppliersSubsystem.suppliersdataacess.CurrencyContentHelper;
import suppliersSubsystem.suppliersdataacess.SuppliersContentHelper;
import utils.AlertUtil;
import utils.RegexUtil;

import java.time.LocalDate;

public class AddNewSupplierController {

    public Button okButton;
    public Button cancelButton;
    public TextField companyCodeField;
    public TextField vatCodeField;
    public TextField managerField;
    public TextField nameField;
    public TextField descriptionField;
    public TextField faxField;
    public TextField websiteField;
    public TextField employeeCountField;
    public TextField insuranceCodeField;
    public TextField businessCategoryField;
    public TextField phoneNumberField;
    public TextField emailField;
    public TextField turnoverFromField;
    public TextField turnoverToField;
    public TextField ownedCarsField;
    public TextField rentedCarsField;
    public TextField countOfPeopleWhoRatedField;
    public TextField ratingField;
    public TextField debtSizeField;
    public TextField fineSizeField;
    public ChoiceBox<Currency> currencyChoice;

    @FXML
    public void initialize() {
        try {
            currencyChoice.setItems(CurrencyContentHelper.getAllAvailableCurrencies());
            currencyChoice.getSelectionModel().select(0);
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Initialization error",
                    "Could not parse all currencies, try again: " + e.getMessage());
        }
    }

    public void returnToSuppliersView(ActionEvent actionEvent) {
        new ContentSwitcher().switchToViewSuppliersWindow();
    }

    public void addNewSupplier(ActionEvent actionEvent) {
        if (areAllFieldsValid() && !doesSupplierWithTheSameCodeExist() && shouldAddSupplierIfItIsBlackListed()) {

            if (insertNewSupplierToDatabase(parseSupplierFromInputFields())) {
                AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "A new supplier has been added");
                new ContentSwitcher().switchToViewSuppliersWindow();
            }
        }
    }

    private boolean areAllFieldsValid() {
        if (!isCompanyCodeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Company Code is not valid");
            return false;
        } else if (!isVatCodeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "VAT code is not valid");
            return false;
        } else if (!isManagerInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "A full name of the manager is not valid");
            return false;
        } else if (!isNameInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Company name is not valid");
            return false;
        } else if (!isFaxInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Fax is not valid");
            return false;
        } else if (!isWebsiteInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Website is not valid");
            return false;
        } else if (!isEmployeeCountInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Employee count is not valid");
            return false;
        } else if (!isInsurerCodeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Insurer code is not valid");
            return false;
        } else if (!isBusinessCategoryInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Business category is not valid");
            return false;
        } else if (!isPhoneNumberInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Phone number is not valid");
            return false;
        } else if (!isEmailInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "E mail is not valid");
            return false;
        } else if (!isTurnoverFromInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Turnover from is not valid");
            return false;
        } else if (!isTurnoverToInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Turnover to is not valid");
            return false;
        } else if (!isOwnedCarsInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Owned cars is not valid");
            return false;
        } else if (!isRentedCarsInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Rented cars is not valid");
            return false;
        } else if (!isRatedPeopleCountInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Rated people count is not valid");
            return false;
        } else if (!isRatingInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Rating is not valid");
            return false;
        } else if (!isDebtSizeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Debt size is not valid");
            return false;
        } else if (!isFineSizeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Fine size is not valid");
            return false;
        } else if (!isCurrencySelected()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected", "Currency is not selected");
            return false;
        }

        return true;
    }

    private boolean isCompanyCodeInputValid() {
        return RegexUtil.checkIfOnlyNumbers(companyCodeField.getText());
    }

    private boolean isVatCodeInputValid() {
        return RegexUtil.checkIfVatCode(vatCodeField.getText());
    }

    private boolean isManagerInputValid() {
        return RegexUtil.checkIfFullNamePatterFound(managerField.getText());
    }

    private boolean isNameInputValid() {
        return !nameField.getText().isEmpty();
    }

    private boolean isFaxInputValid() {
        return RegexUtil.checkIfPhoneNumber(faxField.getText());
    }

    private boolean isWebsiteInputValid() {
        return RegexUtil.checkIfWebsitePatterFound(websiteField.getText());
    }

    private boolean isEmployeeCountInputValid() {
        return RegexUtil.checkIfOnlyNumbers(employeeCountField.getText());
    }

    private boolean isInsurerCodeInputValid() {
        return RegexUtil.checkIfOnlyNumbers(insuranceCodeField.getText());
    }

    private boolean isBusinessCategoryInputValid() {
        return RegexUtil.checkIfOnlyLetters(businessCategoryField.getText());
    }

    private boolean isPhoneNumberInputValid() {
        return RegexUtil.checkIfPhoneNumber(phoneNumberField.getText());
    }

    private boolean isEmailInputValid() {
        return RegexUtil.checkIfEmail(emailField.getText());
    }

    private boolean isTurnoverFromInputValid() {
        return RegexUtil.checkIfOnlyNumbers(turnoverFromField.getText());
    }

    private boolean isTurnoverToInputValid() {
        return RegexUtil.checkIfOnlyNumbers(turnoverToField.getText());
    }

    private boolean isOwnedCarsInputValid() {
        return RegexUtil.checkIfOnlyNumbers(ownedCarsField.getText());
    }

    private boolean isRentedCarsInputValid() {
        return RegexUtil.checkIfOnlyNumbers(rentedCarsField.getText());
    }

    private boolean isRatedPeopleCountInputValid() {
        return RegexUtil.checkIfOnlyNumbers(countOfPeopleWhoRatedField.getText());
    }

    private boolean isRatingInputValid() {
        return RegexUtil.checkIfOnlyNumbers(ratingField.getText());
    }

    private boolean isDebtSizeInputValid() {
        return RegexUtil.checkIfOnlyNumbers(debtSizeField.getText());
    }

    private boolean isFineSizeInputValid() {
        return RegexUtil.checkIfOnlyNumbers(fineSizeField.getText());
    }

    private boolean isCurrencySelected() {
        return currencyChoice.getSelectionModel().getSelectedIndex() >= 0;
    }

    private Supplier parseSupplierFromInputFields() {
        return new Supplier(companyCodeField.getText(), vatCodeField.getText(), managerField.getText(),
                nameField.getText(), descriptionField.getText(), parseContactFromInputFields(), faxField.getText(),
                websiteField.getText(), Integer.parseInt(employeeCountField.getText()), insuranceCodeField.getText(),
                parseTurnoverFromInputFields(), businessCategoryField.getText(), parseTransportFromInputFields(),
                parseRatingFromInputFields(), parseDebtFromInputFields(), parseSelectedCurrencyFromChoiceBox());
    }

    private Contact parseContactFromInputFields() {
        return new Contact(phoneNumberField.getText(), emailField.getText());
    }

    private Turnover parseTurnoverFromInputFields() {
        return new Turnover(0, Double.parseDouble(turnoverFromField.getText()),
                Double.parseDouble(turnoverToField.getText()), LocalDate.now());
    }

    private Transport parseTransportFromInputFields() {
        Integer ownedCars = Integer.valueOf(ownedCarsField.getText());
        Integer rentedCars = Integer.valueOf(rentedCarsField.getText());

        return new Transport(0, LocalDate.now(), ownedCars + rentedCars, ownedCars, rentedCars);
    }

    private Rating parseRatingFromInputFields() {
        return new Rating(0, Integer.valueOf(countOfPeopleWhoRatedField.getText()), Double.valueOf(ratingField.getText()));
    }

    private Debt parseDebtFromInputFields() {
        return new Debt(0, LocalDate.now(), Double.valueOf(debtSizeField.getText()), Double.valueOf(fineSizeField.getText()));
    }

    private Currency parseSelectedCurrencyFromChoiceBox() {
        return currencyChoice.getSelectionModel().getSelectedItem();
    }

    private boolean doesSupplierWithTheSameCodeExist() {
        try {
            return SuppliersContentHelper.doesSupplierWithTheSameCodeExist(companyCodeField.getText());
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error",
                    "Unfortunately, company with the same code already exists");
        }

        return false;
    }

    private boolean shouldAddSupplierIfItIsBlackListed() {
        return !doesSupplierExistInTheBlackList() ||
                AlertUtil.confirmationAlert("Unreliable supplier warning",
                        "The supplier you want to add belongs to the black list. Are you sure you want to continue?", "");
    }

    private boolean doesSupplierExistInTheBlackList() {
        try {
            return SuppliersContentHelper.doesSupplierBelongToTheBlackList(companyCodeField.getText());
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error",
                    "Error while checking if supplier belongs to the black list: " + e.getMessage());
        }

        return false;
    }

    private boolean insertNewSupplierToDatabase(Supplier supplierToInsert) {
        try {
            SuppliersContentHelper.insertNewSupplier(supplierToInsert);

            return true;
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Insertion error",
                    "Unable to insert supplier to database: " + e.getMessage());
        }

        return false;
    }
}
