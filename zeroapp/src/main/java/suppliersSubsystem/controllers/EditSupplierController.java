package suppliersSubsystem.controllers;

import buyersSub.objects.Contact;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import sharedControllers.ContentSwitcher;
import suppliersSubsystem.entities.*;
import suppliersSubsystem.suppliersdataacess.CurrencyContentHelper;
import suppliersSubsystem.suppliersdataacess.SuppliersContentHelper;
import utils.AlertUtil;
import utils.RegexUtil;

public class EditSupplierController {

    public Button okButton;
    public Button cancelButton;
    public TextField companyCodeField;
    public TextField vatCodeField;
    public TextField managerField;
    public TextField nameField;
    public TextField descriptionField;
    public TextField faxField;
    public TextField websiteField;
    public TextField employeeCountField;
    public TextField insuranceCodeField;
    public TextField businessCategoryField;
    public TextField phoneNumberField;
    public TextField emailField;
    public TextField turnoverFromField;
    public TextField turnoverToField;
    public TextField ownedCarsField;
    public TextField rentedCarsField;
    public TextField countOfPeopleWhoRatedField;
    public TextField ratingField;
    public TextField debtSizeField;
    public TextField fineSizeField;
    public ChoiceBox<Currency> currencyChoice;
    private Supplier editableSupplier;

    public void initialize(Supplier supplier) {
        try {
            editableSupplier = supplier;
            Contact contact = editableSupplier.getCompanyContact();
            Turnover turnover = editableSupplier.getTurnover();
            Transport transport = editableSupplier.getTransport();
            Rating rating = editableSupplier.getRating();
            Debt debt = editableSupplier.getDebt();

            companyCodeField.setText(supplier.getCompanyCode());
            vatCodeField.setText(editableSupplier.getVATCode());
            managerField.setText(editableSupplier.getManager());
            nameField.setText(editableSupplier.getName());
            descriptionField.setText(editableSupplier.getDescription());
            faxField.setText(editableSupplier.getFax());
            websiteField.setText(editableSupplier.getWebsite());
            employeeCountField.setText(Integer.toString(editableSupplier.getEmployeeCount()));
            insuranceCodeField.setText(editableSupplier.getInsurance());
            businessCategoryField.setText(editableSupplier.getBusinessCategory());
            phoneNumberField.setText(contact.getPhoneNumber());
            emailField.setText(contact.getEmail());
            turnoverFromField.setText(String.valueOf(turnover.getMinTurnoverProperty()));
            turnoverToField.setText(String.valueOf(turnover.getMaxTurnoverProperty()));
            ownedCarsField.setText(String.valueOf(transport.getOwnedCars()));
            rentedCarsField.setText(String.valueOf(transport.getRentedCars()));
            countOfPeopleWhoRatedField.setText(String.valueOf(rating.getCountOfPeopleWhoRated()));
            ratingField.setText(String.valueOf(rating.getRating()));
            debtSizeField.setText(String.valueOf(debt.getDebtAmount()));
            fineSizeField.setText(String.valueOf(debt.getFineAmount()));
            currencyChoice.setItems(CurrencyContentHelper.getAllAvailableCurrencies());
            currencyChoice.getSelectionModel().select(editableSupplier.getCurrency());
        } catch (RuntimeException e) {
            editableSupplier = new NullSupplier();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Initialization error",
                    "Could not parse editable supplier, input fields are not set: " + e.getMessage());
        }
    }

    public void returnToSuppliersView(ActionEvent actionEvent) {
        new ContentSwitcher().switchToViewSuppliersWindow();
    }

    public void updateSupplierData(ActionEvent actionEvent) {
        if (areAllFieldsValid()) {
            modifySupplierFromInputFields();

            if (updateSupplierData(editableSupplier)) {
                AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success",
                        "Supplier has been successfully updated");
                new ContentSwitcher().switchToViewSuppliersWindow();
            }
        }
    }

    private boolean areAllFieldsValid() {
        if (!isVatCodeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "VAT code is not valid");
            return false;
        } else if (!isManagerInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "A full name of the manager is not valid");
            return false;
        } else if (!isNameInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Company name is not valid");
            return false;
        } else if (!isFaxInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Fax is not valid");
            return false;
        } else if (!isWebsiteInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Website is not valid");
            return false;
        } else if (!isEmployeeCountInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Employee count is not valid");
            return false;
        } else if (!isInsurerCodeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Insurer code is not valid");
            return false;
        } else if (!isBusinessCategoryInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect data", "Business category is not valid");
            return false;
        } else if (!isPhoneNumberInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Phone number is not valid");
            return false;
        } else if (!isEmailInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "E mail is not valid");
            return false;
        } else if (!isTurnoverFromInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Turnover from is not valid");
            return false;
        } else if (!isTurnoverToInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Turnover to is not valid");
            return false;
        } else if (!isOwnedCarsInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Owned cars is not valid");
            return false;
        } else if (!isRentedCarsInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Rented cars is not valid");
            return false;
        } else if (!isRatedPeopleCountInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Rated people count is not valid");
            return false;
        } else if (!isRatingInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Rating is not valid");
            return false;
        } else if (!isDebtSizeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Debt size is not valid");
            return false;
        } else if (!isFineSizeInputValid()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Fine size is not valid");
            return false;
        } else if (!isCurrencySelected()) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Not selected", "Currency is not selected");
            return false;
        }

        return true;
    }

    private boolean isVatCodeInputValid() {
        return RegexUtil.checkIfVatCode(vatCodeField.getText());
    }

    private boolean isManagerInputValid() {
        return RegexUtil.checkIfFullNamePatterFound(managerField.getText());
    }

    private boolean isNameInputValid() {
        return !nameField.getText().isEmpty();
    }

    private boolean isFaxInputValid() {
        return RegexUtil.checkIfPhoneNumber(faxField.getText());
    }

    private boolean isWebsiteInputValid() {
        return RegexUtil.checkIfWebsitePatterFound(websiteField.getText());
    }

    private boolean isEmployeeCountInputValid() {
        return RegexUtil.checkIfOnlyNumbers(employeeCountField.getText());
    }

    private boolean isInsurerCodeInputValid() {
        return RegexUtil.checkIfOnlyNumbers(insuranceCodeField.getText());
    }

    private boolean isBusinessCategoryInputValid() {
        return RegexUtil.checkIfOnlyLetters(businessCategoryField.getText());
    }

    private boolean isPhoneNumberInputValid() {
        return RegexUtil.checkIfPhoneNumber(phoneNumberField.getText());
    }

    private boolean isEmailInputValid() {
        return RegexUtil.checkIfEmail(emailField.getText());
    }

    private boolean isTurnoverFromInputValid() {
        return RegexUtil.checkIfOnlyNumbers(turnoverFromField.getText());
    }

    private boolean isTurnoverToInputValid() {
        return RegexUtil.checkIfOnlyNumbers(turnoverToField.getText());
    }

    private boolean isOwnedCarsInputValid() {
        return RegexUtil.checkIfOnlyNumbers(ownedCarsField.getText());
    }

    private boolean isRentedCarsInputValid() {
        return RegexUtil.checkIfOnlyNumbers(rentedCarsField.getText());
    }

    private boolean isRatedPeopleCountInputValid() {
        return RegexUtil.checkIfOnlyNumbers(countOfPeopleWhoRatedField.getText());
    }

    private boolean isRatingInputValid() {
        return RegexUtil.checkIfOnlyNumbers(ratingField.getText());
    }

    private boolean isDebtSizeInputValid() {
        return RegexUtil.checkIfOnlyNumbers(debtSizeField.getText());
    }

    private boolean isFineSizeInputValid() {
        return RegexUtil.checkIfOnlyNumbers(fineSizeField.getText());
    }

    private boolean isCurrencySelected() {
        return currencyChoice.getSelectionModel().getSelectedIndex() >= 0;
    }

    private void modifySupplierFromInputFields() {
        editableSupplier.setVATCode(vatCodeField.getText());
        editableSupplier.setManager(managerField.getText());
        editableSupplier.setName(nameField.getText());
        editableSupplier.setDescription(descriptionField.getText());
        modifyContactFromInputFields();
        editableSupplier.setFax(faxField.getText());
        editableSupplier.setWebsite(websiteField.getText());
        editableSupplier.setEmployeeCount(Integer.parseInt(employeeCountField.getText()));
        editableSupplier.setInsurance(insuranceCodeField.getText());
        modifyTurnoverFromInputFields();
        editableSupplier.setBusinessCategory(businessCategoryField.getText());
        modifyTransportFromInputFields();
        modifyRatingFromInputFields();
        modifyDebtFromInputFields();
        modifyCurrencyFromChoiceBox();
    }

    private void modifyContactFromInputFields() {
        Contact contact = editableSupplier.getCompanyContact();

        contact.setPhoneNumber(phoneNumberField.getText());
        contact.setEmail(emailField.getText());
    }

    private void modifyTurnoverFromInputFields() {
        Turnover turnover = editableSupplier.getTurnover();

        turnover.setMinTurnoverProperty(Double.parseDouble(turnoverFromField.getText()));
        turnover.setMaxTurnoverProperty(Double.parseDouble(turnoverToField.getText()));
    }

    private void modifyTransportFromInputFields() {
        Transport transport = editableSupplier.getTransport();

        transport.setOwnedCars(Integer.parseInt(ownedCarsField.getText()));
        transport.setRentedCars(Integer.parseInt(rentedCarsField.getText()));
    }

    private void modifyRatingFromInputFields() {
        Rating rating = editableSupplier.getRating();

        rating.setCountOfPeopleWhoRated(Integer.parseInt(countOfPeopleWhoRatedField.getText()));
        rating.setRating(Double.parseDouble(ratingField.getText()));
    }

    private void modifyDebtFromInputFields() {
        Debt debt = editableSupplier.getDebt();

        debt.setDebtAmount(Double.parseDouble(debtSizeField.getText()));
        debt.setFineAmount(Double.parseDouble(fineSizeField.getText()));
    }

    private void modifyCurrencyFromChoiceBox() {
        editableSupplier.setCurrency(currencyChoice.getSelectionModel().getSelectedItem());
    }

    private boolean updateSupplierData(Supplier editedSupplier) {
        try {
            SuppliersContentHelper.updateSupplierData(editedSupplier);

            return true;
        } catch (RuntimeException e) {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Insertion error",
                    "Unable to update supplier data: " + e.getMessage());
        }

        return false;
    }
}
