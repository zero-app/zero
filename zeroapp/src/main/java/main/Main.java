package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import user.User;
import utils.AuthenticationUtil;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application
{
    private static Stage stage;

    private static BorderPane root = new BorderPane();

    public static BorderPane getRoot()
    {
        return root;
    }

    public static Stage getPrimaryStage()
    {
        return stage;
    }

    @Override
    public void start(Stage primaryStage)
    {
        try
        {
            stage = primaryStage;
            stage.setTitle("ZERO");

            stage.setResizable(true);
            stage.setScene(new Scene(root));
            stage.setMinWidth(1000);
            stage.setMinHeight(500);
            stage.getIcons().add(new Image("/images/z-logo01.png"));
            //root.setStyle("-fx-background-color: n");

            gotoLogin();

            primaryStage.show();
        }
        catch (Exception ex)
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void gotoLogin()
    {
        try
        {
            root.setCenter(FXMLLoader.load(getClass().getResource("/layout/LoginWindow.fxml")));
        }
        catch (Exception ex)
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
