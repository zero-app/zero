package user;

public class User
{
    private String username;
    private String hashedPassword;
    private String salt;
    private int role;

    public User(String username, String hashedPassword, String salt, int role)
    {
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
        this.role = role;
    }

    public String getUsername()
    {
        return username;
    }

    public String getHashedPassword()
    {
        return hashedPassword;
    }

    public String getSalt()
    {
        return salt;
    }

    public int getRole()
    {
        return role;
    }
}
