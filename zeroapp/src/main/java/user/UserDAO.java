package user;

import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

public class UserDAO
{
    private static User extractUser(ResultSet resultSet) throws SQLException
    {
        String username = resultSet.getString("vartotojo_vardas");
        String hashedPassword = resultSet.getString("slaptazodis");
        String salt = resultSet.getString("salt");
        int role = resultSet.getInt("role");

        return new User(username, hashedPassword, salt, role);
    }

    public static Integer getEmployeeFK(String username) throws SQLException, ClassNotFoundException {
        String statement = "SELECT * FROM Vartotojai WHERE vartotojo_vardas='"+username+"'";

            ResultSet resultSet = Database.executeQuery(statement);
        if(resultSet.next())
             return resultSet.getInt("fk_darbuotojas");
            return 1;
    }

    private static User getUserFromResultSet(ResultSet resultSet) throws SQLException
    {
        User user = null;

        if(resultSet.next())
        {
            user = extractUser(resultSet);
        }

        return user;
    }

    public static User getUser(String username) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Vartotojai WHERE vartotojo_vardas='"+username+"'";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getUserFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            throw e;
        }
    }

    public static boolean checkIfUsernameExistsInDatabase(String username) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Vartotojai WHERE vartotojo_vardas='"+username+"'";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        }
        catch (SQLException | ClassNotFoundException e)
        {
            throw e;
        }
    }

    public static void insertUser(String username, String hashedPassword, String salt, int role) throws SQLException, ClassNotFoundException
    {
        String statement = "INSERT INTO Vartotojai (vartotojo_vardas, slaptazodis, salt, role) VALUES('"+username+"', '"+hashedPassword+"', '"+salt+"', '"+role+"')";
        try
        {
            Database.executeUpdate(statement);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            throw e;
        }
    }
}
