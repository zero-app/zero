package utils;

import user.User;
import user.UserDAO;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.SQLException;

public class AuthenticationUtil
{
    private static User loggedUser;

    public static User getLoggedUser()
    {
        return loggedUser;
    }

    private static byte[] salt = {11, -50, 112, -78, -15, 22, -126, 55};

    public static boolean validate(String userId, String password) throws SQLException, ClassNotFoundException
    {
        loggedUser = UserDAO.getUser(userId);

        if(loggedUser == null)
        {
            return false;
        }

        try
        {
            return authenticate(password, loggedUser.getHashedPassword());
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public static Integer getEmployeeId(){
        try {
            return UserDAO.getEmployeeFK(loggedUser.getUsername());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }
    private static boolean authenticate(String attemptedPassword, String encryptedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        byte[] encryptedAttemptedPassword = getEncryptedPassword(attemptedPassword);

        return new String(encryptedAttemptedPassword).equals(encryptedPassword);
    }

    public static byte[] getEncryptedPassword(String password) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        String algorithm = "PBKDF2WithHmacSHA1";
        int derivedKeyLength = 160;
        int iterations = 20000;
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength);
        SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);

        return f.generateSecret(spec).getEncoded();
    }

    public static byte[] generateSalt() throws NoSuchAlgorithmException
    {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[8];
        random.nextBytes(salt);

        return salt;
    }

    public static void registerUser(String username, String password, int role)
    {
        try
        {
            if(!UserDAO.checkIfUsernameExistsInDatabase(username))
            {
                byte[] generatedSalt = generateSalt();
                byte[] encryptedPassword = getEncryptedPassword(password);

                UserDAO.insertUser(username, new String(encryptedPassword, "UTF-8"), new String(generatedSalt, "UTF-8"), role);
            }
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException | SQLException | ClassNotFoundException | UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
    }
}
