package utils;

import com.sun.rowset.CachedRowSetImpl;

import javax.sql.rowset.CachedRowSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database
{
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://db.if.ktu.lt:3306/sausta4";

    private static final String USER = "sausta4";
    private static final String PASS = "pahh8OQuee2Sahth";

    private static Connection connection = null;

    private static void connect() throws SQLException, ClassNotFoundException
    {
        try
        {
            Class.forName(JDBC_DRIVER);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();

            throw e;
        }

        try
        {
            connection =  DriverManager.getConnection(DB_URL, USER, PASS);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    private static void closeConnection() throws SQLException
    {
        try
        {
            if(connection != null && !connection.isClosed())
            {
                connection.close();
                connection = null;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }


    public static ResultSet executeQuery(String statement) throws SQLException, ClassNotFoundException
    {
        CachedRowSet cachedRowSet = null;

        Statement stmt = null;
        ResultSet resultSet = null;

        try
        {
            connect();

            stmt = connection.createStatement();

            resultSet =  stmt.executeQuery(statement);
            cachedRowSet = new CachedRowSetImpl();
            cachedRowSet.populate(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
        finally
        {
            if(resultSet != null)
            {
                resultSet.close();
            }
            if(stmt != null)
            {
                stmt.close();
            }
            closeConnection();
        }

        return cachedRowSet;
    }

    public static void executeUpdate(String statement) throws SQLException, ClassNotFoundException
    {
        Statement stmt = null;

        try
        {
            connect();

            stmt = connection.createStatement();

            stmt.executeUpdate(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
        finally
        {
            if(stmt != null)
            {
                stmt.close();
            }

            closeConnection();
        }
    }

    public static long executeUpdateReturnKey(String statement) throws SQLException, ClassNotFoundException
    {
        Statement stmt = null;

        try
        {
            connect();

            stmt = connection.createStatement();

            stmt.executeUpdate(statement, Statement.RETURN_GENERATED_KEYS);

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs != null && rs.next())
            {
                return rs.getLong(1);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
        finally
        {
            if(stmt != null)
            {
                stmt.close();
            }

            closeConnection();
        }

        return -1;
    }
}
