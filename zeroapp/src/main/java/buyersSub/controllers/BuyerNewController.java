package buyersSub.controllers;

import buyersSub.dataAccessObjects.BuyerDAO;
import buyersSub.objects.Address;
import buyersSub.objects.Buyer;
import buyersSub.objects.Contact;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import javafx.event.ActionEvent;
import utils.AlertUtil;
import utils.RegexUtil;

import java.sql.SQLException;

public class BuyerNewController
{
    @FXML
    private TextField nameField;

    @FXML
    private TextField codeField;

    @FXML
    private TextField vatCodeField;

    @FXML
    private TextField phoneField;

    @FXML
    private TextField emailField;

    @FXML
    private TextField countryField;

    @FXML
    private TextField cityField;

    @FXML
    private TextField streetField;

    @FXML
    private TextField placeNumberField;

    @FXML
    private TextField postalCodeField;

    @FXML
    private Button okButton;

    public void addNewBuyer(ActionEvent event)
    {
        Address buyerAddress = new Address(countryField.getText(), cityField.getText(), streetField.getText(), placeNumberField.getText(), postalCodeField.getText());
        Contact buyerContact = new Contact(phoneField.getText(), emailField.getText());

        Buyer newBuyer = new Buyer(nameField.getText(), vatCodeField.getText(), codeField.getText(), buyerAddress, buyerContact);

        if(checkIfFieldsCorrect())
        {
            Boolean databaseCheck = checkIfVatOrCodeExists(newBuyer);

            if(databaseCheck != null && !databaseCheck)
            {
                Boolean inserted = insertBuyer(newBuyer);
                if(inserted != null && inserted)
                {
                    clearAllFields();

                    AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "New buyers was inserted into database");
                }
            }
        }
    }

    private void clearAllFields()
    {
        nameField.clear();
        codeField.clear();
        vatCodeField.clear();
        countryField.clear();
        cityField.clear();
        streetField.clear();
        placeNumberField.clear();
        postalCodeField.clear();
        phoneField.clear();
        emailField.clear();
    }

    private Boolean checkIfVatOrCodeExists(Buyer buyer)
    {
        try
        {
           return BuyerDAO.checkIfVatOrCodeAlreadyExists(buyer.getVatCode(), buyer.getCode());
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get information from database");
        }

        return null;
    }

    private Boolean insertBuyer(Buyer newBuyer)
    {
        try
        {
            BuyerDAO.insertBuyer(newBuyer);

            return true;
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to insert new value into database");
        }

        return null;
    }

    private boolean checkIfFieldsCorrect()
    {
        boolean correct = true;

        if(nameField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Name field was empty or incorrect");
            correct = false;
        }
        else if(codeField.getText().length() < 1 || !RegexUtil.checkIfNaceCode(codeField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "code field was empty or incorrect");
            correct = false;
        }
        else if(vatCodeField.getText().length() < 1 || !RegexUtil.checkIfVatCode(vatCodeField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Vat code field was empty or incorrect");
            correct = false;
        }
        else if(countryField.getText().length() < 1 || !RegexUtil.checkIfOnlyLetters(countryField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Country field was empty or incorrect");
            correct = false;
        }
        else if(cityField.getText().length() < 1 || !RegexUtil.checkIfOnlyLetters(cityField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "City field was empty or incorrect");
            correct = false;
        }
        else if(streetField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Street field was empty or incorrect");
            correct = false;
        }
        else if(placeNumberField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Place number field was empty or incorrect");
            correct = false;
        }
        else if(postalCodeField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Postal code field was empty or incorrect");
            correct = false;
        }
        else if(phoneField.getText().length() < 1 || !RegexUtil.checkIfPhoneNumber(phoneField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Phone number field was empty or incorrect");
            correct = false;
        }
        else if(emailField.getText().length() < 1 || !RegexUtil.checkIfEmail(emailField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "E-mail field was empty or incorrect");
            correct = false;
        }

        return correct;
    }
}
