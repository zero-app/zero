package buyersSub.controllers;

import buyersSub.dataAccessObjects.BuyerDAO;
import buyersSub.objects.Buyer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import utils.AlertUtil;
import utils.RegexUtil;

import java.sql.SQLException;

/**
 * Created by Saulius on 2016-11-29.
 */
public class BuyerEditController
{
    private Buyer buyer;

    @FXML
    private TextField nameField;

    @FXML
    private TextField codeField;

    @FXML
    private TextField vatCodeField;

    @FXML
    private TextField phoneField;

    @FXML
    private TextField emailField;

    @FXML
    private TextField countryField;

    @FXML
    private TextField cityField;

    @FXML
    private TextField streetField;

    @FXML
    private TextField placeNumberField;

    @FXML
    private TextField postalCodeField;

    @FXML
    private Button okButton;

    public void init(int id)
    {
        nameField.setText(Integer.toString(id));

        try
        {
            buyer = BuyerDAO.getBuyer(id);

            nameField.setText(buyer.getName());
            codeField.setText(buyer.getCode());
            vatCodeField.setText(buyer.getVatCode());
            countryField.setText(buyer.getAddress().getCountry());
            cityField.setText(buyer.getAddress().getCity());
            streetField.setText(buyer.getAddress().getStreet());
            placeNumberField.setText(buyer.getAddress().getPlaceNumber());
            postalCodeField.setText(buyer.getAddress().getPostalCode());
            phoneField.setText(buyer.getContact().getPhoneNumber());
            emailField.setText(buyer.getContact().getEmail());
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get buyer from database");
        }
    }

    public void editBuyer(ActionEvent event)
    {
        if(checkIfFieldsCorrect())
        {
            buyer.setName(nameField.getText());
            buyer.setVatCode(vatCodeField.getText());
            buyer.setCode(codeField.getText());
            buyer.getAddress().setCity(cityField.getText());
            buyer.getAddress().setCountry(countryField.getText());
            buyer.getAddress().setPlaceNumber(placeNumberField.getText());
            buyer.getAddress().setPostalCode(postalCodeField.getText());
            buyer.getAddress().setStreet(streetField.getText());
            buyer.getContact().setEmail(emailField.getText());
            buyer.getContact().setPhoneNumber(phoneField.getText());

            if(updateBuyer(buyer))
            {
                AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Buyer has been updated in database");
            }
        }
    }

    private boolean updateBuyer(Buyer buyer)
    {
        try
        {
            BuyerDAO.updateBuyer(buyer);

            return true;
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to update the buyer in database");
        }

        return false;
    }

    private boolean checkIfFieldsCorrect()
    {
        boolean correct = true;

        if(nameField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Name field was empty or incorrect");
            correct = false;
        }
        else if(postalCodeField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Postal code field was empty or incorrect");
            correct = false;
        }
        else if(streetField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Street field was empty or incorrect");
            correct = false;
        }
        else if(countryField.getText().length() < 1 || !RegexUtil.checkIfOnlyLetters(countryField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Country field was empty or incorrect");
            correct = false;
        }
        else if(cityField.getText().length() < 1 || !RegexUtil.checkIfOnlyLetters(cityField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "City field was empty or incorrect");
            correct = false;
        }
        else if(placeNumberField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Place number field was empty or incorrect");
            correct = false;
        }
        else if(emailField.getText().length() < 1 || !RegexUtil.checkIfEmail(emailField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "E-mail field was empty or incorrect");
            correct = false;
        }
        else if(phoneField.getText().length() < 1 || !RegexUtil.checkIfPhoneNumber(phoneField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Incorrect field", "Phone number field was empty or incorrect");
            correct = false;
        }

        return correct;
    }
}
