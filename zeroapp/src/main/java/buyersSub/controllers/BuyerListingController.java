package buyersSub.controllers;

import buyersSub.dataAccessObjects.BuyerDAO;
import buyersSub.dataAccessObjects.BuyerOrderDAO;
import buyersSub.objects.Buyer;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import sharedControllers.ContentSwitcher;
import utils.AlertUtil;
import java.sql.SQLException;

public class BuyerListingController extends AnchorPane
{
    ContentSwitcher switcher = new ContentSwitcher();

    @FXML
    private TableView<Buyer> buyerTable;
    @FXML
    private TableColumn<Buyer, Integer>  buyerIdColumn;
    @FXML
    private TableColumn<Buyer, String>  buyerNameColumn;
    @FXML
    private TableColumn<Buyer, String> buyerVatCodeColumn;
    @FXML
    private TableColumn<Buyer, String> buyerCodeColumn;

    @FXML
    private TableColumn<Buyer, String> buyerCountryColumn;
    @FXML
    private TableColumn<Buyer, String> buyerCityColumn;
    @FXML
    private TableColumn<Buyer, String> buyerStreetColumn;
    @FXML
    private TableColumn<Buyer, String> buyerPlaceNumberColumn;
    @FXML
    private TableColumn<Buyer, String> buyerPostalCodeColumn;

    @FXML
    private TableColumn<Buyer, String> buyerPhoneNumberColumn;
    @FXML
    private TableColumn<Buyer, String> buyerEmailColumn;

    @FXML
    private void initialize()
    {
        buyerIdColumn.setCellValueFactory(cellData -> cellData.getValue().getIdProperty().asObject());
        buyerNameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
        buyerVatCodeColumn.setCellValueFactory(cellData -> cellData.getValue().getVatCodeProperty());
        buyerCodeColumn.setCellValueFactory(cellData -> cellData.getValue().getCodeProperty());

        buyerPhoneNumberColumn.setCellValueFactory(cellData -> cellData.getValue().getContact().getPhoneNumberProperty());
        buyerEmailColumn.setCellValueFactory(cellData -> cellData.getValue().getContact().getEmailProperty());

        buyerCountryColumn.setCellValueFactory(cellData -> cellData.getValue().getAddress().getCountryProperty());
        buyerCityColumn.setCellValueFactory(cellData -> cellData.getValue().getAddress().getCityProperty());
        buyerStreetColumn.setCellValueFactory(cellData -> cellData.getValue().getAddress().getStreetProperty());
        buyerPlaceNumberColumn.setCellValueFactory(cellData -> cellData.getValue().getAddress().getPlaceNumberProperty());
        buyerPostalCodeColumn.setCellValueFactory(cellData -> cellData.getValue().getAddress().getPostalCodeProperty());

        showAllBuyers();
    }

    public void switchToAddNewBuyerWindow(ActionEvent event)
    {
        switcher.switchToAddNewBuyerWindow();
    }

    public void deleteBuyer(ActionEvent event)
    {
        Buyer selectedBuyer = buyerTable.getSelectionModel().getSelectedItem();
        int index = buyerTable.getSelectionModel().getSelectedIndex();

        if(selectedBuyer != null)
        {
            if(AlertUtil.confirmationAlert("Confirmation", "Are you sure?", ""))
            {
                if(deleteSelectedBuyer(selectedBuyer.getId()))
                {
                    AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Buyer was deleted");

                    buyerTable.getItems().remove(index);
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "This buyer has orders");
                }
            }
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing is selected");
        }
    }

    public void editBuyer(ActionEvent event)
    {
        Buyer selectedBuyer = buyerTable.getSelectionModel().getSelectedItem();

        if(selectedBuyer != null)
        {
            switcher.switchToEditBuyerWindow(selectedBuyer.getId());
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing is selected");
        }
    }

    private boolean deleteSelectedBuyer(int id)
    {
        try
        {
            if(!BuyerOrderDAO.checkIfBuyerWithIdHasOrders(id))
            {
                BuyerDAO.deleteBuyer(id);

                return true;
            }

            return false;
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Database error", "Unable to delete the buyer from database");

            return false;
        }
    }

    private void showAllBuyers()
    {
        try
        {
            ObservableList<Buyer> buyersData = BuyerDAO.getAllBuyers();
            populateBuyers(buyersData);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Klaida!", "Klaida", "Error");
        }
    }

    private void populateBuyers (ObservableList<Buyer> buyersData) throws ClassNotFoundException
    {
        buyerTable.setItems(buyersData);
    }
}
