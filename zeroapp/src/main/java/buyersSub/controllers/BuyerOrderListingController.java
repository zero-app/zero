package buyersSub.controllers;

import buyersSub.dataAccessObjects.BuyerOrderDAO;
import buyersSub.objects.BuyerOrder;
import goodsSub.dataAccessObjects.GoodsDAO;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sharedControllers.ContentSwitcher;
import utils.AlertUtil;

import java.sql.SQLException;

public class BuyerOrderListingController
{
    private static int FINISHED_ORDER_STATE_ID = 5;

    ContentSwitcher switcher = new ContentSwitcher();

    @FXML
    private TableView<BuyerOrder> buyerOrderTable;

    @FXML
    private TableColumn<BuyerOrder, Integer> buyerOrderIdColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerOrderDateColumn;
    @FXML
    private TableColumn<BuyerOrder, Double> buyerOrderTotalColumn;
    @FXML
    private TableColumn<BuyerOrder, String>  buyerNameColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerVatCodeColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerCodeColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerCountryColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerCityColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerPostalCodeColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerStreetColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerPhoneNumberColumn;
    @FXML
    private TableColumn<BuyerOrder, String> buyerOrderStateColumn;

    @FXML
    private void initialize()
    {
        buyerOrderIdColumn.setCellValueFactory(cellData -> cellData.getValue().getIdProperty().asObject());
        buyerOrderDateColumn.setCellValueFactory(cellData -> cellData.getValue().getDateProperty());
        buyerOrderTotalColumn.setCellValueFactory(cellData -> cellData.getValue().getTotalProperty().asObject());

        buyerNameColumn.setCellValueFactory(cellData -> cellData.getValue().getBuyer().getNameProperty());
        buyerVatCodeColumn.setCellValueFactory(cellData -> cellData.getValue().getBuyer().getVatCodeProperty());
        buyerCodeColumn.setCellValueFactory(cellData -> cellData.getValue().getBuyer().getCodeProperty());

        buyerPhoneNumberColumn.setCellValueFactory(cellData -> cellData.getValue().getBuyer().getContact().getPhoneNumberProperty());
        buyerCountryColumn.setCellValueFactory(cellData -> cellData.getValue().getBuyer().getAddress().getCountryProperty());
        buyerCityColumn.setCellValueFactory(cellData -> cellData.getValue().getBuyer().getAddress().getCityProperty());
        buyerPostalCodeColumn.setCellValueFactory(cellData -> cellData.getValue().getBuyer().getAddress().getPostalCodeProperty());
        buyerStreetColumn.setCellValueFactory(cellData -> cellData.getValue().getBuyer().getAddress().getStreetProperty());

        buyerOrderStateColumn.setCellValueFactory(cellData -> cellData.getValue().getOrderState().getStateNameProperty());

        showAllBuyerOrdersEssentials();
    }

    public void switchToAddNewBuyerOrderWindow(ActionEvent event)
    {
        switcher.switchToBuyerOrderEditWindow(null);
    }

    public void deleteBuyerOrder(ActionEvent event)
    {
        BuyerOrder selectedBuyerOrder = buyerOrderTable.getSelectionModel().getSelectedItem();
        int index = buyerOrderTable.getSelectionModel().getSelectedIndex();

        if(selectedBuyerOrder != null)
        {
            if(AlertUtil.confirmationAlert("Confirmation", "Are you sure?", ""))
            {
                if(deleteSelectedBuyerOrder(selectedBuyerOrder.getId()))
                {
                    AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Buyer order was deleted");

                    buyerOrderTable.getItems().remove(index);
                }
            }
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing is selected");
        }
    }

    public void editBuyerOrder(ActionEvent event)
    {
        BuyerOrder selectedBuyerOrder = buyerOrderTable.getSelectionModel().getSelectedItem();

        if(selectedBuyerOrder != null)
        {
            if(selectedBuyerOrder.getOrderState().getIdState() != FINISHED_ORDER_STATE_ID)
            {
                switcher.switchToBuyerOrderEditWindow(selectedBuyerOrder.getId());
            }
            else
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Delivered orders are not editable");
            }
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing is selected");
        }
    }

    private boolean deleteSelectedBuyerOrder(int id)
    {
        try
        {
            BuyerOrder buyerOrder = BuyerOrderDAO.getBuyerOrder(id);
            if(buyerOrder.getOrderState().getIdState() == FINISHED_ORDER_STATE_ID)
            {
                BuyerOrderDAO.deleteBuyerOrder(id);

                return true;
            }
            else
            {
                GoodsDAO.changeAmounts(false, buyerOrder.getOrderedGoods());

                BuyerOrderDAO.deleteBuyerOrder(id);

                return true;
            }
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Database error", "Unable to delete the buyer order from database");

            return false;
        }
    }

    private void showAllBuyerOrdersEssentials()
    {
        try
        {
            ObservableList<BuyerOrder> buyersData = BuyerOrderDAO.getAllBuyerOrderEssentials();
            populateBuyerOrdersEssentials(buyersData);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error!", "Error", "Unable to get buyer orders data from database");
        }
    }

    private void populateBuyerOrdersEssentials (ObservableList<BuyerOrder> buyersData) throws ClassNotFoundException
    {
        buyerOrderTable.setItems(buyersData);
    }
}
