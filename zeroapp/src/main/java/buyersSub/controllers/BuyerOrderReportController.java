package buyersSub.controllers;

import buyersSub.dataAccessObjects.SoldByTypeDAO;
import buyersSub.objects.SoldByType;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import utils.AlertUtil;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class BuyerOrderReportController
{
    private static String DATE_FORMAT = "yyyy-MM-dd";

    @FXML
    private DatePicker dateFromPicker;
    @FXML
    private DatePicker dateToPicker;

    @FXML
    private TextField grandTotalField;

    @FXML
    private TableView<SoldByType> soldByTypeTable;
    @FXML
    private TableColumn<SoldByType, String> goodsTypeNameColumn;
    @FXML
    private TableColumn<SoldByType, Integer> soldAmountColumn;
    @FXML
    private TableColumn<SoldByType, Double> totalColumn;

    @FXML
    private void initialize()
    {
        setUpCells();
        setDatePickerFormat(dateFromPicker);
        setDatePickerFormat(dateToPicker);
    }

    @FXML
    public void generateReport(ActionEvent event)
    {
        if(checkFields())
        {
            try
            {
                ObservableList<SoldByType> soldByTypeObservableList = SoldByTypeDAO.getAllSoldByType(dateFromPicker.getValue().toString(), dateToPicker.getValue().toString());
                soldByTypeTable.setItems(soldByTypeObservableList);
                setGrandTotalField(soldByTypeObservableList);
            }
            catch (SQLException | ClassNotFoundException e)
            {
                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Unable to get data from database");
                e.printStackTrace();
            }
        }
    }

    private boolean checkFields()
    {
        if(dateFromPicker.getValue() == null)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Incorrect from date field");
            return false;
        }
        else if(dateToPicker.getValue() == null)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Incorrect from date field");
            return false;
        }
        else if(dateFromPicker.getValue().isAfter(dateToPicker.getValue()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "From date is after to date");
            return false;
        }

        return true;
    }

    private void setGrandTotalField(ObservableList<SoldByType> soldByTypeObservableList)
    {
        double grandTotal = 0;
        for(int i = 0; i < soldByTypeObservableList.size(); i++)
        {
            grandTotal += soldByTypeObservableList.get(i).getTotal();
        }

        grandTotalField.setText(Double.toString(grandTotal));
    }

    private void setUpCells()
    {
        goodsTypeNameColumn.setCellValueFactory(cellData -> cellData.getValue().getTypeNameProperty());
        soldAmountColumn.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty().asObject());
        totalColumn.setCellValueFactory(cellData -> cellData.getValue().getTotalPropert().asObject());
    }

    private void setDatePickerFormat(DatePicker datePicker)
    {
        datePicker.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }
}
