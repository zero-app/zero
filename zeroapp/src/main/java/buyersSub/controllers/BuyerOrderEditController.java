package buyersSub.controllers;

import buyersSub.dataAccessObjects.BuyerDAO;
import buyersSub.dataAccessObjects.BuyerOrderDAO;
import buyersSub.dataAccessObjects.OrderStateDAO;
import buyersSub.objects.Buyer;
import buyersSub.objects.BuyerOrder;
import buyersSub.objects.OrderState;
import buyersSub.objects.OrderedGoods;
import goodsSub.dataAccessObjects.GoodsDAO;
import goodsSub.objects.Goods;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.controlsfx.control.textfield.TextFields;
import utils.AlertUtil;
import utils.RegexUtil;

import java.sql.SQLException;

public class BuyerOrderEditController
{
    private boolean editMode = false;
    private BuyerOrder buyerOrder;

    private ObservableList<Buyer> buyersData;
    private  ObservableList<OrderState> statesData;

    @FXML
    private TextField goodsField;
    @FXML
    private TextField amountField;
    @FXML
    private ChoiceBox<OrderState> stateBox;
    @FXML
    private TextField dateField;
    @FXML
    private TextField totalField;

    @FXML
    private ChoiceBox<Buyer> buyersBox;

    @FXML
    private TableView<OrderedGoods> orderedGoodsTable;

    @FXML
    private TableColumn<OrderedGoods, Integer> amountColumn;
    @FXML
    private TableColumn<OrderedGoods, String> barcodeColumn;
    @FXML
    private TableColumn<OrderedGoods, String> nameColumn;
    @FXML
    private TableColumn<OrderedGoods, String> manufacturerColumn;
    @FXML
    private TableColumn<OrderedGoods, Double> priceColumn;

    private ObservableList<Goods> goodsList;
    private ObservableList<OrderedGoods> orderedGoodsList =  FXCollections.observableArrayList();
    private ObservableList<OrderedGoods> previousOrderedGoodsList =  FXCollections.observableArrayList();

    public void init(Integer id)
    {
        setUpTableCells();
        setUpAddGoodsField();
        populateBuyersChoiceBox();
        populateOrderStateChoiceBox();
        setTotalField();

        if(id != null)
        {
            editMode = true;
            try
            {
                buyerOrder = BuyerOrderDAO.getBuyerOrder(id);
                populateFields();
                previousOrderedGoodsList = buyerOrder.getOrderedGoods();
            }
            catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();

                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get information from database");
            }
        }
    }

    private void addPreviousOrderAmountsBack()
    {
        try
        {
            GoodsDAO.changeAmounts(false, previousOrderedGoodsList);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to change goods amounts in database");
            e.printStackTrace();
        }
    }

    private void populateFields()
    {
        for(int i = 0; i < buyersData.size(); i++)
        {
            if(buyerOrder.getBuyer().getId() == buyersData.get(i).getId())
            {
                buyersBox.getSelectionModel().select(i);
                break;
            }
        }
        for(int i = 0; i < statesData.size(); i++)
        {
            if(buyerOrder.getOrderState().getIdState() == statesData.get(i).getIdState())
            {
                stateBox.getSelectionModel().select(i);
                break;
            }
        }

        dateField.setText(buyerOrder.getDate());

        orderedGoodsTable.setItems(buyerOrder.getOrderedGoods());
        orderedGoodsList = buyerOrder.getOrderedGoods();

        setTotalField();
    }

    private void setUpTableCells()
    {
        amountColumn.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty().asObject());
        barcodeColumn.setCellValueFactory(cellData -> cellData.getValue().getGoods().getBarcodeProperty());

        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getGoods().getNameProperty());
        manufacturerColumn.setCellValueFactory(cellData -> cellData.getValue().getGoods().getManufacturerProperty());
        priceColumn.setCellValueFactory(cellData -> cellData.getValue().getGoods().getPriceProperty().asObject());
    }

    private void setUpAddGoodsField()
    {
        try
        {
            goodsList = GoodsDAO.getAllGoodsEssentials();
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get goods for suggestion box from database");
        }

        TextFields.bindAutoCompletion(goodsField, goodsList);
    }

    private void populateBuyersChoiceBox()
    {
        try
        {
            buyersData = BuyerDAO.getAllBuyers();

            buyersBox.setItems(buyersData);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get buyers from database");
        }
    }

    private void populateOrderStateChoiceBox()
    {
        try
        {
            statesData = OrderStateDAO.getAllOrderStates();

            stateBox.setItems(statesData);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get order states from database");
        }
    }

    @FXML
    public void addGoodsToOrder(ActionEvent event)
    {
        if(checkAddOrderedGoodsFields())
        {
            Integer goodsId = new Integer(goodsField.getText().split(" ")[0]);
            int amount = new Integer(amountField.getText());

            try
            {
                if(GoodsDAO.checkIfGoodsWithIdExists(goodsId))
                {
                    if(!checkIfGoodsIsAlreadyInOrder(goodsId))
                    {
                        orderedGoodsList.add(new OrderedGoods(amount, GoodsDAO.getGoodsWithId(goodsId)));
                        orderedGoodsTable.setItems(orderedGoodsList);
                        setTotalField();
                    }
                    else
                    {
                        AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Item with this id is already in order");
                    }
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Goods with this id does not exists in database");
                }
            }
            catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();

                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to check if goods with this id exists in database");
            }
        }
    }

    @FXML
    public void deleteOrderedGoods(ActionEvent event)
    {
        OrderedGoods selectedOrderedGoods = orderedGoodsTable.getSelectionModel().getSelectedItem();
        int index = orderedGoodsTable.getSelectionModel().getSelectedIndex();

        if(selectedOrderedGoods != null)
        {
            if(AlertUtil.confirmationAlert("Confirmation", "Are you sure?", ""))
            {
                orderedGoodsTable.getItems().remove(index);
                setTotalField();
                AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Ordered goods was deleted");
            }
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing is selected");
        }
    }

    @FXML
    public void saveBuyerOrder(ActionEvent event)
    {
        if(checkFields())
        {
            if(editMode)
            {
                if(checkAmounts())
                {
                    addPreviousOrderAmountsBack();
                    updateBuyerOrder();
                }
            }
            else
            {
                if(checkAmounts())
                {
                    insertBuyerOrder();
                }
            }
        }
    }

    private boolean checkAmounts()
    {
        for(int i = 0; i < orderedGoodsList.size(); i++)
        {
            try
            {
                Goods goods = GoodsDAO.getGoodsWithId(orderedGoodsList.get(i).getGoods().getIdGoods());
                if(goods.getAmount() < orderedGoodsList.get(i).getAmount())
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", goods.toString() + " is not enough to fulfill order");

                    return false;
                }
            }
            catch (SQLException | ClassNotFoundException e)
            {
                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to connect to database");

                e.printStackTrace();
            }
        }

        return true;
    }

    private void insertBuyerOrder()
    {
        Buyer selectedBuyer = buyersBox.getSelectionModel().getSelectedItem();
        OrderState selectedState = stateBox.getSelectionModel().getSelectedItem();
        try
        {
            GoodsDAO.changeAmounts(true, orderedGoodsList);
            BuyerOrderDAO.insertBuyerOrder(new BuyerOrder(dateField.getText(), new Double(totalField.getText()), selectedBuyer, selectedState, orderedGoodsList));

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Order was added to database");
        }
        catch (SQLException | ClassNotFoundException e)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to insert order into database");
            e.printStackTrace();
        }
    }

    private void updateBuyerOrder()
    {
        Buyer selectedBuyer = buyersBox.getSelectionModel().getSelectedItem();
        OrderState selectedState = stateBox.getSelectionModel().getSelectedItem();
        try
        {
            BuyerOrderDAO.deleteBuyerOrder(buyerOrder.getId());

            GoodsDAO.changeAmounts(true, orderedGoodsList);
            BuyerOrderDAO.insertBuyerOrder(new BuyerOrder(dateField.getText(), new Double(totalField.getText()), selectedBuyer, selectedState, orderedGoodsList));

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Order was updated in database");
        }
        catch (SQLException | ClassNotFoundException e)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to update order in database");
            e.printStackTrace();
        }
    }

    private boolean checkFields()
    {
        Buyer selectedBuyer = buyersBox.getSelectionModel().getSelectedItem();
        OrderState selectedState = stateBox.getSelectionModel().getSelectedItem();
        if(selectedBuyer == null)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Buyer is not selected");
            return false;
        }
        else if(orderedGoodsList.size() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Order is empty");
            return false;
        }
        else if(selectedState == null)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Order state is not selected");
            return false;
        }
        else if(dateField.getText().length() < 1 || !RegexUtil.checkIfDate(dateField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Date is incorrect");
            return false;
        }

        return true;
    }

    private boolean checkAddOrderedGoodsFields()
    {
        if(goodsField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Incorrect goods id");
            return false;
        }
        else if(amountField.getText().length() < 1 || !RegexUtil.checkIfInteger(amountField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Incorrect amount");
            return false;
        }

        return true;
    }

    private void setTotalField()
    {
        double total = 0;
        for(int i = 0; i < orderedGoodsList.size(); i++)
        {
            total += orderedGoodsList.get(i).getGoods().getPrice() * orderedGoodsList.get(i).getAmount();
        }

        totalField.setText(Double.toString(total));
    }

    private boolean checkIfGoodsIsAlreadyInOrder(int goodsId)
    {
        for(int i = 0; i < orderedGoodsList.size(); i++)
        {
            if (orderedGoodsList.get(i).getGoods().getIdGoods() == goodsId)
            {
                return true;
            }
        }

        return false;
    }
}
