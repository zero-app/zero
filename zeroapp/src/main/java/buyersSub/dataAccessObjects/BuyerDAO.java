package buyersSub.dataAccessObjects;

import buyersSub.objects.Address;
import buyersSub.objects.Buyer;
import buyersSub.objects.Contact;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BuyerDAO
{
    private static Buyer extractBuyer(ResultSet resultSet) throws SQLException
    {
        int idBuyer = resultSet.getInt("id_Uzsakovas");
        String buyerName = resultSet.getString("pavadinimas");
        String buyerVatCode = resultSet.getString("vat_kodas");
        String buyerCode = resultSet.getString("kodas");

        int idContacts = resultSet.getInt("id_Kontaktas");
        String contactsPhone = resultSet.getString("telefonas");
        String contactsEmail = resultSet.getString("epastas");


        int idAddress = resultSet.getInt("id_Adresas");
        String addressCountry = resultSet.getString("salis");
        String addressCity = resultSet.getString("miestas");
        String addressStreet = resultSet.getString("gatve");
        String addressNumber = resultSet.getString("numeris");
        String addressPostalCode = resultSet.getString("zip_kodas");

        Contact buyerContacts = new Contact(idContacts, contactsPhone, contactsEmail);
        Address buyerAddress = new Address(idAddress, addressCountry, addressCity, addressStreet, addressNumber, addressPostalCode);

        return new Buyer(idBuyer, buyerName, buyerVatCode, buyerCode, buyerAddress, buyerContacts);
    }

    private static Buyer getBuyerFromResultSet(ResultSet resultSet) throws SQLException
    {
        Buyer buyer = null;

        if(resultSet.next())
        {
            buyer = extractBuyer(resultSet);
        }

        return buyer;
    }

    private static ObservableList<Buyer> getAllBuyerFromResultSet(ResultSet resultSet) throws SQLException
    {
        ObservableList<Buyer> buyerObservableList = FXCollections.observableArrayList();

        while(resultSet.next())
        {
            buyerObservableList.add(extractBuyer(resultSet));
        }

        return buyerObservableList;
    }

    public static Buyer getBuyer(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Uzsakovai INNER JOIN Adresai, Kontaktai WHERE Uzsakovai.id_Uzsakovas=" + id + " AND Adresai.id_Adresas=Uzsakovai.fk_adresas AND Kontaktai.id_Kontaktas=Uzsakovai.fk_kontaktai";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getBuyerFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static ObservableList<Buyer> getAllBuyers() throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Uzsakovai INNER JOIN Adresai, Kontaktai WHERE Adresai.id_Adresas=Uzsakovai.fk_adresas AND Kontaktai.id_Kontaktas=Uzsakovai.fk_kontaktai";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getAllBuyerFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void insertBuyer(Buyer buyer) throws SQLException, ClassNotFoundException
    {
        Address bA = buyer.getAddress();
        Contact bC = buyer.getContact();

        try
        {
            String insertAdrress = "INSERT INTO Adresai (salis, miestas, gatve, numeris, zip_kodas) VALUES('"+bA.getCountry()+"', '"+bA.getCity()+"', '"+bA.getStreet()+"', '"+bA.getPlaceNumber()+"', '"+bA.getPostalCode()+"');";
            long addressId = Database.executeUpdateReturnKey(insertAdrress);

            String insertContacts = "INSERT INTO Kontaktai (telefonas, epastas) VALUES ('"+bC.getPhoneNumber()+"', '"+bC.getEmail()+"');";
            long contactId = Database.executeUpdateReturnKey(insertContacts);

            String insertBuyer = "INSERT INTO Uzsakovai (pavadinimas, vat_kodas, kodas, fk_kontaktai, fk_adresas) VALUES('"+buyer.getName()+"', '"+buyer.getVatCode()+"', '"+buyer.getCode()+"', '"+contactId+"', '"+addressId+"');";
            Database.executeUpdate(insertBuyer);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void updateBuyer(Buyer buyer) throws SQLException, ClassNotFoundException
    {
        Address bA = buyer.getAddress();
        Contact bC = buyer.getContact();

        String updateAddress = "UPDATE Adresai SET salis='"+bA.getCountry()+"', miestas='"+bA.getCity()+"', gatve='"+bA.getStreet()+"', numeris='"+bA.getPlaceNumber()+"', zip_kodas='"+bA.getPostalCode()+"' WHERE id_Adresas='"+bA.getId()+"';";
        String updateContacts = "UPDATE Kontaktai SET telefonas='"+bC.getPhoneNumber()+"', epastas='"+bC.getEmail()+"' WHERE id_Kontaktas='"+bC.getId()+"';";
        String updateBuyer = "UPDATE Uzsakovai SET pavadinimas='"+buyer.getName()+"', vat_kodas='"+buyer.getVatCode()+"', kodas='"+buyer.getCode()+"' WHERE id_Uzsakovas='"+buyer.getId()+"';";

        try
        {
            Database.executeUpdate(updateAddress);
            Database.executeUpdate(updateContacts);
            Database.executeUpdate(updateBuyer);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void deleteBuyer(int id) throws SQLException, ClassNotFoundException
    {
        try
        {
            String selectFKs = "SELECT fk_kontaktai, fk_adresas FROM Uzsakovai WHERE id_Uzsakovas='"+id+"'";
            ResultSet resultSet = Database.executeQuery(selectFKs);
            resultSet.next();

            String deleteBuyer = "DELETE FROM Uzsakovai WHERE id_Uzsakovas='"+id+"'";
            Database.executeUpdate(deleteBuyer);

            String deleteAddress = "DELETE FROM Adresai WHERE id_Adresas='"+resultSet.getInt("fk_adresas")+"'";
            Database.executeUpdate(deleteAddress);

            String deleteContact = "DELETE FROM Kontaktai WHERE id_Kontaktas='"+resultSet.getInt("fk_kontaktai")+"'";
            Database.executeUpdate(deleteContact);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static boolean checkIfVatOrCodeAlreadyExists(String vatCode, String code) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Uzsakovai WHERE vat_kodas='"+vatCode+"' OR kodas='"+code+"'";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
