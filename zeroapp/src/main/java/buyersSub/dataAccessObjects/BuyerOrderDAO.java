package buyersSub.dataAccessObjects;

import buyersSub.objects.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BuyerOrderDAO
{
    private static BuyerOrder extractBuyerOrderEssentials(ResultSet resultSet) throws SQLException
    {
        int idOrder = resultSet.getInt("id_Uzsakymas");
        String date = resultSet.getString("data");
        double total = resultSet.getDouble("suma");

        int idState = resultSet.getInt("id_Busena");
        String stateName = resultSet.getString("busena");

        int idBuyer = resultSet.getInt("id_Uzsakovas");
        String buyerName = resultSet.getString("pavadinimas");
        String buyerVatCode = resultSet.getString("vat_kodas");
        String buyerCode = resultSet.getString("kodas");

        int idContacts = resultSet.getInt("id_Kontaktas");
        String contactsPhone = resultSet.getString("telefonas");
        String contactsEmail = resultSet.getString("epastas");


        int idAddress = resultSet.getInt("id_Adresas");
        String addressCountry = resultSet.getString("salis");
        String addressCity = resultSet.getString("miestas");
        String addressStreet = resultSet.getString("gatve");
        String addressNumber = resultSet.getString("numeris");
        String addressPostalCode = resultSet.getString("zip_kodas");

        OrderState orderState = new OrderState(idState, stateName);

        Contact buyerContacts = new Contact(idContacts, contactsPhone, contactsEmail);
        Address buyerAddress = new Address(idAddress, addressCountry, addressCity, addressStreet, addressNumber, addressPostalCode);

        Buyer buyer = new Buyer(idBuyer, buyerName, buyerVatCode, buyerCode, buyerAddress, buyerContacts);

        return new BuyerOrder(idOrder, date, total, buyer, orderState);
    }

    private static ObservableList<BuyerOrder> getAllBuyerOrderEssentialsFromResultSet(ResultSet resultSet) throws SQLException
    {
        ObservableList<BuyerOrder> buyerOrderEssentialsObservableList = FXCollections.observableArrayList();

        while(resultSet.next())
        {
            buyerOrderEssentialsObservableList.add(extractBuyerOrderEssentials(resultSet));
        }

        return buyerOrderEssentialsObservableList;
    }

    private static BuyerOrder getBuyerOrderEssentialsFromResultSet(ResultSet resultSet) throws SQLException
    {
        BuyerOrder buyerOrder = null;

        if(resultSet.next())
        {
            buyerOrder = extractBuyerOrderEssentials(resultSet);
        }

        return buyerOrder;
    }

    public static ObservableList<BuyerOrder> getAllBuyerOrderEssentials() throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM  Uzsakovu_uzsakymai INNER JOIN Adresai, Kontaktai, Uzsakovai, Busenos WHERE Busenos.id_Busena=Uzsakovu_uzsakymai.fk_busena AND Uzsakovu_uzsakymai.fk_uzsakovas=Uzsakovai.id_Uzsakovas AND Adresai.id_Adresas=Uzsakovai.fk_adresas AND Kontaktai.id_Kontaktas=Uzsakovai.fk_kontaktai";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getAllBuyerOrderEssentialsFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void deleteBuyerOrder(int id) throws SQLException, ClassNotFoundException
    {
        try
        {
            String deleteOrderedGoods = "DELETE FROM Uzsakomos_prekes WHERE fk_uzsakymas='"+id+"'";
            Database.executeUpdate(deleteOrderedGoods);

            String deleteBuyerOrder = "DELETE FROM Uzsakovu_uzsakymai WHERE id_Uzsakymas='"+id+"'";
            Database.executeUpdate(deleteBuyerOrder);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void insertBuyerOrder(BuyerOrder buyerOrder) throws SQLException, ClassNotFoundException
    {
        try
        {
            ObservableList<OrderedGoods> orderedGoods = buyerOrder.getOrderedGoods();

            String orderInsert = "INSERT INTO Uzsakovu_uzsakymai (fk_busena, fk_uzsakovas, data, suma) VALUES('"+buyerOrder.getOrderState().getIdState()+"', '"+buyerOrder.getBuyer().getId()+"', '"+buyerOrder.getDate()+"', '"+buyerOrder.getTotal()+"');";
            long orderId = Database.executeUpdateReturnKey(orderInsert);

            for(int i = 0; i < orderedGoods.size(); i++)
            {
                String goodsInsert = "INSERT INTO Uzsakomos_prekes (fk_preke, fk_uzsakymas, uzsakytas_kiekis) VALUES('"+orderedGoods.get(i).getGoods().getIdGoods()+"', '"+orderId+"', '"+orderedGoods.get(i).getAmount()+"')";
                Database.executeUpdate(goodsInsert);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static BuyerOrder getBuyerOrder(int id) throws SQLException, ClassNotFoundException
    {
        try
        {
            String statement = "SELECT * FROM  Uzsakovu_uzsakymai INNER JOIN Adresai, Kontaktai, Uzsakovai, Busenos WHERE Uzsakovu_uzsakymai.id_Uzsakymas='"+id+"' AND Busenos.id_Busena=Uzsakovu_uzsakymai.fk_busena AND Uzsakovu_uzsakymai.fk_uzsakovas=Uzsakovai.id_Uzsakovas AND Adresai.id_Adresas=Uzsakovai.fk_adresas AND Kontaktai.id_Kontaktas=Uzsakovai.fk_kontaktai";
            ResultSet resultSet = Database.executeQuery(statement);
            BuyerOrder buyerOrder = getBuyerOrderEssentialsFromResultSet(resultSet);

            buyerOrder.setOrderedGoods(OrderedGoodsDAO.getAllOrderedGoodsForOrder(buyerOrder.getId()));

            return  buyerOrder;
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static boolean checkIfBuyerWithIdHasOrders(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Uzsakovu_uzsakymai WHERE fk_uzsakovas='"+id+"'";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        }
        catch (SQLException e)
        {
            throw e;
        }
    }
}
