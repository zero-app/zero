package buyersSub.dataAccessObjects;

import buyersSub.objects.OrderedGoods;
import goodsSub.objects.Goods;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderedGoodsDAO
{
    private static OrderedGoods extractOrderedGoods(ResultSet resultSet) throws SQLException
    {
        int idOrderedGoods = resultSet.getInt("id_Uzsakoma_preke");
        int orderedAmount = resultSet.getInt("uzsakytas_kiekis");

        int idGoods = resultSet.getInt("id_Preke");
        String barcode = resultSet.getString("barkodas");
        int amount = resultSet.getInt("kiekis");
        String name = resultSet.getString("pavadinimas");
        String summary = resultSet.getString("aprasymas");
        String manufacturer = resultSet.getString("gamintojas");
        Double price = resultSet.getDouble("kaina");

        Goods goods = new Goods(idGoods, barcode, amount, name, summary, manufacturer, price);

        return new OrderedGoods(idOrderedGoods, orderedAmount, goods);
    }

    private static ObservableList<OrderedGoods> getAllOrderedGoodsFromResultSet(ResultSet resultSet) throws SQLException
    {
        ObservableList<OrderedGoods> orderedGoodsObservableList = FXCollections.observableArrayList();

        while(resultSet.next())
        {
            orderedGoodsObservableList.add(extractOrderedGoods(resultSet));
        }

        return orderedGoodsObservableList;
    }

    public static ObservableList<OrderedGoods> getAllOrderedGoodsForOrder(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Uzsakomos_prekes INNER JOIN Prekes WHERE Uzsakomos_prekes.fk_uzsakymas='"+id+"' AND Uzsakomos_prekes.fk_preke=Prekes.id_Preke";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getAllOrderedGoodsFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            throw e;
        }
    }

    public static boolean checkIfGoodsWithIdIsOrdered(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Uzsakomos_prekes WHERE fk_preke='"+id+"'";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        }
        catch (SQLException e)
        {
            throw e;
        }
    }
}
