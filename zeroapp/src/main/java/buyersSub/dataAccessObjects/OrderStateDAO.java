package buyersSub.dataAccessObjects;

import buyersSub.objects.OrderState;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderStateDAO
{
    private static OrderState extractOrderState(ResultSet resultSet) throws SQLException
    {
        int idState = resultSet.getInt("id_Busena");
        String stateName = resultSet.getString("busena");

        return new OrderState(idState, stateName);
    }

    private static ObservableList<OrderState> getAllOrderStatesFromResultSet(ResultSet resultSet) throws SQLException
    {
        ObservableList<OrderState> statesObservableList = FXCollections.observableArrayList();

        while(resultSet.next())
        {
            statesObservableList.add(extractOrderState(resultSet));
        }

        return statesObservableList;
    }

    public static ObservableList<OrderState> getAllOrderStates() throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Busenos";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getAllOrderStatesFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
