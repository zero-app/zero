package buyersSub.dataAccessObjects;

import buyersSub.objects.SoldByType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SoldByTypeDAO
{
    private static SoldByType extractSoldByType(ResultSet resultSet) throws SQLException
    {
        String typeName = resultSet.getString("tipo_pavadinimas");
        int soldAmount = resultSet.getInt("sold_amount");
        double grandTotal = resultSet.getDouble("grand_total");

        return new SoldByType(typeName, soldAmount, grandTotal);
    }

    private static ObservableList<SoldByType> getAllSoldByTypeFromResultSet(ResultSet resultSet) throws SQLException
    {
        ObservableList<SoldByType> buyerObservableList = FXCollections.observableArrayList();

        while(resultSet.next())
        {
            buyerObservableList.add(extractSoldByType(resultSet));
        }

        return buyerObservableList;
    }

    public static ObservableList<SoldByType> getAllSoldByType(String dateFrom, String dateTo) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT Prekes_tipai.tipo_pavadinimas, COALESCE(SUM( Uzsakomos_prekes.uzsakytas_kiekis), 0) AS sold_amount,  COALESCE(SUM( Uzsakomos_prekes.uzsakytas_kiekis * Prekes.kaina), 0) AS grand_total\n" +
                "FROM Prekes_tipai\n" +
                "LEFT JOIN Prekes ON Prekes.fk_prekes_tipas = Prekes_tipai.id_Prekes_tipas\n" +
                "LEFT JOIN Uzsakomos_prekes ON Uzsakomos_prekes.fk_preke = Prekes.id_Preke\n " +
                "LEFT JOIN Uzsakovu_uzsakymai ON Uzsakovu_uzsakymai.id_Uzsakymas=Uzsakomos_prekes.fk_uzsakymas\n" +
                "WHERE Uzsakovu_uzsakymai.data BETWEEN '"+dateFrom+"' AND '"+dateTo+"' OR ISNULL(Uzsakovu_uzsakymai.data)\n" +
                "GROUP BY Prekes_tipai.id_Prekes_tipas";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getAllSoldByTypeFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            throw e;
        }
    }
}
