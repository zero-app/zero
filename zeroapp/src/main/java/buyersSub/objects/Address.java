package buyersSub.objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Address
{
    private IntegerProperty id;
    private StringProperty country;
    private StringProperty city;
    private StringProperty street;
    private StringProperty placeNumber;
    private StringProperty postalCode;

    public Address(int id, String country, String city, String street, String placeNumber, String postalCode)
    {
        this.id = new SimpleIntegerProperty(id);
        this.country = new SimpleStringProperty(country);
        this.city = new SimpleStringProperty(city);
        this.street = new SimpleStringProperty(street);
        this.placeNumber = new SimpleStringProperty(placeNumber);
        this.postalCode = new SimpleStringProperty(postalCode);
    }

    public Address(String country, String city, String street, String placeNumber, String postalCode)
    {
        this.country = new SimpleStringProperty(country);
        this.city = new SimpleStringProperty(city);
        this.street = new SimpleStringProperty(street);
        this.placeNumber = new SimpleStringProperty(placeNumber);
        this.postalCode = new SimpleStringProperty(postalCode);
    }

    public void setCountry(String country)
    {
        this.country.set(country);
    }

    public void setCity(String city)
    {
        this.city.set(city);;
    }

    public void setStreet(String street)
    {
        this.street.set(street);
    }

    public void setPlaceNumber(String placeNumber)
    {
        this.placeNumber.set(placeNumber);
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode.set(postalCode);
    }

    public String getCountry()
    {
        return country.get();
    }

    public String getCity()
    {
        return city.get();
    }

    public String getStreet()
    {
        return  street.get();
    }

    public String getPlaceNumber()
    {
        return placeNumber.get();
    }

    public String getPostalCode()
    {
        return postalCode.get();
    }

    public int getId()
    {
        return id.get();
    }

    public StringProperty getCityProperty()
    {
        return city;
    }

    public StringProperty getCountryProperty()
    {
        return country;
    }

    public StringProperty getPlaceNumberProperty()
    {
        return placeNumber;
    }

    public StringProperty getPostalCodeProperty()
    {
        return postalCode;
    }

    public StringProperty getStreetProperty()
    {
        return street;
    }

    public IntegerProperty getIdProperty()
    {
        return id;
    }

}
