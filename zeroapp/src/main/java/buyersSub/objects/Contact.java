package buyersSub.objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Contact
{
    private IntegerProperty id;
    private StringProperty phoneNumber;
    private StringProperty email;

    public Contact(int id, String phoneNumber, String email)
    {
        this.id = new SimpleIntegerProperty(id);
        this.phoneNumber = new SimpleStringProperty(phoneNumber);
        this.email = new SimpleStringProperty(email);
    }

    public Contact(String phoneNumber, String email)
    {
        this.phoneNumber = new SimpleStringProperty(phoneNumber);
        this.email = new SimpleStringProperty(email);
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber.set(phoneNumber);
    }

    public void setEmail(String email)
    {
        this.email.set(email);
    }

    public int getId()
    {
        return id.get();
    }

    public String getPhoneNumber()
    {
        return phoneNumber.get();
    }

    public String getEmail()
    {
        return email.get();
    }

    public IntegerProperty getIdProperty()
    {
        return id;
    }

    public StringProperty getEmailProperty()
    {
        return email;
    }

    public StringProperty getPhoneNumberProperty()
    {
        return phoneNumber;
    }
}
