package buyersSub.objects;

import javafx.beans.property.*;

public class SoldByType
{
    private StringProperty typeName;
    private IntegerProperty amount;
    private DoubleProperty total;

    public SoldByType(String typeName, int amount, double total)
    {
        this.typeName = new SimpleStringProperty(typeName);
        this.amount = new SimpleIntegerProperty(amount);
        this.total = new SimpleDoubleProperty(total);
    }

    public String getTypeName()
    {
        return typeName.get();
    }

    public int getAmount()
    {
        return amount.get();
    }

    public double getTotal()
    {
        return total.get();
    }

    public StringProperty getTypeNameProperty()
    {
        return typeName;
    }

    public IntegerProperty getAmountProperty()
    {
        return amount;
    }

    public DoubleProperty getTotalPropert()
    {
        return total;
    }
}
