package buyersSub.objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Buyer
{
    private IntegerProperty id;
    private StringProperty name;
    private StringProperty vatCode;
    private StringProperty code;

    private Address address;
    private Contact contact;

    public Buyer(int id, String name, String vatCode, String code, Address address, Contact contact)
    {
        this.id = new SimpleIntegerProperty(id);
        this.name = new SimpleStringProperty(name);
        this.vatCode = new SimpleStringProperty(vatCode);
        this.code = new SimpleStringProperty(code);

        this.address = address;
        this.contact = contact;
    }

    public Buyer(String name, String vatCode, String code, Address address, Contact contact)
    {
        this.name = new SimpleStringProperty(name);
        this.vatCode = new SimpleStringProperty(vatCode);
        this.code = new SimpleStringProperty(code);

        this.address = address;
        this.contact = contact;
    }

    public void setName(String name)
    {
        this.name.set(name);
    }

    public void setVatCode(String vatCode)
    {
        this.vatCode.set(vatCode);
    }

    public void setCode(String code)
    {
        this.code.set(code);
    }

    public void setAddress(Address address)
    {
        this.address = address;
    }

    public void setContact(Contact contact)
    {
        this.contact = contact;
    }

    public String getName()
    {
        return name.get();
    }

    public String getVatCode()
    {
        return vatCode.get();
    }

    public String getCode()
    {
        return code.get();
    }

    public Address getAddress()
    {
        return address;
    }

    public Contact getContact()
    {
        return contact;
    }

    public int getId()
    {
        return id.get();
    }

    public IntegerProperty getIdProperty()
    {
        return id;
    }

    public StringProperty getNameProperty()
    {
        return name;
    }

    public StringProperty getVatCodeProperty()
    {
        return vatCode;
    }

    public StringProperty getCodeProperty()
    {
        return code;
    }

    public String toString()
    {
        return code.get() + " " + name.get();
    }
}
