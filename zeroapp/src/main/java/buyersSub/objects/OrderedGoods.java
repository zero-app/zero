package buyersSub.objects;

import goodsSub.objects.Goods;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class OrderedGoods
{
    private IntegerProperty idOrderedGoods;
    private IntegerProperty amount;

    private Goods goods;

    public OrderedGoods(int idOrderedGoods, int amount, Goods goods)
    {
        this.idOrderedGoods = new SimpleIntegerProperty(idOrderedGoods);
        this.amount = new SimpleIntegerProperty(amount);

        this.goods = goods;
    }

    public OrderedGoods(int amount, Goods goods)
    {
        this.amount = new SimpleIntegerProperty(amount);

        this.goods = goods;
    }

    public void setAmount(int amount)
    {
        this.amount.set(amount);
    }

    public void setGoods(Goods goods)
    {
        this.goods = goods;
    }

    public int getIdOrderedGoods()
    {
        return idOrderedGoods.get();
    }

    public int getAmount()
    {
        return amount.get();
    }

    public Goods getGoods()
    {
        return goods;
    }

    public IntegerProperty getIdOrderedGoodsProperty()
    {
        return idOrderedGoods;
    }

    public IntegerProperty getAmountProperty()
    {
        return amount;
    }
}
