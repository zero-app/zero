package buyersSub.objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrderState
{
    private IntegerProperty idState;
    private StringProperty stateName;

    public OrderState(int idState, String stateName)
    {
        this.idState = new SimpleIntegerProperty(idState);
        this.stateName = new SimpleStringProperty(stateName);
    }

    public OrderState(String stateName)
    {
        this.stateName = new SimpleStringProperty(stateName);
    }

    public void setStateName(String stateName)
    {
        this.stateName.set(stateName);
    }

    public int getIdState()
    {
        return idState.get();
    }

    public String getStateName()
    {
        return stateName.getName();
    }

    public IntegerProperty getIdStateProperty()
    {
        return idState;
    }

    public StringProperty getStateNameProperty()
    {
        return stateName;
    }

    public String toString()
    {
        return stateName.get();
    }
}
