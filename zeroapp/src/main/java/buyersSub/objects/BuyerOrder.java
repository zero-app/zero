package buyersSub.objects;

import goodsSub.objects.Goods;
import javafx.beans.property.*;
import javafx.collections.ObservableList;

public class BuyerOrder
{
    private IntegerProperty id;
    private StringProperty date;
    private DoubleProperty total;

    private Buyer buyer;
    private OrderState orderState;

    private ObservableList<OrderedGoods> orderedGoods;

    public BuyerOrder(int id, String date, double total, Buyer buyer, OrderState orderState, ObservableList<OrderedGoods> orderedGoods)
    {
        this.id = new SimpleIntegerProperty(id);
        this.date = new SimpleStringProperty(date);
        this.total = new SimpleDoubleProperty(total);

        this.buyer = buyer;
        this.orderState = orderState;
        this.orderedGoods = orderedGoods;
    }

    public BuyerOrder(int id, String date, double total, Buyer buyer, OrderState orderState)
    {
        this.id = new SimpleIntegerProperty(id);
        this.date = new SimpleStringProperty(date);
        this.total = new SimpleDoubleProperty(total);

        this.buyer = buyer;
        this.orderState = orderState;
    }

    public BuyerOrder(String date, double total, Buyer buyer, OrderState orderState, ObservableList<OrderedGoods> orderedGoods)
    {
        this.date = new SimpleStringProperty(date);
        this.total = new SimpleDoubleProperty(total);

        this.buyer = buyer;
        this.orderState = orderState;
        this.orderedGoods = orderedGoods;
    }

    public void setDate(String date)
    {
        this.date.set(date);
    }

    public void setTotal(double total)
    {
        this.total.set(total);
    }

    public void setBuyer(Buyer buyer)
    {
        this.buyer = buyer;
    }

    public void setOrderState(OrderState orderState)
    {
        this.orderState = orderState;
    }

    public void setOrderedGoods(ObservableList<OrderedGoods> orderedGoods)
    {
        this.orderedGoods = orderedGoods;
    }

    public int getId()
    {
        return id.get();
    }

    public String getDate()
    {
        return date.get();
    }

    public double getTotal()
    {
        return total.get();
    }

    public Buyer getBuyer()
    {
        return buyer;
    }

    public OrderState getOrderState()
    {
        return orderState;
    }

    public ObservableList<OrderedGoods> getOrderedGoods()
    {
        return orderedGoods;
    }

    public IntegerProperty getIdProperty()
    {
        return id;
    }

    public StringProperty getDateProperty()
    {
        return date;
    }

    public DoubleProperty getTotalProperty()
    {
        return total;
    }
}
