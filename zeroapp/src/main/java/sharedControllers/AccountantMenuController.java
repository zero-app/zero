package sharedControllers;

import employee.view.EmployeeListGroup;
import employee.view.EquipmentRequestGroup;
import employee.view.EquipmentRequestReviewGroup;
import employee.view.HolidayRequestGroup;
import employee.view.HolidayRequestReviewGroup;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import main.Main;

public class AccountantMenuController extends ContentSwitcher
{
    @FXML
    public void switchToBuyerListingWindow(ActionEvent event)
    {
        this.switchToBuyerListingWindow();
    }

    @FXML
    public void switchToAddNewBuyerWindow(ActionEvent event)
    {
       this.switchToAddNewBuyerWindow();
    }

    @FXML
    public void switchToGoodsListingWindow(ActionEvent event)
    {
        this.switchToGoodsListingWindow();
    }

    @FXML
    public void switchToNewFoodWindow(ActionEvent event)
    {
        this.switchToFoodEditWindow(null);
    }

    @FXML
    public void switchToNewMonitorWindow(ActionEvent event)
    {
        this.switchToMonitorEditWindow(null);
    }

    @FXML
    public void switchToNewComputerWindow(ActionEvent event)
    {
        this.switchToComputerEditWindow(null);
    }

    @FXML
    public void switchToNewSmartphoneWindow(ActionEvent event)
    {
        this.switchToSmartphoneEditWindow(null);
    }

    @FXML
    public void switchToBuyerOrderListingWindow(ActionEvent event)
    {
        this.switchToBuyerOrderListingWindow();
    }

    @FXML
    public void switchToEditBuyerOrderWindow(ActionEvent event)
    {
        this.switchToBuyerOrderEditWindow(null);
    }

    @FXML
    public void switchToBuyersReportWindow(ActionEvent event)
    {
        this.switchToBuyersReportWindow();
    }

    @FXML
    public void switchToGoodsReportWindow(ActionEvent event)
    {
        this.switchToGoodsReportWindow();
    }

    @FXML
    public void handleExit(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    public void showEmployeeList(){
        double width = Main.getRoot().getWidth();
        double height = Main.getRoot().getHeight();
        this.setCenter(new EmployeeListGroup(width,height));
    }

    @FXML
    public void showEquipmentRequestItem(){
        double width = Main.getRoot().getWidth();
        double height = Main.getRoot().getHeight();
        this.setCenter(new EquipmentRequestGroup(width,height));
    }
    @FXML
    public void showHolidayRequestItem(){
        double width = Main.getRoot().getWidth();
        double height = Main.getRoot().getHeight();
        this.setCenter(new HolidayRequestGroup(width,height));
    }
    @FXML
    public void showHolidayReviewItem(){
        double width = Main.getRoot().getWidth();
        double height = Main.getRoot().getHeight();
        this.setCenter(new HolidayRequestReviewGroup(width,height));
    }
    @FXML
    public void showEquipmentRequestReviewItem() {
        double width = Main.getRoot().getWidth();
        double height = Main.getRoot().getHeight();
        this.setCenter(new EquipmentRequestReviewGroup(width, height));
    }

    @FXML
    public void displaySuppliers(ActionEvent event) {
        super.switchToViewSuppliersWindow();
    }

    @FXML
    public void displayBlacklistedSuppliers(ActionEvent actionEvent) {
        super.switchToViewBlacklistedSuppliers();
    }
}
