package sharedControllers;

import buyersSub.controllers.BuyerEditController;
import buyersSub.controllers.BuyerOrderEditController;
import goodsSub.controllers.ComputerEditController;
import goodsSub.controllers.FoodEditController;
import goodsSub.controllers.MonitorEditController;
import goodsSub.controllers.SmartphoneEditController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import main.Main;
import suppliersSubsystem.controllers.AddSupplierToBlacklistController;
import suppliersSubsystem.controllers.EditBlackListSupplierController;
import suppliersSubsystem.controllers.EditSupplierController;
import suppliersSubsystem.entities.BlackListSupplier;
import suppliersSubsystem.entities.Supplier;

import java.io.IOException;
import java.net.URL;

public class ContentSwitcher
{

    public void setCenter(Group group){
        BorderPane border = Main.getRoot();
        border.setCenter(group);
    }
    public static void switchCenter(Group group){
        BorderPane border = Main.getRoot();
        border.setCenter(group);
    }
    public void switchToBuyerListingWindow()
    {
        try
        {
            URL paneUrl = getClass().getResource("/layout/buyersSub/BuyerListingWindow.fxml");
            AnchorPane pane = FXMLLoader.load( paneUrl );

            BorderPane border = Main.getRoot();
            border.setCenter(pane);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToAddNewBuyerWindow()
    {
        try
        {
            URL paneUrl = getClass().getResource("/layout/buyersSub/BuyerNewWindow.fxml");
            AnchorPane pane = FXMLLoader.load( paneUrl );

            BorderPane border = Main.getRoot();
            border.setCenter(pane);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToEditBuyerWindow(int buyerId)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/buyersSub/BuyerModificationWindow.fxml"));
            Parent pane = loader.load();
            BuyerEditController ctrl = loader.getController();
            ctrl.init(buyerId);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToGoodsListingWindow()
    {
        try
        {
            URL paneUrl = getClass().getResource("/layout/goodsSub/GoodsListingWindow.fxml");
            AnchorPane pane = FXMLLoader.load( paneUrl );

            BorderPane border = Main.getRoot();
            border.setCenter(pane);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToBuyerOrderListingWindow()
    {
        try
        {
            URL paneUrl = getClass().getResource("/layout/buyersSub/BuyerOrderListingWindow.fxml");
            AnchorPane pane = FXMLLoader.load( paneUrl );

            BorderPane border = Main.getRoot();
            border.setCenter(pane);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToFoodEditWindow(Integer foodId)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/goodsSub/FoodEditWindow.fxml"));
            Parent pane = loader.load();
            FoodEditController ctrl = loader.getController();
            ctrl.init(foodId);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToMonitorEditWindow(Integer monitorId)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/goodsSub/MonitorEditWindow.fxml"));
            Parent pane = loader.load();
            MonitorEditController ctrl = loader.getController();
            ctrl.init(monitorId);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToComputerEditWindow(Integer computerId)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/goodsSub/ComputerEditWindow.fxml"));
            Parent pane = loader.load();
            ComputerEditController ctrl = loader.getController();
            ctrl.init(computerId);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToSmartphoneEditWindow(Integer smartphoneId)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/goodsSub/SmartphoneEditWindow.fxml"));
            Parent pane = loader.load();
            SmartphoneEditController ctrl = loader.getController();
            ctrl.init(smartphoneId);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToBuyerOrderEditWindow(Integer buyerOrderId)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/buyersSub/BuyerOrderEditWindow.fxml"));
            Parent pane = loader.load();
            BuyerOrderEditController ctrl = loader.getController();
            ctrl.init(buyerOrderId);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToBuyersReportWindow()
    {
        try
        {
            URL paneUrl = getClass().getResource("/layout/buyersSub/BuyerOrderReportWindow.fxml");
            AnchorPane pane = FXMLLoader.load( paneUrl );

            BorderPane border = Main.getRoot();
            border.setCenter(pane);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToGoodsReportWindow()
    {
        try
        {
            URL paneUrl = getClass().getResource("/layout/goodsSub/PopularGoodsReportWindow.fxml");
            AnchorPane pane = FXMLLoader.load( paneUrl );

            BorderPane border = Main.getRoot();
            border.setCenter(pane);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void switchToViewBlacklistedSuppliers() {
        switchToParticularContentByURL(getClass()
                .getResource("/layout/suppliersSubsystem/ViewBlackListSuppliersWindow.fxml"));
    }

    public void switchToViewSuppliersWindow() {
        switchToParticularContentByURL(getClass()
                .getResource("/layout/suppliersSubsystem/ViewSuppliersWindow.fxml"));
    }

    public void switchToAddNewSupplierWindow() {
        switchToParticularContentByURL(getClass()
                .getResource("/layout/suppliersSubsystem/AddNewSupplierWindow.fxml"));
    }

    public void switchToEditSupplier(Supplier supplier) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/suppliersSubsystem/EditSupplierWindow.fxml"));
            Parent pane = loader.load();
            ((EditSupplierController) loader.getController()).initialize(supplier);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void switchToEditBlacklistSupplier(BlackListSupplier supplier) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/suppliersSubsystem/EditBlacklistSupplierWindow.fxml"));
            Parent pane = loader.load();
            ((EditBlackListSupplierController) loader.getController()).initialize(supplier);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void switchToAddBlacklistSupplier(Supplier supplier) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/suppliersSubsystem/AddBlacklistSupplierWindow.fxml"));
            Parent pane = loader.load();
            ((AddSupplierToBlacklistController) loader.getController()).initialize(supplier);

            BorderPane border = Main.getRoot();
            border.setCenter(pane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void switchToParticularContentByURL(URL contentLocation) {

        try {
            Main.getRoot().setCenter(FXMLLoader.<AnchorPane>load(contentLocation));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
