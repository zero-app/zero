package sharedControllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import main.Main;
import utils.AlertUtil;
import utils.AuthenticationUtil;
import java.io.IOException;
import java.sql.SQLException;

public class LoginController
{
    @FXML
    TextField userNameField;
    @FXML
    PasswordField userPasswordField;
    @FXML
    Button loginButton;

    @FXML
    ImageView loginImage;

    @FXML
    private void initialize()
    {
        userNameField.setPromptText("Username");
        userPasswordField.setPromptText("Password");
    }

    public void processLogin(ActionEvent event)
    {

        try
        {
            if(!AuthenticationUtil.validate(userNameField.getText(), userPasswordField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Error", "Incorrect username or password");
            }
            else
            {
                gotoWelcomeWindow();
                setNavBar(AuthenticationUtil.getLoggedUser().getRole());
            }
        }
        catch (SQLException | ClassNotFoundException e)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get user's information from database");
        }
    }

    private void gotoWelcomeWindow()
    {
        BorderPane border = Main.getRoot();
        try
        {
            border.setCenter(FXMLLoader.load(getClass().getResource("/layout/MainWindow.fxml")));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void setNavBar(int userRole)
    {
        BorderPane border = Main.getRoot();
        try
        {
            switch (userRole)
            {
                case 1:
                    border.setTop(FXMLLoader.load(getClass().getResource("/layout/top/AccountantMenu.fxml")));
                    break;

            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
