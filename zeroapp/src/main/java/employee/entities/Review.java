package employee.entities;

import java.util.Date;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class Review {
	private Integer id;
	private Date from;
	private Date to;
	private Integer improvement;
	private Integer quality;
	private Integer grade;
	private String comment;


	private Date dateOfReview;
	private Integer fkEmployee;
	private Integer fkReviewer;

	public Review() {
	}

	public Review(Date from, Date to, Integer improvement, Integer quality, Integer grade, String comment, Integer fkEmployee, Integer fkReviewer,Date dateOfReview) {
		this.dateOfReview = dateOfReview;
		this.from = from;
		this.to = to;
		this.improvement = improvement;
		this.quality = quality;
		this.grade = grade;
		this.comment = comment;
		this.fkEmployee = fkEmployee;
		this.fkReviewer = fkReviewer;
	}

	public Date getDateOfReview() {
		return dateOfReview;
	}

	public void setDateOfReview(Date dateOfReview) {
		this.dateOfReview = dateOfReview;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Integer getImprovement() {
		return improvement;
	}

	public void setImprovement(Integer improvement) {
		this.improvement = improvement;
	}

	public Integer getQuality() {
		return quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getFkEmployee() {
		return fkEmployee;
	}

	public void setFkEmployee(Integer fkEmployee) {
		this.fkEmployee = fkEmployee;
	}

	public Integer getFkReviewer() {
		return fkReviewer;
	}

	public void setFkReviewer(Integer fkReviewer) {
		this.fkReviewer = fkReviewer;
	}
}
