package employee.entities;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.ValueGenerationType;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class Employee {
	private Integer id;
	private String firstName;
	private String lastName;
	private Long personalIdentificationNumber;
	private String bankAccount;
	private String address;
	private String phoneNumber;
	private String condition;
	private Double salary;
	private Double holiday;
	private Date startedWork;

	public Employee(Date startedWork, String firstName, String lastName, Long personalIdentificationNumber, String bankAccount, String address, String phoneNumber, String condition, Double salary, Double holiday) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.personalIdentificationNumber = personalIdentificationNumber;
		this.bankAccount = bankAccount;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.condition = condition;
		this.salary = salary;
		this.holiday = holiday;
		this.startedWork = startedWork;
	}

	public Employee() {
	}

	public Employee(String firstName, String lastName, Long personalIdentificationNumber, String bankAccount, String address, String phoneNumber, String condition, Double salary, Double holiday) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.personalIdentificationNumber = personalIdentificationNumber;
		this.bankAccount = bankAccount;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.condition = condition;
		this.salary = salary;
		this.holiday = holiday;
	}

	public Date getStartedWork() {
		return startedWork;
	}

	public void setStartedWork(Date startedWork) {
		this.startedWork = startedWork;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getPersonalIdentificationNumber() {
		return personalIdentificationNumber;
	}

	public void setPersonalIdentificationNumber(Long personalIdentificationNumber) {
		this.personalIdentificationNumber = personalIdentificationNumber;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Double getHoliday() {
		return holiday;
	}

	public void setHoliday(Double holiday) {
		this.holiday = holiday;
	}
}
