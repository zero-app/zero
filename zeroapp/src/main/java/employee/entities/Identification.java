package employee.entities;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class Identification {
	private Integer id;
	private String number;
	private String department;
	private String position;
	private Double workinHours;
	private String shift;
	private Integer fkEmployee;
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Identification() {
	}

	public Identification(Integer id, String number, String department, String position, Double workinHours, String shift, Integer fkEmployee) {
		this.id = id;
		this.number = number;
		this.department = department;
		this.position = position;
		this.workinHours = workinHours;
		this.shift = shift;
		this.fkEmployee = fkEmployee;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Double getWorkinHours() {
		return workinHours;
	}

	public void setWorkinHours(Double workinHours) {
		this.workinHours = workinHours;
	}

	public String getShift() {
		return shift;
	}

	public void setShift(String shift) {
		this.shift = shift;
	}

	public Integer getFkEmployee() {
		return fkEmployee;
	}

	public void setFkEmployee(Integer fkEmployee) {
		this.fkEmployee = fkEmployee;
	}
}
