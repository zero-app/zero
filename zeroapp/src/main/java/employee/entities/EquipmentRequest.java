package employee.entities;

import java.util.Date;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class EquipmentRequest {
	private Integer id;
	private Date date;
	private String comment;
	private String condition;
	private Integer fkEmployee;

	public EquipmentRequest() {
	}

	public EquipmentRequest(Integer id, Date date, String comment, String condition, Integer fkEmployee) {
		this.id = id;
		this.date = date;
		this.comment = comment;
		this.condition = condition;
		this.fkEmployee = fkEmployee;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Integer getFkEmployee() {
		return fkEmployee;
	}

	public void setFkEmployee(Integer fkEmployee) {
		this.fkEmployee = fkEmployee;
	}
}
