package employee.entities;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class Equipment {
	private Integer id;
	private String name;
	private String type;

	public Equipment() {
	}

	public Equipment(Integer id, String name, String type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
