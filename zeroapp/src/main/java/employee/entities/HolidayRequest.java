package employee.entities;

import java.util.Date;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class HolidayRequest {
	private Integer id;
	private Date from;
	private Date to;
	private String comment;
	private String condition;
	private Integer fkEmployee;

	public HolidayRequest() {
	}

	public HolidayRequest(Integer id, Date from, Date to, String comment, String condition, Integer fkEmployee) {
		this.id = id;
		this.from = from;
		this.to = to;
		this.comment = comment;
		this.condition = condition;
		this.fkEmployee = fkEmployee;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Integer getFkEmployee() {
		return fkEmployee;
	}

	public void setFkEmployee(Integer fkEmployee) {
		this.fkEmployee = fkEmployee;
	}
}
