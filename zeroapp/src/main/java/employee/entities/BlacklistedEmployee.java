package employee.entities;

import java.util.Date;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class BlacklistedEmployee {
	private Integer id;
	private Date employedFrom;
	private Date employedTo;
	private String comment;
	private Integer fkEmployee;

	public BlacklistedEmployee() {
	}

	public BlacklistedEmployee(Date employedFrom, Date employedTo, String comment, Integer fkEmployee) {
		this.employedFrom = employedFrom;
		this.employedTo = employedTo;
		this.comment = comment;
		this.fkEmployee = fkEmployee;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEmployedFrom() {
		return employedFrom;
	}

	public void setEmployedFrom(Date employedFrom) {
		this.employedFrom = employedFrom;
	}

	public Date getEmployedTo() {
		return employedTo;
	}

	public void setEmployedTo(Date employedTo) {
		this.employedTo = employedTo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getFkEmployee() {
		return fkEmployee;
	}

	public void setFkEmployee(Integer fkEmployee) {
		this.fkEmployee = fkEmployee;
	}
}
