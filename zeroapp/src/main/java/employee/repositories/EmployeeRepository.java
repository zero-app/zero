package employee.repositories;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import employee.entities.BlacklistedEmployee;
import employee.entities.Employee;
import employee.entities.Identification;
import employee.entities.Review;
import employee.viewModels.EmployeeViewModel;
import employee.viewModels.ReviewViewModel;

import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_ACTIVE;
import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_INACTIVE;
import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_SUSPENDED;
import static employee.utils.ConstantValues.STATUS_ACTIVE;

/**
 * Created by Aivaras on 12/15/2016.
 */
public class EmployeeRepository {
	private static SessionFactory factory;
	private static SessionFactory getFactory(){
		if(factory == null){
			factory =  new Configuration().configure().buildSessionFactory();
		}
		return factory;
	}

	public static Integer addNewOrUpdateEmployee(Employee employee) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		Integer id = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return id;
	}

	public static List<EmployeeViewModel> getAllEmployees() {
		Session session = getFactory().openSession();
		Transaction tx = null;
		List<EmployeeViewModel> employeeViewModels = new ArrayList<>();
		try {
			tx = session.beginTransaction();
			List<Employee> list = session.createQuery("FROM Employee E where E.condition = :status_value_active or E.condition = :status_value_suspended or E.condition = :status_value_inactive")
				.setParameter("status_value_active",EMPLOYEE_CONDITION_ACTIVE)
				.setParameter("status_value_suspended", EMPLOYEE_CONDITION_SUSPENDED)
				.setParameter("status_value_inactive", EMPLOYEE_CONDITION_INACTIVE).list();
			for (int i = 0; i < list.size(); i++) {
				Employee e = list.get(i);
				List<Identification> identificationList = session.createQuery("FROM Identification I where I.fkEmployee = :employee_id and I.status = :status_value_active")
					.setParameter("status_value_active", EMPLOYEE_CONDITION_ACTIVE)
					.setParameter("employee_id", e.getId()).list();
				String department;
				if(identificationList == null || identificationList.isEmpty()) {
					department = null;
				}else {
					department = identificationList.get(0).getDepartment();
				}
				employeeViewModels.add(new EmployeeViewModel(e.getId(), e.getFirstName(), e.getLastName(),department ));

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return employeeViewModels;
	}


	public static Identification getActiveIdentification(Integer id) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		List<Identification> list = null;
		try {
			tx = session.beginTransaction();
			list = session.createQuery("FROM Identification I where I.fkEmployee = :employee_id and I.status = :active_status")
				.setParameter("active_status", STATUS_ACTIVE)
				.setParameter("employee_id", id).list();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		if(list == null || list.isEmpty()) return null;
		return list.get(0);
	}

	public static void changeIdentityStatus(Identification identification) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(identification);
		}catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static void saveIdentification(Identification identification) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(identification);
		}catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static Employee getEmployeeById(Integer id) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		Employee employee = null;
		try {
			tx = session.beginTransaction();
			employee =session.find(Employee.class,id);
		}catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return employee;
	}

	public static void addBlacklistedEmployee(BlacklistedEmployee bl) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(bl);
		}catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static void addNewReview(Review review) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(review);
		}catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static List<ReviewViewModel> getReviewList() {
		Session session = getFactory().openSession();
		Transaction tx = null;
		List<ReviewViewModel> reviewViewModels = new ArrayList<>();
		try {
			tx = session.beginTransaction();
			List<Review> list = session.createQuery("from Review ").list();
			for(Review r: list){
				Employee e = (Employee) session.createQuery("from Employee e where e.id = :id").setParameter("id",r.getFkEmployee()).list().get(0);
				String[] from = r.getFrom().toString().split(" ");
				String[] to = r.getTo().toString().split(" ");
				String dateString = from[0]+ " " + to[0];
				reviewViewModels.add(new ReviewViewModel(e.getFirstName()+" "+e.getLastName(),dateString,r.getQuality(),r.getGrade(),r.getImprovement(),r.getComment()));
			}
			return reviewViewModels;
		}catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return reviewViewModels;
	}
}
