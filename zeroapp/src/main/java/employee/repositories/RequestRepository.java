package employee.repositories;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import employee.entities.Equipment;
import employee.entities.EquipmentRequest;
import employee.entities.HolidayRequest;
import employee.viewModels.EquipmentViewModel;

import static employee.utils.ConstantValues.REQUEST_CONDITION_APPROVED;
import static employee.utils.ConstantValues.REQUEST_CONDITION_DECLINED;

/**
 * Created by Aivaras on 12/22/2016.
 */
public class RequestRepository {
	private static SessionFactory factory;
	private static SessionFactory getFactory() {
		if (factory == null) {
			factory = new Configuration().configure().buildSessionFactory();
		}
		return factory;
	}
		public static List<EquipmentViewModel> getEquipmentList(){
			Session session = getFactory().openSession();
			Transaction tx = null;
			List<Equipment> list = new ArrayList<>();
			try {
				tx = session.beginTransaction();
				list = session.createQuery("from Equipment").list();
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
			List<EquipmentViewModel> result = new ArrayList<>();
			for (Equipment e:list){
				result.add(new EquipmentViewModel(e.getName(),e.getType()));
			}
		return result;
	}

	public static void saveNewEquipmentRequest(EquipmentRequest equipmentRequest) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(equipmentRequest);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static List<EquipmentRequest> getEquipmentRequestList() {
		Session session = getFactory().openSession();
		Transaction tx = null;
		List<EquipmentRequest> list = new ArrayList<>();
		try {
			tx = session.beginTransaction();
			list = session.createQuery("from EquipmentRequest ").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	public static void updateEquipmentRequest(EquipmentRequest equipmentRequest) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(equipmentRequest);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static void acceptEquipmentRequest(Integer id) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			EquipmentRequest er = session.get(EquipmentRequest.class,id);
			er.setCondition(REQUEST_CONDITION_APPROVED);
			session.update(er);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static void declineEquipmentRequest(Integer id) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			EquipmentRequest er = session.get(EquipmentRequest.class,id);
			er.setCondition(REQUEST_CONDITION_DECLINED);
			session.update(er);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static void saveNewHolidayRequest(HolidayRequest holidayRequest) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(holidayRequest);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static List<HolidayRequest> getHolidayRequestList() {
		Session session = getFactory().openSession();
		Transaction tx = null;
		List<HolidayRequest> list = new ArrayList<>();
		try {
			tx = session.beginTransaction();
			list = session.createQuery("from HolidayRequest ").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	public static void changeHolidayStatus(Integer id, String requestCondition) {
		Session session = getFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			HolidayRequest er = session.get(HolidayRequest.class,id);
			er.setCondition(requestCondition);
			session.update(er);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
