package employee.viewModels;

import java.time.LocalDate;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class EquipmentRequestViewModel {
	private Integer id;
	private StringProperty fullName;
	private StringProperty date;
	private StringProperty message;
	private StringProperty condition;

	public EquipmentRequestViewModel(String fullName, String date, String message, String condition) {
		this.setFullName(fullName);
		this.setDate(date);
		this.setMessage(message);
		this.setCondition(condition);
	}

	public EquipmentRequestViewModel(Integer id,String fullName, String date, String message, String condition) {
		this.setFullName(fullName);
		this.id = id;
		this.setDate(date);
		this.setMessage(message);
		this.setCondition(condition);
	}

	public String getFullName() {
		return fullNameProperty().get();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StringProperty fullNameProperty() {
		if(fullName == null){
			fullName = new SimpleStringProperty();
		}
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullNameProperty().set(fullName);
	}

	public String getDate() {
		return dateProperty().get();
	}

	public StringProperty dateProperty() {

		if(date == null){
			date = new SimpleStringProperty();
		}
		return date;
	}

	public void setDate(String date) {
		this.dateProperty().set(date);
	}

	public String getMessage() {
		return messageProperty().get();
	}

	public StringProperty messageProperty() {
		if(message == null){
			message = new SimpleStringProperty();
		}
		return message;
	}

	public void setMessage(String message) {
		this.messageProperty().set(message);
	}

	public String getCondition() {
		return conditionProperty().get();
	}

	public StringProperty conditionProperty() {
		if(condition == null){
			condition = new SimpleStringProperty();
		}
		return condition;
	}

	public void setCondition(String condition) {
		this.conditionProperty().set(condition);
	}
}
