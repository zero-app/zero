package employee.viewModels;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class EmployeeViewModel {
	private int id;
	private StringProperty name;
	private StringProperty lastName;
	private StringProperty department;

	public EmployeeViewModel(int id, String name, String lastName, String department) {
		this.id = id;
		setName(name);
		setLastName(lastName);
		setDepartment(department);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return nameProperty().get();
	}

	public StringProperty nameProperty() {
		if(name == null){
			name = new SimpleStringProperty();
		}
		return name;
	}

	public String getLastName() {
		return lastNameProperty().get();
	}

	public StringProperty lastNameProperty() {
		if(lastName == null){
			lastName = new SimpleStringProperty();
		}
		return lastName;
	}

	public String getDepartment() {
		return departmentProperty().get();
	}

	public StringProperty departmentProperty() {
		if(department == null){
			department = new SimpleStringProperty();
		}
		return department;
	}

	public void setName(String name) {
		this.nameProperty().set(name);
	}

	public void setLastName(String lastName) {
		this.lastNameProperty().set(lastName);
	}

	public void setDepartment(String department) {
		this.departmentProperty().set(department);
	}
}
