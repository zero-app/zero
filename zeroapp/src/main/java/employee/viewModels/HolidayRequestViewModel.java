package employee.viewModels;

import employee.entities.HolidayRequest;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class HolidayRequestViewModel {
	private StringProperty fullName;
	private StringProperty datesRequested;
	private StringProperty comment;
	private StringProperty status;
	private Integer id;

	public HolidayRequestViewModel(Integer id, String fullName, String datesRequested, String comment, String status) {
		this.id = id;
		this.setFullName(fullName);
		this.setDatesRequested(datesRequested);
		this.setComment(comment);
		this.setStatus(status);
	}

	public String getFullName() {
		return fullNameProperty().get();
	}

	public StringProperty fullNameProperty() {
		if(fullName == null){
			fullName = new SimpleStringProperty();
		}
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullNameProperty().set(fullName);
	}

	public String getDatesRequested() {
		return datesRequestedProperty().get();
	}

	public StringProperty datesRequestedProperty() {
		if(datesRequested == null){
			datesRequested = new SimpleStringProperty();
		}
		return datesRequested;
	}

	public void setDatesRequested(String datesRequested) {
		this.datesRequestedProperty().set(datesRequested);
	}

	public String getComment() {
		return commentProperty().get();
	}

	public StringProperty commentProperty() {
		if(comment == null){
			comment = new SimpleStringProperty();
		}
		return comment;
	}

	public void setComment(String comment) {
		this.commentProperty().set(comment);
	}

	public String getStatus() {
		return statusProperty().get();
	}

	public StringProperty statusProperty() {
		if(status == null){
			status = new SimpleStringProperty();
		}
		return status;
	}

	public void setStatus(String status) {
		this.statusProperty().set(status);
	}

	public Integer getId() {
		return id;
	}
}
