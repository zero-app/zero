package employee.viewModels;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Aivaras on 12/23/2016.
 */
public class ReviewViewModel {
	/*private Integer id;
	private Date from;
	private Date to;
	private Integer improvement;
	private Integer quality;
	private Integer grade;
	private String comment;


	private Date dateOfReview;
	private Integer fkEmployee;
	private Integer fkReviewer;*/
	private StringProperty nameProperty;
	private StringProperty dateStringProperty;
	private IntegerProperty qualityProperty;
	private IntegerProperty gradeProperty;
	private IntegerProperty improvementProperty;
	private StringProperty commentProperty;

	public ReviewViewModel(String name, String dateString, Integer quality, Integer grade, Integer improvement, String comment) {
		this.nameProperty = new SimpleStringProperty(name);
		this.dateStringProperty = new SimpleStringProperty(dateString);
		this.qualityProperty = new SimpleIntegerProperty(quality);
		this.gradeProperty = new SimpleIntegerProperty(grade);
		this.improvementProperty = new SimpleIntegerProperty(improvement);
		this.commentProperty = new SimpleStringProperty(comment);
	}

	public String getNameProperty() {
		return nameProperty.get();
	}

	public StringProperty namePropertyProperty() {
		return nameProperty;
	}

	public void setNameProperty(String nameProperty) {
		this.nameProperty.set(nameProperty);
	}

	public String getDateStringProperty() {
		return dateStringProperty.get();
	}

	public StringProperty dateStringPropertyProperty() {
		return dateStringProperty;
	}

	public void setDateStringProperty(String dateStringProperty) {
		this.dateStringProperty.set(dateStringProperty);
	}

	public int getQualityProperty() {
		return qualityProperty.get();
	}

	public IntegerProperty qualityPropertyProperty() {
		return qualityProperty;
	}

	public void setQualityProperty(int qualityProperty) {
		this.qualityProperty.set(qualityProperty);
	}

	public int getGradeProperty() {
		return gradeProperty.get();
	}

	public IntegerProperty gradePropertyProperty() {
		return gradeProperty;
	}

	public void setGradeProperty(int gradeProperty) {
		this.gradeProperty.set(gradeProperty);
	}

	public int getImprovementProperty() {
		return improvementProperty.get();
	}

	public IntegerProperty improvementPropertyProperty() {
		return improvementProperty;
	}

	public void setImprovementProperty(int improvementProperty) {
		this.improvementProperty.set(improvementProperty);
	}

	public String getCommentProperty() {
		return commentProperty.get();
	}

	public StringProperty commentPropertyProperty() {
		return commentProperty;
	}

	public void setCommentProperty(String commentProperty) {
		this.commentProperty.set(commentProperty);
	}
}
