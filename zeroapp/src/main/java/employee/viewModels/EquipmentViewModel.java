package employee.viewModels;

import java.io.Serializable;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class EquipmentViewModel {
	private StringProperty name;
	private StringProperty type;



	public EquipmentViewModel(String name, String type) {
		this.setName(name);
		this.setType(type);
	}

	public String getName() {
		return nameProperty().get();
	}

	public StringProperty nameProperty() {
		if(name == null){
			name = new SimpleStringProperty();
		}
		return name;
	}

	public void setName(String name) {
		this.nameProperty().set(name);
	}

	public String getType() {
		return typeProperty().get();
	}

	public StringProperty typeProperty() {
		if(type == null){
			type = new SimpleStringProperty();
		}
		return type;
	}

	public void setType(String type) {
		this.typeProperty().set(type);
	}


}
