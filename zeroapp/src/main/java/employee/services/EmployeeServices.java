package employee.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import employee.entities.BlacklistedEmployee;
import employee.entities.Employee;
import employee.entities.Identification;
import employee.entities.Review;
import employee.repositories.EmployeeRepository;
import employee.viewModels.EmployeeViewModel;
import employee.viewModels.ReviewViewModel;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class EmployeeServices {

	public static List<EmployeeViewModel> getEmployeeList() {
		List<EmployeeViewModel> list =  EmployeeRepository.getAllEmployees();
		return list;
	}

	public static Employee getEmployee(Integer id) {
		Employee e = EmployeeRepository.getEmployeeById(id);
		return e;
	}

	public static void saveOrUpdateEmployee(Employee employee) {
		EmployeeRepository.addNewOrUpdateEmployee(employee);
	}

	public static Identification generateNewIdentification(Identification identification) {
		Identification identity = identification;
		Long timestamp = Calendar.getInstance().getTimeInMillis();
		identity.setNumber(identity.getDepartment()+identity.getPosition()+timestamp);
		return identity;
	}

	public static void saveIdentification(Identification identification) {
		EmployeeRepository.saveIdentification(identification);

	}

	public static Identification getEmployeeIdentification(Integer id) {
		Identification identification = EmployeeRepository.getActiveIdentification(id);
		return identification;
	}

	public static void removeIdentification(Identification identification) {
		EmployeeRepository.changeIdentityStatus(identification);
	}

	public static void addToBlackList(Employee employee, String text) {
		Date today = Calendar.getInstance().getTime();
		BlacklistedEmployee bl = new BlacklistedEmployee(employee.getStartedWork(),today,text,employee.getId());
		EmployeeRepository.addBlacklistedEmployee(bl);
	}

	public static void saveNewReview(Review review) {
		EmployeeRepository.addNewReview(review);
	}

	public static List<ReviewViewModel> getReviews() {
		return EmployeeRepository.getReviewList();
	}
}
