package employee.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import employee.entities.Employee;
import employee.entities.EquipmentRequest;
import employee.entities.HolidayRequest;
import employee.repositories.RequestRepository;
import employee.viewModels.EquipmentRequestViewModel;
import employee.viewModels.HolidayRequestViewModel;

import static employee.utils.ConstantValues.REQUEST_CONDITION_APPROVED;
import static employee.utils.ConstantValues.REQUEST_CONDITION_DECLINED;
import static employee.utils.HolidayStatus.WAITING;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class ReviewServices {
	public static List<HolidayRequestViewModel> getHolidayRequestList() {
		List<HolidayRequestViewModel> list = new ArrayList<>();
		List<HolidayRequest> hrl = RequestRepository.getHolidayRequestList();
		for (HolidayRequest h :hrl){

			Employee e = EmployeeServices.getEmployee(h.getFkEmployee());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

			list.add(new HolidayRequestViewModel(h.getId(),e.getFirstName()+" "+ e.getLastName(),df.format(h.getFrom()) +" - "+ df.format(h.getTo()),h.getComment(),h.getCondition()));
		}

		return list;
	}

	public static void acceptHolidayRequest(Integer id) {
		RequestRepository.changeHolidayStatus(id,REQUEST_CONDITION_APPROVED);
	}

	public static void declineHolidayRequest(Integer id) {

		RequestRepository.changeHolidayStatus(id,REQUEST_CONDITION_DECLINED);
	}


	public static List<EquipmentRequestViewModel> getEquipmentRequestList() {
		List<EquipmentRequestViewModel> list = new ArrayList<>();
		List<EquipmentRequest> rl = RequestRepository.getEquipmentRequestList();
		for(EquipmentRequest r : rl){
			Employee e = EmployeeServices.getEmployee(r.getFkEmployee());
			list.add(new EquipmentRequestViewModel(r.getId(),e.getFirstName()+" "+e.getLastName(),r.getDate().toString(),r.getComment(),r.getCondition()));

		}
		return list;
	}

	public static void acceptEquipmentRequest(Integer id) {
		RequestRepository.acceptEquipmentRequest(id);
	}

	public static void declineEquipmentRequest(Integer id) {

		RequestRepository.declineEquipmentRequest(id);
	}
}
