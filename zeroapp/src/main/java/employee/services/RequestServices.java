package employee.services;

import java.util.ArrayList;
import java.util.List;

import employee.entities.EquipmentRequest;
import employee.entities.HolidayRequest;
import employee.repositories.RequestRepository;
import employee.viewModels.EquipmentViewModel;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class RequestServices {
	public static List<EquipmentViewModel> getAvailableEquipmentViewModels() {
		List<EquipmentViewModel> list;
		list = RequestRepository.getEquipmentList();
		return list;
	}

	public static void submitEquipmentRequest(EquipmentRequest equipmentRequest) {
		RequestRepository.saveNewEquipmentRequest(equipmentRequest);
	}

	public static void submitHolidayRequest(HolidayRequest holidayRequest) {
		RequestRepository.saveNewHolidayRequest(holidayRequest);
	}

	public static int getFirstYearOfRecords() {
		//// TODO: 12/15/2016 return the year of first record in work day table
		return 1970;
	}

}
