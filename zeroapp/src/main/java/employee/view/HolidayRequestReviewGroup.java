package employee.view;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import employee.entities.HolidayRequest;
import employee.services.ReviewServices;
import employee.viewModels.HolidayRequestViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sharedControllers.ContentSwitcher;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class HolidayRequestReviewGroup extends Group {
	private HolidayRequest holidayRequest;
	private TableView<HolidayRequestViewModel> requestTable;
	private DatePicker fromDatePicker;
	private DatePicker toDatePicker;
	private TextArea commentTextArea;
	private Button acceptButton;
	private Button declineButton;
	HolidayRequestViewModel model;
	private Double width;
	private Double height;

	public HolidayRequestReviewGroup(Double width, Double height) {
		this.width = width;
		this.height = height;
		final ObservableList<HolidayRequestViewModel> holidayOL = FXCollections.observableArrayList(ReviewServices.getHolidayRequestList());

		commentTextArea = new TextArea();
		commentTextArea.setMinSize(450, 150);
		commentTextArea.setEditable(false);

		fromDatePicker = new DatePicker();
		toDatePicker = new DatePicker();
		toDatePicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DateCell() {
					@Override
					public void updateItem(LocalDate item, boolean empty) {
						super.updateItem(item, empty);
						setDisable(true);
					}
				};
			}
		});
		fromDatePicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DateCell() {
					@Override
					public void updateItem(LocalDate item, boolean empty) {
						super.updateItem(item, empty);
						setDisable(true);
					}
				};
			}
		});
		fromDatePicker.setEditable(false);
		toDatePicker.setEditable(false);
		HBox dateBox = new HBox(10, new VBox(5, new Label("From date"), fromDatePicker), new VBox(5, new Label("To date"), toDatePicker));

		TableView<HolidayRequestViewModel> tableView = new TableView<>(holidayOL);
		TableColumn<HolidayRequestViewModel, String> nameColumn = new TableColumn<>("Name");
		nameColumn.setPrefWidth(150);
		nameColumn.setCellValueFactory(new PropertyValueFactory<HolidayRequestViewModel, String>("fullName"));
		TableColumn<HolidayRequestViewModel, String> datesColumn = new TableColumn<>("Dates requested");
		datesColumn.setPrefWidth(150);
		datesColumn.setCellValueFactory(new PropertyValueFactory<HolidayRequestViewModel, String>("datesRequested"));
		TableColumn<HolidayRequestViewModel, String> statusColumn = new TableColumn<>("Status");
		statusColumn.setPrefWidth(150);
		statusColumn.setCellValueFactory(new PropertyValueFactory<HolidayRequestViewModel, String>("status"));
		tableView.getColumns().addAll(nameColumn, datesColumn, statusColumn);
		tableView.setOnMouseClicked(event -> {
			if (tableView.getSelectionModel().getSelectedItem() == null) return;
			model = tableView.getSelectionModel().getSelectedItem();
			commentTextArea.setText(model.getComment());
			String[] from = model.getDatesRequested().split("-")[0].trim().split("/");
			Integer fromYear =Integer.valueOf(from[2].trim());
			Integer fromMonth = Integer.valueOf(from[0].trim());
			Integer fromDay = Integer.valueOf(from[1].trim());
			fromDatePicker.setValue(LocalDate.of(fromYear,fromMonth , fromDay));
			String[] to = model.getDatesRequested().split("-")[1].trim().split("/");

			Integer toYear =Integer.valueOf(to[2].trim());
			Integer toMonth = Integer.valueOf(to[0].trim());
			Integer toDay = Integer.valueOf(to[1].trim());
			toDatePicker.setValue(LocalDate.of(toYear, toMonth, toDay));
		});
		acceptButton = new Button("Accept");
		acceptButton.setOnAction(event -> {

			ReviewServices.acceptHolidayRequest(model.getId());
			ContentSwitcher.switchCenter(new HolidayRequestReviewGroup(width,height));
		});
		declineButton = new Button("Decline");
		declineButton.setOnAction(event -> {
			ReviewServices.declineHolidayRequest(model.getId());
			ContentSwitcher.switchCenter(new HolidayRequestReviewGroup(width,height));
		});
		HBox mainView = new HBox(10, tableView, new VBox(10, dateBox, commentTextArea, new HBox(10, acceptButton, declineButton)));

		this.getChildren().addAll(mainView);
	}
}
