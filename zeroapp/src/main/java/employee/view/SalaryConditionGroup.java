package employee.view;

import employee.entities.Employee;
import employee.services.EmployeeServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sharedControllers.ContentSwitcher;

import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_ACTIVE;
import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_INACTIVE;
import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_SUSPENDED;
import static employee.utils.ConstantValues.MEDIUM_FIELD_WIDTH;
import static employee.utils.ConstantValues.MEDIUM_LABEL_WIDTH;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class SalaryConditionGroup extends Group {
	private TextField employeeName;
	private TextField employeeLastName;
	private ComboBox<String> employeeCondition;
	private Button saveButton;
	private Button cancelButton;

	private Double width;
	private Double height;
	private Employee employee;

	public SalaryConditionGroup(Double width, Double height, Integer id) {
		this.width = width;
		this.height = height;
		employee = EmployeeServices.getEmployee(id);

		employeeName = new TextField(employee.getFirstName());
		employeeName.setMinWidth(MEDIUM_FIELD_WIDTH);
		employeeLastName = new TextField(employee.getLastName());
		employeeLastName.setMinWidth(MEDIUM_FIELD_WIDTH);
		final ObservableList<String> conditionList = FXCollections.observableArrayList(EMPLOYEE_CONDITION_ACTIVE,EMPLOYEE_CONDITION_SUSPENDED,EMPLOYEE_CONDITION_INACTIVE);
		employeeCondition = new ComboBox<>(conditionList);
		employeeCondition.setMinWidth(MEDIUM_FIELD_WIDTH);
		Label nameLabel = new Label("First name");
		nameLabel.setMinWidth(MEDIUM_LABEL_WIDTH);
		Label lastNameLabel = new Label("Last Name");
		lastNameLabel.setMinWidth(MEDIUM_LABEL_WIDTH);
		Label conditionLabel = new Label("Condition");
		conditionLabel.setMinWidth(MEDIUM_LABEL_WIDTH);
		HBox nameHBox = new HBox(10,nameLabel,employeeName);
		HBox lastNameHBox = new HBox(10,lastNameLabel,employeeLastName);
		HBox conditionHBox = new HBox(10,conditionLabel,employeeCondition);

		saveButton = new Button("Save");
		saveButton.setOnAction(event -> onButtonClick(event));
		cancelButton = new Button("Cancel");
		cancelButton.setOnAction(event -> onButtonClick(event));
		HBox buttonBox = new HBox(10,saveButton,cancelButton);
		VBox mainView = new VBox(10,nameHBox,lastNameHBox,conditionHBox,buttonBox);
		this.getChildren().add(mainView);
	}

	private void onButtonClick(Event event){
		switch (((Button)event.getSource()).getText()){
			case ("Save"):
				employee.setCondition(employeeCondition.getSelectionModel().getSelectedItem().trim());
				EmployeeServices.saveOrUpdateEmployee(employee);
				ContentSwitcher.switchCenter(new EmployeeListGroup(width,height));
				break;
			case ("Cancel"):
				ContentSwitcher.switchCenter(new EmployeeListGroup(width,height));
				break;
		}
	}
}
