package employee.view;

import com.sun.istack.internal.Nullable;

import employee.entities.Employee;
import employee.entities.Identification;
import employee.services.EmployeeServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sharedControllers.ContentSwitcher;

import static employee.utils.ConstantValues.MEDIUM_FIELD_WIDTH;
import static employee.utils.ConstantValues.MEDIUM_LABEL_WIDTH;
import static employee.utils.ConstantValues.PREF_WINDOW_HEIGHT;
import static employee.utils.ConstantValues.PREF_WINDOW_WIDTH;
import static employee.utils.ConstantValues.STATUS_ACTIVE;
import static employee.utils.ConstantValues.STATUS_INACTIVE;
import static employee.view.custom.CustomViews.float_Validation;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class IdentificationGroup extends Group{
	/**
	 * private Integer id;
	 private String number;
	 private String department;
	 private String position;
	 private String workinHours;
	 private String shift;
	 private Integer fkEmployee;
	 */

	private TextField numberField;
	private ComboBox<String> departmentBox;
	private ComboBox<String> positionBox;
	private TextField monthlyHourField;
	private ComboBox<String> shiftBox;
	private Identification identification;
	Double width;
	Double height;
	Integer id;

	public IdentificationGroup(@Nullable Integer id, String fullName,Double width, Double height) {
		super();
		this.width = width;
		this.height = height;
		this.id = id;
		identification = EmployeeServices.getEmployeeIdentification(id);
		numberField = new TextField();
		numberField.setMinWidth(MEDIUM_FIELD_WIDTH);
		Label numberLabel = new Label("Identification number");
		numberLabel.setMinWidth(MEDIUM_LABEL_WIDTH);
		HBox numberHBox = new HBox(10);
		numberHBox.getChildren().addAll(numberLabel,numberField);

		monthlyHourField = new TextField();
		monthlyHourField.setMinWidth(MEDIUM_FIELD_WIDTH);
		Label monthlyHourLabel = new Label("Monthly working hours");
		monthlyHourLabel.setMinWidth(MEDIUM_LABEL_WIDTH);
		HBox monthlyHourHBox = new HBox(10);
		monthlyHourHBox.getChildren().addAll(monthlyHourLabel,monthlyHourField);

		final ObservableList<String> departmentList = FXCollections.observableArrayList("HR","IT","Sales","Hygiene","Delivery");
		departmentBox = new ComboBox<>(departmentList);
		departmentBox.setMinWidth(MEDIUM_FIELD_WIDTH);
		Label departmentLabel = new Label("Department");
		departmentLabel.setMinWidth(MEDIUM_LABEL_WIDTH);
		HBox departmentHBox = new HBox(10);
		departmentHBox.getChildren().addAll(departmentLabel,departmentBox);
		
		final ObservableList<String> positionList = FXCollections.observableArrayList("Manager", "Team Leader", "Senior Member", "Member", "Owner", "Administrator");
		positionBox = new ComboBox<>(positionList);
		positionBox.setMinWidth(MEDIUM_FIELD_WIDTH);
		Label positionLabel = new Label("Position");
		positionLabel.setMinWidth(MEDIUM_LABEL_WIDTH);
		HBox positionHBox = new HBox(10);
		positionHBox.getChildren().addAll(positionLabel,positionBox);

		final ObservableList<String> shiftList = FXCollections.observableArrayList("08:00 - 16:00", "16:00 - 00:00","00:00 - 08:00");
		shiftBox = new ComboBox<>(shiftList);
		shiftBox.setMinWidth(MEDIUM_FIELD_WIDTH);
		Label shiftLabel = new Label("shift");
		shiftLabel.setMinWidth(MEDIUM_LABEL_WIDTH);
		HBox shiftHBox = new HBox(10);
		shiftHBox.getChildren().addAll(shiftLabel,shiftBox);

		Button submit = new Button("Generate Identification");
		submit.setOnAction(event -> onAction(event));
		Button done = new Button("Done");
		submit.setOnAction(event -> onAction(event));
		Button save = new Button("Save");
		save.setOnAction(event -> onAction(event));
		Button delete = new Button("Delete");
		delete.setOnAction(event -> onAction(event));
		HBox buttonBox = new HBox(10,submit,done,save,delete);

		if(identification != null){
			numberField.setText(identification.getNumber());
			departmentBox.getSelectionModel().select(identification.getDepartment());
			positionBox.getSelectionModel().select(identification.getPosition());
			monthlyHourField.setText(Double.toString(identification.getWorkinHours()));
			shiftBox.getSelectionModel().select(identification.getShift());
		}
		monthlyHourField.addEventFilter(KeyEvent.KEY_TYPED ,float_Validation(10));
		VBox mainView = new VBox(10);
		mainView.setPadding(new Insets(10));
		mainView.getChildren().addAll(new Label(fullName),monthlyHourHBox,departmentHBox,positionHBox,shiftHBox,numberHBox,buttonBox);
		this.getChildren().add(mainView);
	}

	void onAction(Event event){
		if(identification == null){
			identification = new Identification(null,null,departmentBox.getSelectionModel().getSelectedItem(),positionBox.getSelectionModel().getSelectedItem(),
				Double.valueOf(monthlyHourField.getText()),shiftBox.getSelectionModel().getSelectedItem(),id);
		}
		switch (((Button)event.getSource()).getText()){
			case ("Generate Identification"):
				identification = EmployeeServices.generateNewIdentification(identification);
				identification.setStatus(STATUS_ACTIVE);
				numberField.setText(identification.getNumber());
				break;
			case ("Delete"):
				identification.setStatus(STATUS_INACTIVE);
				EmployeeServices.removeIdentification(identification);
				ContentSwitcher.switchCenter(new EmployeeListGroup(width,height));
				break;
			case ("Save"):
				EmployeeServices.saveIdentification(identification);
				ContentSwitcher.switchCenter(new EmployeeListGroup(width,height));
				break;
			case ("Done"):
				ContentSwitcher.switchCenter(new EmployeeListGroup(width,height));
				break;
		}
	}
}
