package employee.view;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import employee.entities.HolidayRequest;
import employee.services.RequestServices;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sharedControllers.ContentSwitcher;
import utils.AuthenticationUtil;

import static employee.utils.ConstantValues.MEDIUM_FIELD_WIDTH;
import static employee.utils.ConstantValues.REQUEST_CONDITION_WAITING;
import static employee.utils.HolidayStatus.WAITING;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class HolidayRequestGroup extends Group {
	private DatePicker dateFromPicker;
	private DatePicker dateToPicker;
	private TextArea commentTextArea;
	private Button submit;
	private Button cancel;
	private Double width;
	private Double height;

	public HolidayRequestGroup(Double width, Double height) {
		Integer id = AuthenticationUtil.getEmployeeId();
		this.width = width;
		this.height = height;
		HBox datePickersBox = setupDatePickers();
		commentTextArea = new TextArea();
		commentTextArea.setMinSize(200,150);

		submit = new Button("Submit");
		submit.setOnAction(event -> {
			HolidayRequest holidayRequest = new HolidayRequest(null, Date.from(dateFromPicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()),
				Date.from(dateToPicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()),commentTextArea.getText(),REQUEST_CONDITION_WAITING,id);
			RequestServices.submitHolidayRequest(holidayRequest);
			ContentSwitcher.switchCenter(new EmployeeListGroup(width, height));
		});
		cancel = new Button("Cancel");
		cancel.setOnAction(event -> ContentSwitcher.switchCenter(new EmployeeListGroup(width, height)));
		VBox mainView = new VBox(10,datePickersBox,commentTextArea,new HBox(10,submit,cancel));
		this.getChildren().add(mainView);
	}

	private HBox setupDatePickers() {
		dateFromPicker = new DatePicker();
		dateFromPicker.setMinWidth(MEDIUM_FIELD_WIDTH);
		dateFromPicker.setValue(LocalDate.now());
		dateFromPicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DateCell() {
					@Override
					public void updateItem(LocalDate item, boolean empty) {
						super.updateItem(item, empty);
						if (item.isBefore(LocalDate.now()) || item.isAfter(dateToPicker.getValue())) {
							setDisable(true);
							setStyle("-fx-background-color: #ffc0cb;");
						}
					}
				};
			}
		});
		dateToPicker = new DatePicker();
		dateToPicker.setValue(LocalDate.now());
		dateToPicker.setMinWidth(MEDIUM_FIELD_WIDTH);
		dateToPicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DateCell() {
					@Override
					public void updateItem(LocalDate item, boolean empty) {
						super.updateItem(item, empty);
						if (item.isBefore(dateFromPicker.getValue()) || item.isBefore(LocalDate.now())) {
							setDisable(true);
							setStyle("-fx-background-color: #ffc0cb;");
						}
					}
				};
			}
		});
		VBox dateFromPickerBox = new VBox(new Label("From date"),dateFromPicker);
		VBox dateToPickerBox = new VBox(new Label("To date"),dateToPicker);
		return new HBox(10,dateFromPickerBox,dateToPickerBox);
	}
}
