package employee.view;

import employee.entities.Review;
import employee.services.EmployeeServices;
import employee.viewModels.ReviewViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Created by Aivaras on 12/23/2016.
 */
public class ReviewListGroup extends Group {
	private Double width;
	private Double height;
	private TableView<ReviewViewModel> tableView;

	public ReviewListGroup(Double width, Double height) {
		this.width = width;
		this.height = height;
		final ObservableList<ReviewViewModel> list = FXCollections.observableArrayList(EmployeeServices.getReviews());
		tableView = new TableView<>(list);
		TableColumn<ReviewViewModel,String> nameCol = new TableColumn("Name");
		nameCol.setCellValueFactory(new PropertyValueFactory("nameProperty"));
		nameCol.setPrefWidth(100);
		TableColumn<ReviewViewModel,String> dateStringCol = new TableColumn("Date");
		dateStringCol.setCellValueFactory(new PropertyValueFactory("dateStringProperty"));
		dateStringCol.setPrefWidth(100);
		TableColumn<ReviewViewModel,String> qualityCol = new TableColumn("quality");
		qualityCol.setCellValueFactory(new PropertyValueFactory("qualityProperty"));
		qualityCol.setPrefWidth(100);
		TableColumn<ReviewViewModel,String> gradeCol = new TableColumn("grade");
		gradeCol.setCellValueFactory(new PropertyValueFactory("gradeProperty"));
		gradeCol.setPrefWidth(100);
		TableColumn<ReviewViewModel,String> improvementCol = new TableColumn("improvement");
		improvementCol.setCellValueFactory(new PropertyValueFactory("improvementProperty"));
		improvementCol.setPrefWidth(100);
		tableView.getColumns().addAll(nameCol,dateStringCol,qualityCol,gradeCol,improvementCol);
		this.getChildren().add(tableView);
	}
}
