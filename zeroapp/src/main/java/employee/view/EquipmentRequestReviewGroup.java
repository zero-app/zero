package employee.view;

import java.time.LocalDate;

import employee.entities.EquipmentRequest;
import employee.services.ReviewServices;
import employee.viewModels.EquipmentRequestViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sharedControllers.ContentSwitcher;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class EquipmentRequestReviewGroup extends Group {
	private TableView<EquipmentRequestViewModel> requestTable;
	private EquipmentRequest equipmentRequest;
	private TextArea commentTextArea;
	private Button acceptButton;
	private Button declineButton;
	private Double width;
	private Double height;
	EquipmentRequestViewModel model;

	public EquipmentRequestReviewGroup(Double width, Double height) {
		this.width = width;
		this.height = height;
		final ObservableList<EquipmentRequestViewModel> holidayOL = FXCollections.observableArrayList(ReviewServices.getEquipmentRequestList());

		commentTextArea = new TextArea();
		commentTextArea.setMinSize(200, 150);
		commentTextArea.setEditable(false);


		TableView<EquipmentRequestViewModel> tableView = new TableView<>(holidayOL);
		TableColumn<EquipmentRequestViewModel, String> nameColumn = new TableColumn<>("Name");
		nameColumn.setPrefWidth(150);
		nameColumn.setCellValueFactory(new PropertyValueFactory<EquipmentRequestViewModel, String>("fullName"));
		TableColumn<EquipmentRequestViewModel, String> datesColumn = new TableColumn<>("Date of request");
		datesColumn.setPrefWidth(150);
		datesColumn.setCellValueFactory(new PropertyValueFactory<EquipmentRequestViewModel, String>("date"));
		TableColumn<EquipmentRequestViewModel, String> statusColumn = new TableColumn<>("Status");
		statusColumn.setPrefWidth(150);
		statusColumn.setCellValueFactory(new PropertyValueFactory<EquipmentRequestViewModel, String>("condition"));
		tableView.getColumns().addAll(nameColumn, datesColumn, statusColumn);
		tableView.setOnMouseClicked(event -> {
			if (tableView.getSelectionModel().getSelectedItem() == null) return;
			model = tableView.getSelectionModel().getSelectedItem();
			commentTextArea.setText(model.getMessage());
		});
		acceptButton = new Button("Accept");
		acceptButton.setOnAction(event -> {
			ReviewServices.acceptEquipmentRequest(model.getId());
			ContentSwitcher.switchCenter(new EquipmentRequestReviewGroup(width,height));
		});
		declineButton = new Button("Decline");
		declineButton.setOnAction(event -> {
			ReviewServices.declineEquipmentRequest(model.getId());
			ContentSwitcher.switchCenter(new EquipmentRequestReviewGroup(width,height));
		});
		HBox mainView = new HBox(10, tableView, new VBox(10, commentTextArea, new HBox(10, acceptButton, declineButton)));

		this.getChildren().addAll(mainView);
	}
}
