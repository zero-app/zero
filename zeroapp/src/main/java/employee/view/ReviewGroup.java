package employee.view;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;

import employee.entities.Employee;
import employee.entities.Review;
import employee.services.EmployeeServices;
import employee.view.custom.CustomViews;
import javafx.collections.FXCollections;
import javafx.collections.ObservableIntegerArray;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sharedControllers.ContentSwitcher;
import utils.AuthenticationUtil;

/**
 * Created by Aivaras on 12/12/2016.
 */
public class ReviewGroup extends Group {
	private TextField firstNameField;
	private TextField surenameField;
	private DatePicker dateFromPicker;
	private DatePicker dateToPicker;
	private ComboBox<Integer> improvementBox;
	private ComboBox<Integer> qualityBox;
	private ComboBox<Integer> overallGradeBox;
	private TextArea comment;
	private Employee employee;
	double width;
	double height;

	public ReviewGroup(Employee reviewed, double width, double height) {
		this.width = width;
		this.height = height;
		this.employee = reviewed;
		firstNameField = new TextField();
		firstNameField.setText(reviewed.getFirstName());
		firstNameField.setEditable(false);
		surenameField = new TextField();
		surenameField.setText(reviewed.getLastName());
		surenameField.setEditable(false);
		HBox datePickerBox = setupDatePickers();
		HBox firsNameBox = CustomViews.setHbox(firstNameField, "First Name");
		HBox surenameBox = CustomViews.setHbox(surenameField, "Last Name");

		final ObservableList<Integer> improvementList = FXCollections.observableArrayList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		final ObservableList<Integer> qualityList = FXCollections.observableArrayList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		final ObservableList<Integer> overallGradeList = FXCollections.observableArrayList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

		improvementBox = new ComboBox<>(improvementList);
		qualityBox = new ComboBox<>(qualityList);
		overallGradeBox = new ComboBox<>(overallGradeList);

		comment = new TextArea();
		comment.setMinSize(200, 150);
		VBox mainView = new VBox(10);
		mainView.setFillWidth(true);
		mainView.setPadding(new Insets(10));

		Button cancelButton = new Button("Cancel");
		Button submitButton = new Button("Submit");
		submitButton.setOnAction(event -> submitReview());
		cancelButton.setOnAction(event -> ContentSwitcher.switchCenter(new EmployeeListGroup(width, height)));
		HBox buttonBox = new HBox(10);
		buttonBox.getChildren().addAll(cancelButton, submitButton);

		VBox gradingBox = setupComboBoxList();
		mainView.getChildren().addAll(firsNameBox, surenameBox, datePickerBox, gradingBox, comment, buttonBox);
		this.getChildren().add(mainView);
	}

	private void submitReview() {
		Review review = new Review();
		review.setFkEmployee(employee.getId());
		review.setFkReviewer(AuthenticationUtil.getEmployeeId());
		review.setComment(comment.getText());
		review.setDateOfReview(Calendar.getInstance().getTime());
		review.setFrom(Date.from(dateFromPicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
		review.setTo(Date.from(dateToPicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
		review.setGrade(overallGradeBox.getSelectionModel().getSelectedItem());
		review.setImprovement(improvementBox.getSelectionModel().getSelectedItem());
		review.setQuality(qualityBox.getSelectionModel().getSelectedItem());
		EmployeeServices.saveNewReview(review);
		ContentSwitcher.switchCenter(new EmployeeListGroup(width,height));
	}

	private VBox setupComboBoxList() {
		HBox improvementHBox = new HBox(10);
		Label improvementLabel = new Label("Improvement");
		improvementLabel.setMinWidth(150);
		improvementHBox.setMinWidth(200);
		improvementHBox.getChildren().addAll(improvementLabel, improvementBox);

		HBox qualityHBox = new HBox(10);
		Label qualityLabel = new Label("Quality");
		qualityLabel.setMinWidth(150);
		qualityHBox.setMinWidth(200);
		qualityHBox.getChildren().addAll(qualityLabel, qualityBox);

		HBox overallGradeHBox = new HBox(10);
		Label overallGradeLabel = new Label("Overall Grade");
		overallGradeLabel.setMinWidth(150);
		overallGradeHBox.setMinWidth(200);
		overallGradeHBox.getChildren().addAll(overallGradeLabel, overallGradeBox);

		VBox vBox = new VBox(10);
		vBox.getChildren().addAll(improvementHBox, qualityHBox, overallGradeHBox);
		return vBox;
	}

	private HBox setupDatePickers() {
		dateFromPicker = new DatePicker();
		dateFromPicker.setValue(LocalDate.now());
		dateFromPicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DateCell() {
					@Override
					public void updateItem(LocalDate item, boolean empty) {
						super.updateItem(item, empty);
						if (item.isAfter(dateToPicker.getValue())) {
							setDisable(true);
							setStyle("-fx-background-color: #ffc0cb;");
						}
					}
				};
			}
		});
		dateToPicker = new DatePicker();
		dateToPicker.setValue(LocalDate.now());
		dateToPicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DateCell() {
					@Override
					public void updateItem(LocalDate item, boolean empty) {
						super.updateItem(item, empty);
						if (item.isBefore(dateFromPicker.getValue().plusDays(1)) || item.isAfter(LocalDate.now())) {
							setDisable(true);
							setStyle("-fx-background-color: #ffc0cb;");
						}
					}
				};
			}
		});

		Label fromLabel = new Label("Begining of review period");
		fromLabel.setMinWidth(200);
		Label toLabel = new Label("End of review period");
		toLabel.setMinWidth(200);
		VBox fromVBox = new VBox(10);
		fromVBox.getChildren().addAll(fromLabel, dateFromPicker);
		VBox toVBox = new VBox(10);
		toVBox.getChildren().addAll(toLabel, dateToPicker);
		HBox hBox = new HBox(10);
		hBox.getChildren().addAll(fromVBox, toVBox);
		return hBox;
	}
}
