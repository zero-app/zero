package employee.view;

import employee.entities.Employee;
import employee.services.EmployeeServices;
import employee.view.custom.CustomViews;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sharedControllers.ContentSwitcher;

import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_BLACKLISTED;
import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_RESIGNED;

/**
 * Created by Aivaras on 12/12/2016.
 */
public class RemoveEmployeeGroup extends Group {
	private TextField nameField;
	private TextField surenameField;
	private CheckBox addToBlackListBox;
	private TextArea commentArea;
	private Button executeButton;
	private Button cancelButton;
	private Employee employee;
	double width, height;
	public RemoveEmployeeGroup(Employee e, double width, double height) {
		this.width = width;
		this.height = height;
		nameField = new TextField();
		surenameField = new TextField();
		addToBlackListBox = new CheckBox("Add to black list");
		commentArea = new TextArea();
		commentArea.setMinWidth(200);
		commentArea.setMinHeight(100);
		employee = e;
		HBox nameBox = CustomViews.setHbox(nameField, "First Name");
		HBox surenameBox = CustomViews.setHbox(surenameField, "Last Name");
		nameField.setText(e.getFirstName());
		surenameField.setText(e.getLastName());
		nameField.setEditable(false);
		surenameField.setEditable(false);
		addToBlackListBox.setOnAction(event -> {
			if(addToBlackListBox.isSelected()){
				commentArea.setEditable(true);
			}else {
				commentArea.setEditable(false);
			}
		});
		commentArea.setEditable(false);
		HBox tickHBox = new HBox(10);
		Label tickLabel = new Label("Add to black list");
		tickLabel.setMinWidth(150);
		tickHBox.getChildren().addAll(tickLabel,addToBlackListBox);
		VBox mainView = new VBox(10);
		executeButton = new Button("Remove");
		executeButton.setOnAction(event -> onRemove(event));
		cancelButton = new Button("Cancel");
		cancelButton.setOnAction(event -> ContentSwitcher.switchCenter(new EmployeeListGroup(width,height)));
		HBox buttonBox = new HBox(10);
		buttonBox.getChildren().addAll(executeButton,cancelButton);

		mainView.setPadding(new Insets(10));
		mainView.setFillWidth(true);
		mainView.getChildren().addAll(nameBox,surenameBox,tickHBox,commentArea,buttonBox);

		this.getChildren().add(mainView);
	}

	private void onRemove(ActionEvent event) {
		employee.setCondition(EMPLOYEE_CONDITION_RESIGNED);
		if(addToBlackListBox.isSelected()){
			EmployeeServices.addToBlackList(employee,commentArea.getText());
			employee.setCondition(EMPLOYEE_CONDITION_BLACKLISTED);
		}
		EmployeeServices.saveOrUpdateEmployee(employee);
		ContentSwitcher.switchCenter(new EmployeeListGroup(width,height));
	}

}
