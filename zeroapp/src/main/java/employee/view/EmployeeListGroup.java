package employee.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import employee.entities.Employee;
import employee.services.EmployeeServices;
import employee.viewModels.EmployeeViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sharedControllers.ContentSwitcher;

import static utils.AuthenticationUtil.getLoggedUser;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class EmployeeListGroup extends Group {
	private TableView<EmployeeViewModel> employeeTable;
	private ComboBox<String> comboBox;
	private List<String> comboOptions;
	private Double width, height;

	public EmployeeListGroup(Double width, Double height) {
		this.width = width;
		this.height = height;
		comboOptions = new ArrayList<>();
		comboOptions.add("Add new");
		comboOptions.add("Modify");
		comboOptions.add("Remove");
		comboOptions.add("Change salary condition");
		comboOptions.add("Review");
		comboOptions.add("Identification");
		comboOptions.add("Employee review history");
		employeeTable = new TableView<>();
		final ObservableList<EmployeeViewModel> models = FXCollections.observableArrayList(EmployeeServices.getEmployeeList());
		employeeTable.setItems(models);
		employeeTable.setPrefWidth(width/2);
		Button button = new Button("Execute command");
		button.setOnAction(event -> onButtonClick());
		double cellWidth = employeeTable.getPrefWidth()/3.0;

		HBox mainView = new HBox(10);
		mainView.setPadding(new Insets(10));

		VBox actionBox = new VBox(10);
		actionBox.setPadding(new Insets(10));

		TableColumn<EmployeeViewModel,String> firstNameCol = new TableColumn<EmployeeViewModel,String>("First Name");
		firstNameCol.setCellValueFactory(new PropertyValueFactory("name"));
		firstNameCol.setPrefWidth(cellWidth);
		TableColumn<EmployeeViewModel,String> lastNameCol = new TableColumn<EmployeeViewModel,String>("Last Name");
		lastNameCol.setCellValueFactory(new PropertyValueFactory("lastName"));
		lastNameCol.setPrefWidth(cellWidth);
		TableColumn<EmployeeViewModel,String> departmentCol = new TableColumn<EmployeeViewModel,String>("Department");
		departmentCol.setCellValueFactory(new PropertyValueFactory("department"));
		departmentCol.setPrefWidth(cellWidth);


		employeeTable.getColumns().setAll(firstNameCol, lastNameCol, departmentCol);
		comboBox = new ComboBox<>();
		comboBox.getItems().addAll(comboOptions);
		this.prefHeight(height);
		this.prefWidth(width);

		actionBox.getChildren().addAll(comboBox,button);
		mainView.getChildren().addAll(employeeTable,actionBox);

		this.getChildren().add(mainView);


	}

	private void onButtonClick(){
		String option = comboBox.getSelectionModel().getSelectedItem();
		if(option == null) return;
		switch (option){
			case ("Add new"):
				ContentSwitcher.switchCenter(new AddEmployeeGroup(width,height));
				break;
			case("Modify"):
				if(employeeTable.getSelectionModel().getSelectedItem() != null){
					Employee e = EmployeeServices.getEmployee(employeeTable.getSelectionModel().getSelectedItem().getId());
					ContentSwitcher.switchCenter(new AddEmployeeGroup(e,width,height));
				}
				break;
			case("Remove"):
				if(employeeTable.getSelectionModel().getSelectedItem() != null){
					Employee e = EmployeeServices.getEmployee(employeeTable.getSelectionModel().getSelectedItem().getId());
					ContentSwitcher.switchCenter(new RemoveEmployeeGroup(e,width,height));
				}
				break;
			case("Change salary condition"):
				if(employeeTable.getSelectionModel().getSelectedItem() != null){
					ContentSwitcher.switchCenter(new SalaryConditionGroup(width,height,employeeTable.getSelectionModel().getSelectedItem().getId()));
				}
				break;
			case("Identification"):
				if(employeeTable.getSelectionModel().getSelectedItem() != null){
					EmployeeViewModel employee = employeeTable.getSelectionModel().getSelectedItem();
					ContentSwitcher.switchCenter(new IdentificationGroup(employee.getId(),employee.getName()+" "+employee.getLastName(),width,height));
				}
				break;
			case("Review"):
				if(employeeTable.getSelectionModel().getSelectedItem() != null){
					Employee e = EmployeeServices.getEmployee(employeeTable.getSelectionModel().getSelectedItem().getId());
					ContentSwitcher.switchCenter(new ReviewGroup(e,width,height));
				}
				break;
			case("Employee review history"):
				ContentSwitcher.switchCenter(new ReviewListGroup(width,height));
				break;
			default:
				break;
		}
	}
}
