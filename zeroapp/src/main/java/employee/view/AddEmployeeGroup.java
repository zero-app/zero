package employee.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import employee.entities.Employee;
import employee.services.EmployeeServices;
import employee.view.custom.CustomViews;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sharedControllers.ContentSwitcher;
import utils.AlertUtil;

import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_ACTIVE;
import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_INACTIVE;
import static employee.utils.ConstantValues.EMPLOYEE_CONDITION_SUSPENDED;
import static employee.view.custom.CustomViews.alphaNumeric_Validation;
import static employee.view.custom.CustomViews.float_Validation;
import static employee.view.custom.CustomViews.letter_Validation;
import static employee.view.custom.CustomViews.numeric_Validation;
import static employee.view.custom.CustomViews.phone_Validation;
import static employee.view.custom.CustomViews.setHbox;

/**
 * Created by Aivaras on 12/10/2016.
 */
public class AddEmployeeGroup extends Group {
	private TextField personalIdentificationNumber;
	private TextField bankAccount;
	private TextField address;
	private TextField phoneNumber;
	private ComboBox<String> condition;
	private TextField salary;
	private TextField holiday;
	private TextField firstName;
	private TextField lastName;
	private Employee employee;
	Double width;
	Double height;

	private Boolean isSet;
	private StringBuilder notificationText;
	private Boolean isNew = true;

	public AddEmployeeGroup(Employee employee, double width, double height) {
		this(width, height);
		if(employee != null) {
			this.employee = employee;
			isNew = false;
		}
		firstName.setText(this.employee.getFirstName());
		lastName.setText(this.employee.getLastName());
		personalIdentificationNumber.setText(this.employee.getPersonalIdentificationNumber().toString());
		bankAccount.setText(this.employee.getBankAccount());
		address.setText(this.employee.getAddress());
		phoneNumber.setText(this.employee.getPhoneNumber());
		condition.getSelectionModel().select(this.employee.getCondition());
		salary.setText(this.employee.getSalary().toString());
		holiday.setText(this.employee.getHoliday().toString());
	}

	public AddEmployeeGroup(double width, double height) {
		employee = new Employee();
		isSet = false;
		notificationText = new StringBuilder();
		this.width = width;
		this.height = height;
		firstName = new TextField();
		lastName = new TextField();
		personalIdentificationNumber = new TextField();
		bankAccount = new TextField();
		address = new TextField();
		phoneNumber = new TextField();
		salary = new TextField();
		holiday = new TextField();
		VBox mainView = new VBox(10);
		mainView.setPadding(new Insets(10));
		mainView.setFillWidth(true);
		HBox fNameBox = setHbox(firstName, "First name");
		HBox lastNameBox = setHbox(lastName, "Last name");
		HBox personalIdentificationNumberBox = setHbox(personalIdentificationNumber, "Personal id");
		HBox bankAccountBox = setHbox(bankAccount, "Bank account");
		HBox addressBox = setHbox(address, "Address");
		HBox phoneNumberBox = setHbox(phoneNumber, "Phone");
		HBox salaryBox = setHbox(salary, "Monthly salary");
		HBox holidayBox = setHbox(holiday, "Holiday day count");
		List<String> conditionList = new ArrayList<>();
		conditionList.add(EMPLOYEE_CONDITION_ACTIVE);
		conditionList.add(EMPLOYEE_CONDITION_INACTIVE);
		conditionList.add(EMPLOYEE_CONDITION_SUSPENDED);
		condition = new ComboBox<>();
		condition.getItems().addAll(conditionList);
		condition.setOnAction(event -> {
			employee.setCondition(((ComboBox)event.getSource()).getSelectionModel().getSelectedItem().toString());
		});
		HBox conditionBox = new HBox(10);
		Label conditionLabel = new Label("Salary condtion");
		conditionLabel.setMinWidth(150);
		condition.setMinWidth(200);
		conditionBox.getChildren().addAll(conditionLabel, condition);
		setInputCheck();
		Button saveButton = new Button("Save");
		saveButton.setOnAction(event -> onSaveClick());
		mainView.getChildren().addAll(fNameBox, lastNameBox, personalIdentificationNumberBox, bankAccountBox, addressBox, phoneNumberBox, salaryBox, holidayBox, conditionBox, saveButton);
		this.getChildren().add(mainView);
	}

	private void setInputCheck(){
		personalIdentificationNumber.addEventFilter(KeyEvent.KEY_TYPED , numeric_Validation(20));
		bankAccount.addEventFilter(KeyEvent.KEY_TYPED ,alphaNumeric_Validation(50));
		phoneNumber.addEventFilter(KeyEvent.KEY_TYPED ,phone_Validation(20));
		salary.addEventFilter(KeyEvent.KEY_TYPED ,float_Validation(20));
		holiday.addEventFilter(KeyEvent.KEY_TYPED ,float_Validation(20));
		firstName.addEventFilter(KeyEvent.KEY_TYPED ,letter_Validation(50));
		lastName.addEventFilter(KeyEvent.KEY_TYPED ,letter_Validation(50));
	}

	private void onSaveClick() {
		isSet = true;
		String eFirstName = firstName.getText() == null || firstName.getText().trim().isEmpty() ? (String)notification("First name is missing") : firstName.getText().trim();
		String eLastName = lastName.getText() == null || lastName.getText().trim().isEmpty() ? (String)notification("Last name is missing") : lastName.getText().trim();
		Long ePID = personalIdentificationNumber.getText() == null || personalIdentificationNumber.getText().trim().isEmpty() ? (Long)notification("Personal identification number is missing") : Long.valueOf(personalIdentificationNumber.getText().trim());
		String eBankAcc = bankAccount.getText() == null || bankAccount.getText().trim().isEmpty() ? (String)notification("Bank account number is missing") : bankAccount.getText().trim();
		String eAddress = address.getText() == null || address.getText().trim().isEmpty() ? (String)notification("Address is missing") : address.getText().trim();
		String ePhone = phoneNumber.getText() == null || phoneNumber.getText().trim().isEmpty() ? (String)notification("Phone number is missing") : phoneNumber.getText().trim();
		String eCondition = condition.getSelectionModel().getSelectedItem() == null || condition.getSelectionModel().getSelectedItem().trim().isEmpty()? (String)notification("Condition is missing") : condition.getSelectionModel().getSelectedItem();
		Double eSalary = salary.getText() == null || salary.getText().trim().isEmpty() ? (Double)notification("Salary is missing") : Double.valueOf(salary.getText());
		Double hol = holiday.getText() == null || holiday.getText().trim().isEmpty() ? (Double)notification("Holiday is missing") : Double.valueOf(holiday.getText());
		if (isSet){
			employee.setFirstName(eFirstName);
			employee.setLastName(eLastName);
			employee.setPersonalIdentificationNumber(ePID);
			employee.setBankAccount(eBankAcc);
			employee.setAddress(eAddress);
			employee.setPhoneNumber(ePhone);
			employee.setCondition(eCondition);
			employee.setSalary(eSalary);
			employee.setHoliday(hol);
			if(isNew){
				employee.setStartedWork(Calendar.getInstance().getTime());
			}
			EmployeeServices.saveOrUpdateEmployee(employee);
			ContentSwitcher.switchCenter(new EmployeeListGroup(width, height));
		}else {
			AlertUtil.displayAlert(Alert.AlertType.WARNING,"Input error","There was an error with your input",notificationText.toString());
		}
	}

	private Object notification(String s){
		isSet = false;
		notificationText.append(s);
		notificationText.append("\n");
		return null;
	}
}
