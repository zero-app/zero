package employee.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import employee.entities.Equipment;
import employee.entities.EquipmentRequest;
import employee.services.RequestServices;
import employee.viewModels.EquipmentViewModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sharedControllers.ContentSwitcher;
import utils.AuthenticationUtil;

import static employee.utils.ConstantValues.REQUEST_CONDITION_WAITING;

/**
 * Created by Aivaras on 12/14/2016.
 */
public class EquipmentRequestGroup extends Group {
	private ListView<EquipmentViewModel> requiredEquipmentListView;
	private ListView<EquipmentViewModel> availableEquipmentListView;
	private EquipmentRequest equipmentRequest;
	private Double width;
	private Double height;

	public EquipmentRequestGroup(Double width, Double height) {
		equipmentRequest = new EquipmentRequest();
		this.width = width;
		this.height = height;
		List<EquipmentViewModel> availableEquipmentList = new ArrayList<>(RequestServices.getAvailableEquipmentViewModels());
		final ObservableList<EquipmentViewModel> availableEquipmentOL = FXCollections.observableArrayList(availableEquipmentList);
		final ObservableList<EquipmentViewModel> requiredEquipmentOL = FXCollections.observableArrayList();
		requiredEquipmentListView = new ListView<>(requiredEquipmentOL);
		availableEquipmentListView = new ListView<>(availableEquipmentOL);
		availableEquipmentListView.setCellFactory(new Callback<ListView<EquipmentViewModel>, ListCell<EquipmentViewModel>>() {
			@Override
			public ListCell<EquipmentViewModel> call(ListView<EquipmentViewModel> param) {
				ListCell<EquipmentViewModel> listCell = new ListCell<EquipmentViewModel>() {
					@Override
					protected void updateItem(EquipmentViewModel item, boolean empty) {
						super.updateItem(item, empty);
						if (!empty) {
							setText(item.getName());
							setTooltip(new Tooltip(item.getType()));
						}
					}
				};
				return listCell;
			}
		});
		requiredEquipmentListView.setCellFactory(new Callback<ListView<EquipmentViewModel>, ListCell<EquipmentViewModel>>() {
			@Override
			public ListCell<EquipmentViewModel> call(ListView<EquipmentViewModel> param) {
				ListCell<EquipmentViewModel> listCell = new ListCell<EquipmentViewModel>() {
					@Override
					protected void updateItem(EquipmentViewModel item, boolean empty) {
						super.updateItem(item, empty);
						if (!empty) {
							setText(item.getName());
							setTooltip(new Tooltip(item.getType()));
						}

					}
				};
				return listCell;
			}
		});
		Button add = new Button("Add");
		add.setMinWidth(100);
		Button remove = new Button("Remove");
		remove.setMinWidth(100);
		add.setOnAction(event -> requiredEquipmentOL.add(availableEquipmentListView.getSelectionModel().getSelectedItem()));
		remove.setOnAction(event -> requiredEquipmentOL.remove(requiredEquipmentListView.getSelectionModel().getSelectedItem()));
		VBox addRemoveBox = new VBox(10,add,remove);
		HBox listBox = new HBox(2, availableEquipmentListView, addRemoveBox,requiredEquipmentListView);

		Button submitButton = new Button("Submit");
		submitButton.setOnAction(event -> {
			equipmentRequest.setCondition(REQUEST_CONDITION_WAITING);
			equipmentRequest.setDate(Calendar.getInstance().getTime());
			equipmentRequest.setFkEmployee(AuthenticationUtil.getEmployeeId());
			StringBuilder sb = new StringBuilder();
			for(EquipmentViewModel e: requiredEquipmentOL){
				sb.append(e.getType()+":"+e.getName()+"\n");
			}
			equipmentRequest.setComment(sb.toString());
			RequestServices.submitEquipmentRequest(equipmentRequest);
			ContentSwitcher.switchCenter(new EmployeeListGroup(width, height));
		});
		VBox mainView = new VBox(10, listBox, submitButton);
		this.getChildren().add(mainView);
	}
}
