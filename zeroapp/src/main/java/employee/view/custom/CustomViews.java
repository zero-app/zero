package employee.view.custom;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;

/**
 * Created by Aivaras on 12/12/2016.
 */
public class CustomViews {
	public static HBox setHbox(TextField control, String name) {
		HBox hBox = new HBox(10);
		Label nameLabel = new Label(name);
		nameLabel.setMinWidth(150);
		control.setMinWidth(200);
		hBox.getChildren().addAll(nameLabel, control);
		return hBox;
	}

	public static EventHandler<KeyEvent> numeric_Validation(final Integer max_Lengh) {
		return new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				TextField txt_TextField = (TextField) e.getSource();
				if(e.getCharacter().matches("[0-9.]")){
					if(txt_TextField.getText().contains(".") && e.getCharacter().matches("[.]")){
						e.consume();
					}else if(txt_TextField.getText().length() == 0 && e.getCharacter().matches("[.]")){
						e.consume();
					}
				}else{
					e.consume();
				}
			}
		};
	}
	public static EventHandler<KeyEvent> phone_Validation(final Integer max_Lengh) {
		return new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				TextField txt_TextField = (TextField) e.getSource();
				if(!e.getCharacter().matches("[0-9+]")){
					e.consume();
				} else if(!txt_TextField.getText().isEmpty() && txt_TextField.getText().equals("^+[0-9]") && e.getCharacter().equals("+")){
					e.consume();
				}
			}
		};
	}

	public static EventHandler<KeyEvent> float_Validation(final Integer max_Lengh) {
		return new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				TextField txt_TextField = (TextField) e.getSource();
				if(!e.getCharacter().matches("[0-9.]")){
					e.consume();
				}
			}
		};
	}
	/*****************************************************************************************/

 /* Letters Validation Limit the  characters to maxLengh AND to ONLY Letters *************************************/
	public static EventHandler<KeyEvent> letter_Validation(final Integer max_Lengh) {
		return new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				TextField txt_TextField = (TextField) e.getSource();
				if(e.getCharacter().matches("[A-Za-z]")){
				}else{
					e.consume();
				}
			}
		};
	}
	public static EventHandler<KeyEvent> alphaNumeric_Validation(final Integer max_Lengh) {
		return new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				TextField txt_TextField = (TextField) e.getSource();
				if(e.getCharacter().matches("[A-Za-z0-9]")){
				}else{
					e.consume();
				}
			}
		};
	}
}
