package goodsSub.dataAccessObjects;

import goodsSub.objects.Food;
import goodsSub.objects.Goods;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FoodDAO extends GoodsDAO
{
    private static Food extractFood(ResultSet resultSet) throws SQLException
    {
        int idFood = resultSet.getInt("id_Maistas");
        String expirationDate = resultSet.getString("galioja_iki");
        int calories = resultSet.getInt("kalorijos");
        String composition = resultSet.getString("sudetis");

        return new Food(idFood, expirationDate, calories, composition, extractGoods(resultSet));
    }

    private static Food getFoodFromResultSet(ResultSet resultSet) throws SQLException
    {
        Food food = null;

        if(resultSet.next())
        {
            food = extractFood(resultSet);
        }

        return food;
    }

    public static Food getFood(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Prekes INNER JOIN Maisto_prekes, Prekes_tipai WHERE Prekes.id_Preke=" + id + " AND Prekes.id_Preke=Maisto_prekes.fk_Preke AND Prekes_tipai.id_Prekes_tipas=Prekes.fk_prekes_tipas";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getFoodFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void insertFood(Food food) throws SQLException, ClassNotFoundException
    {
        Goods goods = food.getGoods();

        try
        {
            String insertFood = "INSERT INTO Maisto_prekes (galioja_iki, kalorijos, sudetis, fk_preke) VALUES ('"+food.getExpirationDate()+"', '"+food.getCalories()+"', '"+food.getComposition()+"'";
            insertGoods(goods, insertFood);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void updateFood(Food food) throws SQLException, ClassNotFoundException
    {
        Goods goods = food.getGoods();

        String updateFood = "UPDATE Maisto_prekes SET galioja_iki='"+food.getExpirationDate()+"', kalorijos='"+food.getCalories()+"', sudetis='"+food.getComposition()+"' WHERE id_Maistas='"+food.getId()+"';";

        try
        {
            updateGoods(goods, updateFood);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
