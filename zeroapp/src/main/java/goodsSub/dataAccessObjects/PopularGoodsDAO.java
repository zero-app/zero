package goodsSub.dataAccessObjects;

import goodsSub.objects.PopularGoods;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PopularGoodsDAO
{
    private static PopularGoods extractPopularGoodsByType(ResultSet resultSet) throws SQLException
    {
        String name = resultSet.getString("pavadinimas");
        String barcode = resultSet.getString("barkodas");
        int soldAmount = resultSet.getInt("sold_amount");

        return new PopularGoods(name, barcode, soldAmount);
    }

    private static ObservableList<PopularGoods> getAllPopularGoodsFromResultSet(ResultSet resultSet) throws SQLException
    {
        ObservableList<PopularGoods> popularGoodsObservableList = FXCollections.observableArrayList();

        while(resultSet.next())
        {
            popularGoodsObservableList.add(extractPopularGoodsByType(resultSet));
        }

        return popularGoodsObservableList;
    }

    public static ObservableList<PopularGoods> getAllPopularGoodsByType(int top) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT Prekes.pavadinimas, Prekes.barkodas, SUM(Uzsakomos_prekes.uzsakytas_kiekis) AS sold_amount FROM Uzsakomos_prekes\n" +
                "INNER JOIN Prekes ON Prekes.id_preke=Uzsakomos_prekes.fk_preke\n" +
                "GROUP BY Prekes.id_preke\n" +
                "ORDER BY sold_amount DESC\n" +
                "LIMIT "+top;

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getAllPopularGoodsFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
