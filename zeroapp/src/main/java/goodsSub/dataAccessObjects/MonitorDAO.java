package goodsSub.dataAccessObjects;

import goodsSub.objects.Goods;
import goodsSub.objects.Monitor;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MonitorDAO extends GoodsDAO
{
    private static Monitor extractMonitor(ResultSet resultSet) throws SQLException
    {
        int idMonitor = resultSet.getInt("id_Monitorius");
        double size = resultSet.getDouble("ekrano_dydis");
        String resolution = resultSet.getString("ekrano_raiska");
        int responseTime = resultSet.getInt("reakcijos_laikas");
        String contrast = resultSet.getString("kontrastas");

        return new Monitor(idMonitor, size, resolution, responseTime, contrast, extractGoods(resultSet));
    }

    private static Monitor getMonitorFromResultSet(ResultSet resultSet) throws SQLException
    {
        Monitor monitor = null;

        if(resultSet.next())
        {
            monitor = extractMonitor(resultSet);
        }

        return monitor;
    }

    public static Monitor getMonitor(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Prekes INNER JOIN Monitorius_prekes, Prekes_tipai WHERE Prekes.id_Preke=" + id + " AND Prekes.id_Preke=Monitorius_prekes.fk_Preke AND Prekes_tipai.id_Prekes_tipas=Prekes.fk_prekes_tipas";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getMonitorFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void insertMonitor(Monitor monitor) throws SQLException, ClassNotFoundException
    {
        Goods goods = monitor.getGoods();

        try
        {
            String insertMonitor = "INSERT INTO Monitorius_prekes (ekrano_dydis, ekrano_raiska, reakcijos_laikas, kontrastas, fk_preke) VALUES ('"+monitor.getScreenSize()+"', '"+monitor.getResolution()+"', '"+monitor.getResponseTime()+"', '"+monitor.getContrast()+"'";
            insertGoods(goods, insertMonitor);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void updateMonitor(Monitor monitor) throws SQLException, ClassNotFoundException
    {
        Goods goods = monitor.getGoods();

        String updateMonitor = "UPDATE Monitorius_prekes SET ekrano_dydis='"+monitor.getScreenSize()+"', ekrano_raiska='"+monitor.getResolution()+"', reakcijos_laikas='"+monitor.getResponseTime()+"', kontrastas='"+monitor.getContrast()+"' WHERE id_Monitorius='"+monitor.getId()+"';";

        try
        {
            updateGoods(goods, updateMonitor);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
