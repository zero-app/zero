package goodsSub.dataAccessObjects;

import goodsSub.objects.GoodsType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.Database;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GoodsTypeDAO
{
    private static GoodsType extractGoodsType(ResultSet resultSet) throws SQLException
    {
        int id = resultSet.getInt("id_Prekes_tipas");
        String name = resultSet.getString("tipo_pavadinimas");
        String tableName = resultSet.getString("lenteles_pavadinimas");

        return new GoodsType(id, name, tableName);
    }

    private static ObservableList<GoodsType> getAlGoodsTypesFromResultSet(ResultSet resultSet) throws SQLException
    {
        ObservableList<GoodsType> goodsTypesObservableList = FXCollections.observableArrayList();

        while(resultSet.next())
        {
            goodsTypesObservableList.add(extractGoodsType(resultSet));
        }

        return goodsTypesObservableList;
    }

    private static GoodsType getGoodsTypeFromResultSet(ResultSet resultSet) throws SQLException
    {
        GoodsType goodsType = null;

        if(resultSet.next())
        {
            goodsType = extractGoodsType(resultSet);
        }

        return goodsType;
    }

    public static ObservableList<GoodsType> getAllGoodsTypes() throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Prekes_tipai";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getAlGoodsTypesFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static GoodsType getGoodsType(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Prekes_tipai WHERE id_Prekes_tipas='"+id+"'";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getGoodsTypeFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
