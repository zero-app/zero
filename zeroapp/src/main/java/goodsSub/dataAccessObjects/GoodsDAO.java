package goodsSub.dataAccessObjects;

import buyersSub.objects.Buyer;
import buyersSub.objects.OrderedGoods;
import goodsSub.objects.Goods;
import goodsSub.objects.GoodsType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GoodsDAO
{
    protected static Goods extractGoods(ResultSet resultSet) throws SQLException
    {
        Goods goods = extractGoodsEssentials(resultSet);

        int idGoodsType = resultSet.getInt("id_Prekes_tipas");
        String typeName = resultSet.getString("tipo_pavadinimas");
        String tableName = resultSet.getString("lenteles_pavadinimas");

        GoodsType goodsType = new GoodsType(idGoodsType, typeName, tableName);
        goods.setGoodsType(goodsType);
        return goods;
    }

    private static Goods extractGoodsEssentials(ResultSet resultSet) throws SQLException
    {
        int idGoods = resultSet.getInt("id_Preke");
        String barcode = resultSet.getString("barkodas");
        int amount = resultSet.getInt("kiekis");
        String name = resultSet.getString("pavadinimas");
        String summary = resultSet.getString("aprasymas");
        String manufacturer = resultSet.getString("gamintojas");
        Double price = resultSet.getDouble("kaina");

        return new Goods(idGoods, barcode, amount, name, summary, manufacturer, price);
    }

    private static ObservableList<Goods> getAllGoodsEssentialsFromResultSet(ResultSet resultSet) throws SQLException
    {
        ObservableList<Goods> goodsObservableList = FXCollections.observableArrayList();

        while(resultSet.next())
        {
            goodsObservableList.add(extractGoodsEssentials(resultSet));
        }

        return goodsObservableList;
    }

    private static Goods getGoodsFromResultSet(ResultSet resultSet) throws SQLException
    {
        Goods goods = null;

        if(resultSet.next())
        {
            goods = extractGoodsEssentials(resultSet);
        }

        return goods;
    }

    public static ResultSet getSetOfAllGoodsOfType(String tableName) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT Prekes.*, goods.* FROM Prekes INNER JOIN "+tableName+" AS goods WHERE goods.fk_preke=id_Preke";

        try
        {
            return Database.executeQuery(statement);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void deleteGoods(int id, String tableName) throws SQLException, ClassNotFoundException
    {
        try
        {
            String deleteGoods = "DELETE FROM "+tableName+" WHERE fk_Preke='"+id+"'";
            Database.executeUpdate(deleteGoods);

            deleteGoods = "DELETE FROM Prekes WHERE id_Preke='"+id+"'";
            Database.executeUpdate(deleteGoods);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static boolean checkIfBarcodeExists(String barcode) throws SQLException, ClassNotFoundException
    {
        try
        {
            String statement = "SELECT * FROM Prekes WHERE barkodas='"+barcode+"'";
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static boolean checkIfBarcodeExistsIgnoreWithId(String barcode, int id) throws SQLException, ClassNotFoundException
    {
        try
        {
            String statement = "SELECT * FROM Prekes WHERE barkodas='"+barcode+"' AND id_Preke!='"+id+"'";
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    protected static void insertGoods(Goods goods, String goodsOfTypeInsert) throws SQLException, ClassNotFoundException
    {
        try
        {
            String insertGoods = "INSERT INTO Prekes (fk_prekes_tipas, barkodas, kiekis, pavadinimas, aprasymas, gamintojas, kaina) VALUES('"+goods.getGoodsType().getIdType()+"', '"+goods.getBarcode()+"', '"+goods.getAmount()+"', '"+goods.getName()+"', '"+goods.getSummary()+"', '"+goods.getManufacturer()+"', '"+goods.getPrice()+"');";
            long goodsId = Database.executeUpdateReturnKey(insertGoods);
            goodsOfTypeInsert += ", '"+goodsId+"');";
            Database.executeUpdate(goodsOfTypeInsert);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    protected static void updateGoods(Goods goods, String goodsOfTypeUpdate) throws SQLException, ClassNotFoundException
    {
        try
        {
            updateGoodsEssentials(goods);
            Database.executeUpdate(goodsOfTypeUpdate);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void updateGoodsEssentials(Goods goods) throws SQLException, ClassNotFoundException
    {
        try
        {
            String updateGoods = "UPDATE Prekes SET barkodas='"+goods.getBarcode()+"', kiekis='"+goods.getAmount()+"', pavadinimas='"+goods.getName()+"', aprasymas='"+goods.getSummary()+"', gamintojas='"+goods.getManufacturer()+"', kaina='"+goods.getPrice()+"' WHERE id_Preke='"+goods.getIdGoods()+"';";
            Database.executeUpdate(updateGoods);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static ObservableList<Goods> getAllGoodsEssentials() throws SQLException, ClassNotFoundException
    {
        try
        {
            String statement = "SELECT * FROM Prekes";
            ResultSet resultSet = Database.executeQuery(statement);

            return getAllGoodsEssentialsFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static boolean checkIfGoodsWithIdExists(int id) throws SQLException, ClassNotFoundException
    {
        try
        {
            String statement = "SELECT * FROM Prekes WHERE id_Preke='"+id+"'";
            ResultSet resultSet = Database.executeQuery(statement);

            return resultSet.next();
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static Goods getGoodsWithId(int id) throws SQLException, ClassNotFoundException
    {
        try
        {
            String statement = "SELECT * FROM Prekes WHERE id_Preke='"+id+"'";
            ResultSet resultSet = Database.executeQuery(statement);

            return getGoodsFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void changeAmounts(boolean subtract, ObservableList<OrderedGoods> orderedGoods) throws SQLException, ClassNotFoundException
    {
        try
        {
            for(int i = 0; i < orderedGoods.size(); i++)
            {
                Goods goods = GoodsDAO.getGoodsWithId(orderedGoods.get(i).getGoods().getIdGoods());
                if(subtract)
                {
                    goods.setAmount(goods.getAmount() - orderedGoods.get(i).getAmount());
                }
                else
                {
                    goods.setAmount(goods.getAmount() + orderedGoods.get(i).getAmount());
                }

                GoodsDAO.updateGoodsEssentials(goods);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
