package goodsSub.dataAccessObjects;

import goodsSub.objects.Goods;
import goodsSub.objects.Smartphone;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SmartphoneDAO extends GoodsDAO
{
    private static Smartphone extractSmartphone(ResultSet resultSet) throws SQLException
    {
        int idSmartphone = resultSet.getInt("id_Telefonas");
        String processor = resultSet.getString("procesorius");
        double size = resultSet.getDouble("ekrano_dydis");
        String resolution = resultSet.getString("ekrano_raiska");
        String color = resultSet.getString("spalva");
        String rams = resultSet.getString("darbine_atmintis");
        String storage = resultSet.getString("talpa");
        String camera = resultSet.getString("kamera");

        return new Smartphone(idSmartphone, processor, size, resolution, color, rams, storage, camera, extractGoods(resultSet));
    }

    private static Smartphone getSmartphoneFromResultSet(ResultSet resultSet) throws SQLException
    {
        Smartphone smartphone = null;

        if(resultSet.next())
        {
            smartphone = extractSmartphone(resultSet);
        }

        return smartphone;
    }

    public static Smartphone getSmartphone(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Prekes INNER JOIN Telefonas_prekes, Prekes_tipai WHERE Prekes.id_Preke=" + id + " AND Prekes.id_Preke=Telefonas_prekes.fk_Preke AND Prekes_tipai.id_Prekes_tipas=Prekes.fk_prekes_tipas";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getSmartphoneFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void insertSmartphone(Smartphone smartphone) throws SQLException, ClassNotFoundException
    {
        Goods goods = smartphone.getGoods();

        try
        {
            String insertSmartphone = "INSERT INTO Telefonas_prekes (procesorius, ekrano_dydis, ekrano_raiska, spalva, darbine_atmintis, talpa, kamera, fk_preke) VALUES ('"+smartphone.getProcessor()+"', '"+smartphone.getScreenSize()+"', '"+smartphone.getResolution()+"', '"+smartphone.getColor()+"', '"+smartphone.getRams()+"', '"+smartphone.getStorage()+"', '"+smartphone.getCamera()+"'";
            insertGoods(goods, insertSmartphone);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void updateSmartphone(Smartphone smartphone) throws SQLException, ClassNotFoundException
    {
        Goods goods = smartphone.getGoods();

        String updateSmartphone = "UPDATE Telefonas_prekes SET procesorius='"+smartphone.getProcessor()+"', ekrano_dydis='"+smartphone.getScreenSize()+"', ekrano_raiska='"+smartphone.getResolution()+"', spalva='"+smartphone.getColor()+"', darbine_atmintis='"+smartphone.getRams()+"', talpa='"+smartphone.getStorage()+"', kamera='"+smartphone.getCamera()+"' WHERE id_Telefonas='"+smartphone.getId()+"';";

        try
        {
            updateGoods(goods, updateSmartphone);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
