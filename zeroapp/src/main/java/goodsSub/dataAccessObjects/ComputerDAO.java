package goodsSub.dataAccessObjects;

import goodsSub.objects.Computer;
import goodsSub.objects.Goods;
import utils.Database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ComputerDAO extends GoodsDAO
{
    private static Computer extractComputer(ResultSet resultSet) throws SQLException
    {
        int idComputer = resultSet.getInt("id_Kompiuteris");
        String processor = resultSet.getString("procesorius");
        String graphicsCard = resultSet.getString("vaizdo_adapteris");
        String rams = resultSet.getString("darbine_atmintis");
        int storage = resultSet.getInt("talpa");
        String computerCase = resultSet.getString("korpusas");
        String powerSupply = resultSet.getString("maitinimo_blokas");

        return new Computer(idComputer, processor, graphicsCard, rams, storage, computerCase, powerSupply, extractGoods(resultSet));
    }

    private static Computer getComputerFromResultSet(ResultSet resultSet) throws SQLException
    {
        Computer computer = null;

        if(resultSet.next())
        {
            computer = extractComputer(resultSet);
        }

        return computer;
    }

    public static Computer getComputer(int id) throws SQLException, ClassNotFoundException
    {
        String statement = "SELECT * FROM Prekes INNER JOIN Kompiuteris_prekes, Prekes_tipai WHERE Prekes.id_Preke=" + id + " AND Prekes.id_Preke=Kompiuteris_prekes.fk_Preke AND Prekes_tipai.id_Prekes_tipas=Prekes.fk_prekes_tipas";

        try
        {
            ResultSet resultSet = Database.executeQuery(statement);

            return getComputerFromResultSet(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void insertComputer(Computer computer) throws SQLException, ClassNotFoundException
    {
        Goods goods = computer.getGoods();

        try
        {
            String insertComputer = "INSERT INTO Kompiuteris_prekes (procesorius, vaizdo_adapteris, darbine_atmintis, talpa, korpusas, maitinimo_blokas, fk_preke) VALUES ('"+computer.getProcessor()+"', '"+computer.getGraphicsCard()+"', '"+computer.getRams()+"', '"+computer.getStorage()+"', '"+computer.getComputerCase()+"', '"+computer.getPowerSupply()+"'";
            insertGoods(goods, insertComputer);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }

    public static void updateComputer(Computer computer) throws SQLException, ClassNotFoundException
    {
        Goods goods = computer.getGoods();

        String updateComputer = "UPDATE Kompiuteris_prekes SET procesorius='"+computer.getProcessor()+"', vaizdo_adapteris='"+computer.getGraphicsCard()+"', darbine_atmintis='"+computer.getRams()+"', talpa='"+computer.getStorage()+"', korpusas='"+computer.getComputerCase()+"', maitinimo_blokas='"+computer.getPowerSupply()+"' WHERE id_Kompiuteris='"+computer.getId()+"';";

        try
        {
           updateGoods(goods, updateComputer);
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            throw e;
        }
    }
}
