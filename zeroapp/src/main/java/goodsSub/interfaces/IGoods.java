package goodsSub.interfaces;

import goodsSub.objects.Goods;

public interface IGoods
{
    Goods getGoods();
}
