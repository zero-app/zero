package goodsSub.objects;

import goodsSub.interfaces.IGoods;
import javafx.beans.property.*;

public class Smartphone implements IGoods
{
    private IntegerProperty id;
    private StringProperty processor;
    private DoubleProperty screenSize;
    private StringProperty resolution;
    private StringProperty color;
    private StringProperty rams;
    private StringProperty storage;
    private StringProperty camera;

    private Goods goods;

    public Smartphone(int id, String processor, double screenSize, String resolution, String color, String rams, String storage, String camera, Goods goods)
    {
        this.id = new SimpleIntegerProperty(id);
        this.processor = new SimpleStringProperty(processor);
        this.screenSize = new SimpleDoubleProperty(screenSize);
        this.resolution = new SimpleStringProperty(resolution);
        this.color = new SimpleStringProperty(color);
        this.rams = new SimpleStringProperty(rams);
        this.storage = new SimpleStringProperty(storage);
        this.camera = new SimpleStringProperty(camera);

        this.goods = goods;
    }

    public Smartphone(String processor, double screenSize, String resolution, String color, String rams, String storage, String camera, Goods goods)
    {
        this.processor = new SimpleStringProperty(processor);
        this.screenSize = new SimpleDoubleProperty(screenSize);
        this.resolution = new SimpleStringProperty(resolution);
        this.color = new SimpleStringProperty(color);
        this.rams = new SimpleStringProperty(rams);
        this.storage = new SimpleStringProperty(storage);
        this.camera = new SimpleStringProperty(camera);

        this.goods = goods;
    }

    public void setProcessor(String processor)
    {
        this.processor.set(processor);
    }

    public void setScreenSize(double screenSize)
    {
        this.screenSize.set(screenSize);
    }

    public void setResolution(String resolution)
    {
        this.resolution.set(resolution);
    }

    public void setColor(String color)
    {
        this.color.set(color);
    }

    public void setRams(String rams)
    {
        this.rams.set(rams);
    }

    public void setStorage(String storage)
    {
        this.storage.set(storage);
    }

    public void setCamera(String camera)
    {
        this.camera.set(camera);
    }

    public void setGoods(Goods goods)
    {
        this.goods = goods;
    }

    public int getId()
    {
        return id.get();
    }

    public String getProcessor()
    {
        return processor.get();
    }

    public double getScreenSize()
    {
        return screenSize.get();
    }

    public String getResolution()
    {
        return resolution.get();
    }

    public String getColor()
    {
        return color.get();
    }

    public String getRams()
    {
        return rams.get();
    }

    public String getStorage()
    {
        return storage.get();
    }

    public String getCamera()
    {
        return camera.get();
    }

    public Goods getGoods()
    {
        return goods;
    }

    public IntegerProperty getIdProperty()
    {
        return id;
    }

    public StringProperty getProcessorProperty()
    {
        return processor;
    }

    public DoubleProperty getScreenSizeProperty()
    {
        return screenSize;
    }

    public StringProperty getResolutionProperty()
    {
        return resolution;
    }

    public StringProperty getColorProperty()
    {
        return color;
    }

    public StringProperty getRamsProperty()
    {
        return rams;
    }

    public StringProperty getStorageProperty()
    {
        return storage;
    }

    public StringProperty getCameraProperty()
    {
        return camera;
    }
}
