package goodsSub.objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GoodsType
{
    private IntegerProperty idType;
    private StringProperty name;
    private StringProperty tableName;

    public GoodsType(int idType, String name, String tableName)
    {
        this.idType = new SimpleIntegerProperty(idType);
        this.name = new SimpleStringProperty(name);
        this.tableName = new SimpleStringProperty(tableName);
    }

    public GoodsType(String name, String tableName)
    {
        this.name = new SimpleStringProperty(name);
        this.tableName = new SimpleStringProperty(tableName);
    }

    public void setName(String name)
    {
        this.name.set(name);
    }

    public void setTableName(String tableName)
    {
        this.tableName.set(tableName);
    }

    public int getIdType()
    {
        return idType.get();
    }

    public String getName()
    {
        return name.get();
    }

    public String getTableName()
    {
        return tableName.get();
    }

    public IntegerProperty getIdTypeProperty()
    {
        return idType;
    }

    public StringProperty getNameProperty()
    {
        return name;
    }

    public StringProperty getTableNameProperty()
    {
        return tableName;
    }

    public String toString()
    {
        return name.get();
    }
}
