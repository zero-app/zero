package goodsSub.objects;

import goodsSub.interfaces.IGoods;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Computer implements IGoods
{
    private IntegerProperty id;
    private StringProperty processor;
    private StringProperty graphicsCard;
    private StringProperty rams;
    private IntegerProperty storage;
    private StringProperty computerCase;
    private StringProperty powerSupply;

    private Goods goods;

    public Computer(int id, String processor, String graphicsCard, String rams, Integer storage, String computerCase, String powerSupply, Goods goods)
    {
        this.id = new SimpleIntegerProperty(id);
        this.processor = new SimpleStringProperty(processor);
        this.graphicsCard = new SimpleStringProperty(graphicsCard);
        this.rams = new SimpleStringProperty(rams);
        this.storage = new SimpleIntegerProperty(storage);
        this.computerCase = new SimpleStringProperty(computerCase);
        this.powerSupply = new SimpleStringProperty(powerSupply);

        this.goods = goods;
    }

    public Computer(String processor, String graphicsCard, String rams, Integer storage, String computerCase, String powerSupply, Goods goods)
    {
        this.processor = new SimpleStringProperty(processor);
        this.graphicsCard = new SimpleStringProperty(graphicsCard);
        this.rams = new SimpleStringProperty(rams);
        this.storage = new SimpleIntegerProperty(storage);
        this.computerCase = new SimpleStringProperty(computerCase);
        this.powerSupply = new SimpleStringProperty(powerSupply);

        this.goods = goods;
    }

    public void setProcessor(String processor)
    {
        this.processor.set(processor);
    }

    public void setGraphicsCard(String graphicsCard)
    {
        this.graphicsCard.set(graphicsCard);
    }

    public void setRams(String rams)
    {
        this.rams.set(rams);
    }

    public void setStorage(int storage)
    {
        this.storage.set(storage);
    }

    public void setComputerCase(String computerCase)
    {
        this.computerCase.set(computerCase);
    }

    public void setPowerSupply(String powerSupply)
    {
        this.powerSupply.set(powerSupply);
    }

    public void setGoods(Goods goods)
    {
        this.goods = goods;
    }

    public int getId()
    {
        return id.get();
    }

    public String getProcessor()
    {
        return processor.get();
    }

    public String getGraphicsCard()
    {
        return graphicsCard.get();
    }

    public String getRams()
    {
        return rams.get();
    }

    public int getStorage()
    {
        return storage.get();
    }

    public String getComputerCase()
    {
        return computerCase.get();
    }

    public String getPowerSupply()
    {
        return powerSupply.get();
    }

    public Goods getGoods()
    {
        return goods;
    }

    public IntegerProperty getIdProperty()
    {
        return id;
    }

    public StringProperty getProcessorProperty()
    {
        return processor;
    }

    public StringProperty getGraphicsCardPropery()
    {
        return graphicsCard;
    }

    public StringProperty getRamsProperty()
    {
        return rams;
    }

    public IntegerProperty getStorageProperty()
    {
        return storage;
    }

    public StringProperty getComputerCaseProperty()
    {
        return computerCase;
    }

    public StringProperty getPowerSupplyProperty()
    {
        return powerSupply;
    }
}
