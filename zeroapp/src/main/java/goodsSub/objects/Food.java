package goodsSub.objects;

import goodsSub.interfaces.IGoods;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Food implements IGoods
{
    private IntegerProperty id;
    private StringProperty expirationDate;
    private IntegerProperty calories;
    private StringProperty composition;

    private Goods goods;

    public Food(int id, String expirationDate, int calories, String composition, Goods goods)
    {
        this.id = new SimpleIntegerProperty(id);
        this.expirationDate = new SimpleStringProperty(expirationDate);
        this.calories = new SimpleIntegerProperty(calories);
        this.composition = new SimpleStringProperty(composition);

        this.goods = goods;
    }

    public Food(String expirationDate, int calories, String composition, Goods goods)
    {
        this.expirationDate = new SimpleStringProperty(expirationDate);
        this.calories = new SimpleIntegerProperty(calories);
        this.composition = new SimpleStringProperty(composition);

        this.goods = goods;
    }

    public void setExpirationDate(String expirationDate)
    {
        this.expirationDate.set(expirationDate);
    }

    public void setCalories(int calories)
    {
        this.calories.set(calories);
    }

    public void setComposition(String composition)
    {
        this.composition.set(composition);
    }

    public void setGoods(Goods goods)
    {
        this.goods = goods;
    }

    public int getId()
    {
        return id.get();
    }

    public String getExpirationDate()
    {
        return expirationDate.get();
    }

    public int getCalories()
    {
        return calories.get();
    }

    public String getComposition()
    {
        return composition.get();
    }

    public Goods getGoods()
    {
        return goods;
    }

    public IntegerProperty getIdProperty()
    {
        return id;
    }

    public StringProperty getExpirationDateProperty()
    {
        return expirationDate;
    }

    public IntegerProperty getCaloriesProperty()
    {
        return calories;
    }

    public StringProperty getCompositionProperty()
    {
        return composition;
    }
}
