package goodsSub.objects;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;

public class GoodsForListing
{
    private static final int GOODS_ID_FIELD = 0;
    private static final int GOODS_TYPE_ID_FIELD = 7;

    private ArrayList<StringProperty> properties = new ArrayList<>();

    public void addProperty(String property)
    {
        properties.add(new SimpleStringProperty(property));
    }

    public Integer getGoodsId()
    {
        if(properties.size() > GOODS_ID_FIELD)
        {
            return new Integer(properties.get(GOODS_ID_FIELD).get());
        }

        return null;
    }

    public Integer getGoodsTypeId()
    {
        if(properties.size() > GOODS_TYPE_ID_FIELD)
        {
            return new Integer(properties.get(GOODS_TYPE_ID_FIELD).get());
        }

        return null;
    }

    public String get(int i)
    {
        return properties.get(i).get();
    }
}
