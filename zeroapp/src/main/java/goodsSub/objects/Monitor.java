package goodsSub.objects;

import goodsSub.interfaces.IGoods;
import javafx.beans.property.*;

public class Monitor implements IGoods
{
    private IntegerProperty id;
    private DoubleProperty screenSize;
    private StringProperty resolution;
    private IntegerProperty responseTime;
    private StringProperty contrast;

    private Goods goods;

    public Monitor(int id , double screenSize, String resolution, int responseTime, String contrast, Goods goods)
    {
        this.id = new SimpleIntegerProperty(id);
        this.screenSize = new SimpleDoubleProperty(screenSize);
        this.resolution = new SimpleStringProperty(resolution);
        this.responseTime = new SimpleIntegerProperty(responseTime);
        this.contrast = new SimpleStringProperty(contrast);

        this.goods = goods;
    }

    public Monitor(double screenSize, String resolution, int responseTime, String contrast, Goods goods)
    {
        this.screenSize = new SimpleDoubleProperty(screenSize);
        this.resolution = new SimpleStringProperty(resolution);
        this.responseTime = new SimpleIntegerProperty(responseTime);
        this.contrast = new SimpleStringProperty(contrast);

        this.goods = goods;
    }

    public void setScreenSize(double screenSize)
    {
        this.screenSize.set(screenSize);
    }

    public void setResolution(String resolution)
    {
        this.resolution.set(resolution);
    }

    public void setResponseTime(int responseTime)
    {
        this.responseTime.set(responseTime);
    }

    public void setContrast(String contrast)
    {
        this.contrast.set(contrast);
    }

    public void setGoods(Goods goods)
    {
        this.goods = goods;
    }

    public int getId()
    {
        return id.get();
    }

    public Double getScreenSize()
    {
        return screenSize.get();
    }

    public String getResolution()
    {
        return resolution.get();
    }

    public int getResponseTime()
    {
        return responseTime.get();
    }

    public String getContrast()
    {
        return contrast.get();
    }

    public Goods getGoods()
    {
        return goods;
    }

    public IntegerProperty getIdProperty()
    {
        return id;
    }

    public DoubleProperty getScreenSizeProperty()
    {
        return screenSize;
    }

    public StringProperty getResoliutionProperty()
    {
        return resolution;
    }

    public IntegerProperty getResponseTimeProperty()
    {
        return responseTime;
    }

    public StringProperty getContrastProperty()
    {
        return contrast;
    }
}
