package goodsSub.objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PopularGoods
{
    private StringProperty name;
    private StringProperty barcode;
    private IntegerProperty soldAmount;

    public PopularGoods(String name, String barcode, int soldAmount)
    {
        this.name = new SimpleStringProperty(name);
        this.barcode = new SimpleStringProperty(barcode);
        this.soldAmount = new SimpleIntegerProperty(soldAmount);
    }

    public String getName()
    {
        return name.get();
    }

    public String getBarcode()
    {
        return barcode.get();
    }

    public int getSoldAmount()
    {
        return soldAmount.get();
    }

    public StringProperty getNameProperty()
    {
        return name;
    }

    public StringProperty getBarcodeProperty()
    {
        return barcode;
    }

    public IntegerProperty getSoldAmountProperty()
    {
        return soldAmount;
    }
}
