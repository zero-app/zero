package goodsSub.objects;

import javafx.beans.property.*;

public class Goods
{
    private IntegerProperty idGoods;
    private StringProperty barcode;
    private IntegerProperty amount;
    private StringProperty name;
    private StringProperty summary;
    private StringProperty manufacturer;
    private DoubleProperty price;

    private GoodsType goodsType;

    public Goods(int id, String barcode, int amount, String name, String summary, String manufacturer, double price, GoodsType goodsType)
    {
        this.idGoods = new SimpleIntegerProperty(id);
        this.barcode = new SimpleStringProperty(barcode);
        this.amount = new SimpleIntegerProperty(amount);
        this.name = new SimpleStringProperty(name);
        this.summary = new SimpleStringProperty(summary);
        this.manufacturer = new SimpleStringProperty(manufacturer);
        this.price = new SimpleDoubleProperty(price);

        this.goodsType = goodsType;
    }

    public Goods(int id, String barcode, int amount, String name, String summary, String manufacturer, double price)
    {
        this.idGoods = new SimpleIntegerProperty(id);
        this.barcode = new SimpleStringProperty(barcode);
        this.amount = new SimpleIntegerProperty(amount);
        this.name = new SimpleStringProperty(name);
        this.summary = new SimpleStringProperty(summary);
        this.manufacturer = new SimpleStringProperty(manufacturer);
        this.price = new SimpleDoubleProperty(price);
    }

    public Goods(String barcode, int amount, String name, String summary, String manufacturer, double price, GoodsType goodsType)
    {
        this.barcode = new SimpleStringProperty(barcode);
        this.amount = new SimpleIntegerProperty(amount);
        this.name = new SimpleStringProperty(name);
        this.summary = new SimpleStringProperty(summary);
        this.manufacturer = new SimpleStringProperty(manufacturer);
        this.price = new SimpleDoubleProperty(price);

        this.goodsType = goodsType;
    }

    public void setBarcode(String barcode)
    {
        this.barcode.set(barcode);
    }

    public void setAmount(int amount)
    {
        this.amount.set(amount);
    }

    public void setName(String name)
    {
        this.name.set(name);
    }

    public void setSummary(String summary)
    {
        this.summary.set(summary);
    }

    public void setManufacturer(String manufacturer)
    {
        this.manufacturer.set(manufacturer);
    }

    public void setPrice(double price)
    {
        this.price.set(price);
    }

    public void setGoodsType(GoodsType goodsType)
    {
        this.goodsType = goodsType;
    }

    public int getIdGoods()
    {
        return idGoods.get();
    }

    public String getBarcode()
    {
        return barcode.get();
    }

    public int getAmount()
    {
        return amount.get();
    }

    public String getName()
    {
        return name.get();
    }

    public String getSummary()
    {
        return summary.get();
    }

    public String getManufacturer()
    {
        return manufacturer.get();
    }

    public double getPrice()
    {
        return price.get();
    }

    public GoodsType getGoodsType()
    {
        return goodsType;
    }

    public IntegerProperty getIdGoodsProperty()
    {
        return idGoods;
    }

    public StringProperty getBarcodeProperty()
    {
        return barcode;
    }

    public IntegerProperty getAmountProperty()
    {
        return amount;
    }

    public StringProperty getNameProperty()
    {
        return name;
    }

    public StringProperty getSummaryProperty()
    {
        return summary;
    }

    public StringProperty getManufacturerProperty()
    {
        return manufacturer;
    }

    public DoubleProperty getPriceProperty()
    {
        return price;
    }

    public String toString()
    {
        return idGoods.get() + " " + barcode.get() + " " + name.get() + " Amount: " + amount.get();
    }
}
