package goodsSub.controllers;

import goodsSub.dataAccessObjects.MonitorDAO;
import goodsSub.objects.Monitor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import utils.AlertUtil;
import utils.RegexUtil;

import java.sql.SQLException;

public class MonitorEditController extends GoodsEditController
{
    private static final int GOODS_TYPE_ID = 3;

    private Monitor monitor;

    @FXML
    private TextField sizeField;

    @FXML
    private TextField resolutionField;

    @FXML
    private TextField responseTimeField;

    @FXML
    private TextField contrastField;

    public void init(Integer id)
    {
        if(id != null)
        {
            editMode = true;
            try
            {
                monitor = MonitorDAO.getMonitor(id);
                populateFields();
            }
            catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();

                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get information from database");
            }
        }
        else
        {
            getGoodsType(GOODS_TYPE_ID);
        }
    }

    private void populateFields()
    {
        populateGoodsField(monitor);

        sizeField.setText(Double.toString(monitor.getScreenSize()));
        resolutionField.setText(monitor.getResolution());
        responseTimeField.setText(Integer.toString(monitor.getResponseTime()));
        contrastField.setText(monitor.getContrast());
    }

    private void clearFields()
    {
       clearGoodsFields();

        sizeField.clear();
        resolutionField.clear();
        responseTimeField.clear();
        contrastField.clear();
    }

    public void saveChanges(ActionEvent event)
    {
        if(checkFields())
        {
            if(editMode)
            {
                if(!databaseUpdateCheck(monitor.getGoods().getIdGoods()))
                {
                    updateMonitor();
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Barcode already exists");
                }
            }
            else
            {
                if(!databaseInsertCheck())
                {
                    insertMonitor();
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Barcode already exists");
                }
            }
        }
    }

    private boolean checkFields()
    {
        if(checkGoodsFields())
        {
            if(sizeField.getText().length() < 1 || !RegexUtil.checkIfOnlyNumbers(sizeField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect screen size");
                return false;
            }
            else if(resolutionField.getText().length() < 1 || !RegexUtil.checkIfResolution(resolutionField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect resolution");
                return false;
            }
            else if(responseTimeField.getText().length() < 1 || !RegexUtil.checkIfInteger(responseTimeField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect response time");
                return false;
            }
            else if(contrastField.getText().length() < 1 || !RegexUtil.checkIfContrast(contrastField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect contrast");
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    private void setNewMonitorObject()
    {
        monitor = new Monitor(new Double(sizeField.getText()), resolutionField.getText(), new Integer(responseTimeField.getText()), contrastField.getText(), getNewGoodsObjectFromFields());
    }

    private void setNewValuesToCurrentMonitorObject()
    {
        monitor.setScreenSize(new Double(sizeField.getText()));
        monitor.setResolution(resolutionField.getText());
        monitor.setResponseTime(new Integer(responseTimeField.getText()));
        monitor.setContrast(contrastField.getText());

        setNewValuesToCurrentGoodsObject(monitor);
    }

    private void insertMonitor()
    {
        setNewMonitorObject();
        try
        {
            MonitorDAO.insertMonitor(monitor);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Monitor was inserted");
            clearFields();
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to insert new monitor to database");
        }
    }

    private void updateMonitor()
    {
        setNewValuesToCurrentMonitorObject();
        try
        {
            MonitorDAO.updateMonitor(monitor);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Monitor was updated");
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to update monitor in database");
        }
    }
}
