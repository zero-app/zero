package goodsSub.controllers;

import goodsSub.dataAccessObjects.FoodDAO;
import goodsSub.dataAccessObjects.GoodsTypeDAO;
import goodsSub.objects.Food;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import utils.AlertUtil;
import utils.RegexUtil;

import java.sql.SQLException;

public class FoodEditController extends GoodsEditController
{
    private static final int GOODS_TYPE_ID = 2;

    private Food food;

    @FXML
    private TextField expirationDateField;

    @FXML
    private TextField caloriesField;

    @FXML
    private TextArea compositionField;

    public void init(Integer id)
    {
        if(id != null)
        {
            editMode = true;
            try
            {
                food = FoodDAO.getFood(id);
                populateFields();
            }
            catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();

                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get information from database");
            }
        }
        else
        {
            getGoodsType(GOODS_TYPE_ID);
        }
    }

    private void populateFields()
    {
        populateGoodsField(food);

        expirationDateField.setText(food.getExpirationDate());
        caloriesField.setText(Integer.toString(food.getCalories()));
        compositionField.setText(food.getComposition());
    }

    private void clearFields()
    {
        clearGoodsFields();

        expirationDateField.clear();
        caloriesField.clear();
        compositionField.clear();
    }

    public void saveChanges(ActionEvent event)
    {
        if(checkFields())
        {
            if(editMode)
            {
                if(!databaseUpdateCheck(food.getGoods().getIdGoods()))
                {
                    updateFood();
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Barcode already exists");
                }
            }
            else
            {
                if(!databaseInsertCheck())
                {
                    insertFood();
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Barcode already exists");
                }
            }
        }
    }

    private boolean checkFields()
    {

        if(checkGoodsFields())
        {
            if(expirationDateField.getText().length() < 1 || !RegexUtil.checkIfDate(expirationDateField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect expiration date");
                return false;
            }
            else if(caloriesField.getText().length() < 1 || !RegexUtil.checkIfInteger(caloriesField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect calories");
                return false;
            }
            else if(compositionField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect composition");
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    private void setNewFoodObject()
    {
        food = new Food(expirationDateField.getText(), new Integer(caloriesField.getText()), compositionField.getText(), getNewGoodsObjectFromFields());
    }

    private void setNewValuesToCurrentFoodObject()
    {
        food.setExpirationDate(expirationDateField.getText());
        food.setCalories(new Integer(caloriesField.getText()));
        food.setComposition(compositionField.getText());

        setNewValuesToCurrentGoodsObject(food);
    }

    private void insertFood()
    {
        setNewFoodObject();
        try
        {
            FoodDAO.insertFood(food);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Food was inserted");
            clearFields();
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to insert new food to database");
        }
    }

    private void updateFood()
    {
        setNewValuesToCurrentFoodObject();
        try
        {
            FoodDAO.updateFood(food);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Food was updated");
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to update food in database");
        }
    }
}
