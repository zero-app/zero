package goodsSub.controllers;

import goodsSub.dataAccessObjects.PopularGoodsDAO;
import goodsSub.objects.PopularGoods;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import utils.AlertUtil;
import utils.RegexUtil;

import java.sql.SQLException;

public class PopularGoodsReportController
{
    @FXML
    private TextField topField;

    @FXML
    private TableView<PopularGoods> popularGoodsTable;
    @FXML
    private TableColumn<PopularGoods, String> nameColumn;
    @FXML
    private TableColumn<PopularGoods, String> barcodeColumn;
    @FXML
    private TableColumn<PopularGoods, Integer> soldAmountColumn;

    @FXML
    private void initialize()
    {
        setUpCells();
    }

    @FXML
    public void generateReport(ActionEvent event)
    {
        if(checkFields())
        {
            try
            {
                popularGoodsTable.setItems(PopularGoodsDAO.getAllPopularGoodsByType(new Integer(topField.getText())));
            }
            catch (SQLException | ClassNotFoundException e)
            {
                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get data from database");
                e.printStackTrace();
            }
        }
    }

    private boolean checkFields()
    {
        if(topField.getText().length() < 1 || !RegexUtil.checkIfInteger(topField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Top field is incorrect");
            return false;
        }

        return true;
    }

    private void setUpCells()
    {
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
        soldAmountColumn.setCellValueFactory(cellData -> cellData.getValue().getSoldAmountProperty().asObject());
        barcodeColumn.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
    }
}
