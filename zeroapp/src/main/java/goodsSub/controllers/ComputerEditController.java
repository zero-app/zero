package goodsSub.controllers;

import goodsSub.dataAccessObjects.ComputerDAO;
import goodsSub.objects.Computer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import utils.AlertUtil;
import utils.RegexUtil;

import java.sql.SQLException;

public class ComputerEditController extends GoodsEditController
{
    private static final int GOODS_TYPE_ID = 1;

    private Computer computer;

    @FXML
    private TextField processorField;

    @FXML
    private TextField graphicsCardField;

    @FXML
    private TextField ramField;

    @FXML
    private TextField storageField;

    @FXML
    private TextField caseField;

    @FXML
    private TextField powerSupplyField;

    public void init(Integer id)
    {
        if(id != null)
        {
            editMode = true;
            try
            {
                computer = ComputerDAO.getComputer(id);
                populateFields();
            }
            catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();

                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get information from database");
            }
        }
        else
        {
           getGoodsType(GOODS_TYPE_ID);
        }
    }

    private void populateFields()
    {
        populateGoodsField(computer);

        processorField.setText(computer.getProcessor());
        graphicsCardField.setText(computer.getGraphicsCard());
        ramField.setText(computer.getRams());
        storageField.setText(Integer.toString(computer.getStorage()));
        caseField.setText(computer.getComputerCase());
        powerSupplyField.setText(computer.getPowerSupply());
    }

    private void clearFields()
    {
        clearGoodsFields();

        processorField.clear();
        graphicsCardField.clear();
        ramField.clear();
        storageField.clear();
        caseField.clear();
        powerSupplyField.clear();
    }

    public void saveChanges(ActionEvent event)
    {
        if(checkFields())
        {
            if(editMode)
            {
                if(!databaseUpdateCheck(computer.getGoods().getIdGoods()))
                {
                    updateComputer();
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Barcode already exists");
                }
            }
            else
            {
                if(!databaseInsertCheck())
                {
                    insertComputer();
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Barcode already exists");
                }
            }
        }
    }

    private boolean checkFields()
    {
        if(checkGoodsFields())
        {
            if(processorField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect processor");
                return false;
            }
            else if(graphicsCardField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect graphics card");
                return false;
            }
            else if(ramField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect RAM");
                return false;
            }
            else if(storageField.getText().length() < 1 || !RegexUtil.checkIfInteger(storageField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect storage");
                return false;
            }
            else if(caseField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect computer case");
                return false;
            }
            else if(powerSupplyField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect power supply");
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    private void setNewComputerObject()
    {
        computer = new Computer(processorField.getText(), graphicsCardField.getText(), ramField.getText(), new Integer(storageField.getText()), caseField.getText(), powerSupplyField.getText(), getNewGoodsObjectFromFields());
    }

    private void setNewValuesToCurrentComputerObject()
    {
        computer.setProcessor(processorField.getText());
        computer.setGraphicsCard(graphicsCardField.getText());
        computer.setRams(ramField.getText());
        computer.setStorage(new Integer(storageField.getText()));
        computer.setComputerCase(caseField.getText());
        computer.setPowerSupply(powerSupplyField.getText());

        setNewValuesToCurrentGoodsObject(computer);
    }

    private void insertComputer()
    {
        setNewComputerObject();
        try
        {
            ComputerDAO.insertComputer(computer);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Computer was inserted");
            clearFields();
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to insert new computer to database");
        }
    }

    private void updateComputer()
    {
        setNewValuesToCurrentComputerObject();
        try
        {
            ComputerDAO.updateComputer(computer);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Computer was updated");
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to update computer in database");
        }
    }
}
