package goodsSub.controllers;

import goodsSub.dataAccessObjects.GoodsDAO;
import goodsSub.dataAccessObjects.GoodsTypeDAO;
import goodsSub.interfaces.IGoods;
import goodsSub.objects.Goods;
import goodsSub.objects.GoodsType;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import utils.AlertUtil;
import utils.RegexUtil;

import java.sql.SQLException;

public class GoodsEditController
{
    protected GoodsType goodsType;
    protected boolean editMode = false;

    @FXML
    protected TextField barcodeField;

    @FXML
    protected TextField amountField;

    @FXML
    protected TextField nameField;

    @FXML
    protected TextArea summaryField;

    @FXML
    protected TextField manufacturerField;

    @FXML
    protected TextField priceField;

    protected void populateGoodsField(IGoods iGoods)
    {
        barcodeField.setText(iGoods.getGoods().getBarcode());
        amountField.setText(Integer.toString(iGoods.getGoods().getAmount()));
        summaryField.setText(iGoods.getGoods().getSummary());
        nameField.setText(iGoods.getGoods().getName());
        manufacturerField.setText(iGoods.getGoods().getManufacturer());
        priceField.setText(Double.toString(iGoods.getGoods().getPrice()));
    }

    protected void clearGoodsFields()
    {
        barcodeField.clear();
        amountField.clear();
        summaryField.clear();
        nameField.clear();
        manufacturerField.clear();
        priceField.clear();
    }

    protected boolean checkGoodsFields()
    {
        if(barcodeField.getText().length() < 1 || !RegexUtil.checkIfInteger(barcodeField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect barcode");
            return false;
        }
        else if(amountField.getText().length() < 1 || !RegexUtil.checkIfInteger(amountField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect amount");
            return false;
        }
        else if(summaryField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect summary");
            return false;
        }
        else if(nameField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect name");
            return false;
        }
        else if(manufacturerField.getText().length() < 1)
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect manufacturer");
            return false;
        }
        else if(priceField.getText().length() < 1 || !RegexUtil.checkIfOnlyNumbers(priceField.getText()))
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect price");
            return false;
        }

        return true;
    }

    protected boolean databaseInsertCheck()
    {
        try
        {
            return GoodsDAO.checkIfBarcodeExists(barcodeField.getText());
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to check data in database");
        }

        return false;
    }

    protected boolean databaseUpdateCheck(int id)
    {
        try
        {
            return GoodsDAO.checkIfBarcodeExistsIgnoreWithId(barcodeField.getText(), id);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to check data in database");
        }

        return false;
    }

    protected void setNewValuesToCurrentGoodsObject(IGoods goodsObject)
    {
        goodsObject.getGoods().setBarcode(barcodeField.getText());
        goodsObject.getGoods().setAmount(new Integer(amountField.getText()));
        goodsObject.getGoods().setName(nameField.getText());
        goodsObject.getGoods().setSummary(summaryField.getText());
        goodsObject.getGoods().setManufacturer(manufacturerField.getText());
        goodsObject.getGoods().setPrice(new Double(priceField.getText()));
        goodsObject.getGoods().setGoodsType(goodsType);
    }

    protected Goods getNewGoodsObjectFromFields()
    {
        return new Goods(barcodeField.getText(), new Integer(amountField.getText()), nameField.getText(), summaryField.getText(), manufacturerField.getText(), new Double(priceField.getText()), goodsType);
    }

    protected void getGoodsType(int id)
    {
        try
        {
            goodsType = GoodsTypeDAO.getGoodsType(id);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get information from database");
        }
    }
}
