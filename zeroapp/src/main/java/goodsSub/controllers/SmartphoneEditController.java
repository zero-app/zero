package goodsSub.controllers;

import goodsSub.dataAccessObjects.SmartphoneDAO;
import goodsSub.objects.Smartphone;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import utils.AlertUtil;
import utils.RegexUtil;

import javax.jnlp.DownloadService;
import java.sql.SQLException;

public class SmartphoneEditController extends GoodsEditController
{
    private static final int GOODS_TYPE_ID = 4;

    private Smartphone smartphone;

    @FXML
    private TextField processorField;

    @FXML
    private TextField sizeField;

    @FXML
    private TextField resolutionField;

    @FXML
    private TextField colorField;

    @FXML
    private TextField ramField;

    @FXML
    private TextField storageField;

    @FXML
    private TextField cameraField;

    public void init(Integer id)
    {
        if(id != null)
        {
            editMode = true;
            try
            {
                smartphone = SmartphoneDAO.getSmartphone(id);
                populateFields();
            }
            catch (SQLException | ClassNotFoundException e)
            {
                e.printStackTrace();

                AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get information from database");
            }
        }
        else
        {
            getGoodsType(GOODS_TYPE_ID);
        }
    }

    private void populateFields()
    {
        populateGoodsField(smartphone);

        processorField.setText(smartphone.getProcessor());
        sizeField.setText(Double.toString(smartphone.getScreenSize()));
        resolutionField.setText(smartphone.getResolution());
        colorField.setText(smartphone.getColor());
        ramField.setText(smartphone.getRams());
        storageField.setText(smartphone.getStorage());
        cameraField.setText(smartphone.getCamera());
    }

    private void clearFields()
    {
        clearGoodsFields();

        processorField.clear();
        sizeField.clear();
        resolutionField.clear();
        colorField.clear();
        ramField.clear();
        storageField.clear();
        cameraField.clear();
    }

    public void saveChanges(ActionEvent event)
    {
        if(checkFields())
        {
            if(editMode)
            {
                if(!databaseUpdateCheck(smartphone.getGoods().getIdGoods()))
                {
                    updateSmartphone();
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Barcode already exists");
                }
            }
            else
            {
                if(!databaseInsertCheck())
                {
                    insertSmartphone();
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Barcode already exists");
                }
            }
        }
    }

    private boolean checkFields()
    {
        if(checkGoodsFields())
        {
            if(processorField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect processor");
                return false;
            }
            else if(sizeField.getText().length() < 1 || !RegexUtil.checkIfOnlyNumbers(sizeField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect screen size");
                return false;
            }
            if(resolutionField.getText().length() < 1 || !RegexUtil.checkIfResolution(resolutionField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect resolution");
                return false;
            }
            if(colorField.getText().length() < 1 || !RegexUtil.checkIfOnlyLetters(colorField.getText()))
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect color");
                return false;
            }
            else if(ramField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect RAM");
                return false;
            }
            else if(storageField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect storage");
                return false;
            }
            else if(cameraField.getText().length() < 1)
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Incorrect camera");
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    private void setNewSmartphoneObject()
    {
        smartphone = new Smartphone(processorField.getText(), new Double(sizeField.getText()), resolutionField.getText(), colorField.getText(), ramField.getText(), storageField.getText(), cameraField.getText(), getNewGoodsObjectFromFields());
    }

    private void setNewValuesToCurrentSmartphoneObject()
    {
        smartphone.setProcessor(processorField.getText());
        smartphone.setScreenSize(new Double(sizeField.getText()));
        smartphone.setResolution(resolutionField.getText());
        smartphone.setColor(colorField.getText());
        smartphone.setRams(ramField.getText());
        smartphone.setStorage(storageField.getText());
        smartphone.setCamera(cameraField.getText());

        setNewValuesToCurrentGoodsObject(smartphone);
    }

    private void insertSmartphone()
    {
        setNewSmartphoneObject();
        try
        {
            SmartphoneDAO.insertSmartphone(smartphone);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Smartphone was inserted");
            clearFields();
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to insert new smartphone to database");
        }
    }

    private void updateSmartphone()
    {
        setNewValuesToCurrentSmartphoneObject();
        try
        {
            SmartphoneDAO.updateSmartphone(smartphone);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Smartphone was updated");
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to update smartphone in database");
        }
    }
}
