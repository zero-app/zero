package goodsSub.controllers;

import buyersSub.dataAccessObjects.OrderedGoodsDAO;
import goodsSub.dataAccessObjects.GoodsDAO;
import goodsSub.dataAccessObjects.GoodsTypeDAO;
import goodsSub.interfaces.IGoods;
import goodsSub.objects.GoodsForListing;
import goodsSub.objects.GoodsType;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;
import sharedControllers.ContentSwitcher;
import utils.AlertUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GoodsListingController
{
    private ContentSwitcher contentSwitcher = new ContentSwitcher();

    @FXML
    private ChoiceBox<GoodsType> goodsTypeChoiceBox;

    @FXML
    private TableView<GoodsForListing> goodsTable;

    private String selectedTableName;

    @FXML
    private void initialize()
    {
        populateGoodsTypeChoiceBox();
    }

    private void populateGoodsTypeChoiceBox()
    {
        try
        {
            ObservableList<GoodsType> goodsTypesData = GoodsTypeDAO.getAllGoodsTypes();

            goodsTypeChoiceBox.setItems(goodsTypesData);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get goods types from database");
        }
    }


    private void populateTableWithGoods(String tableName)
    {
        try
        {
            ResultSet resultSet = GoodsDAO.getSetOfAllGoodsOfType(tableName);
            ObservableList<GoodsForListing> goodsData = FXCollections.observableArrayList();

            for(int i = 0 ; i < resultSet.getMetaData().getColumnCount(); i++)
            {
                final int j = i;
                if(!resultSet.getMetaData().getColumnName(i+1).startsWith("fk_"))
                {
                    TableColumn col = new TableColumn(resultSet.getMetaData().getColumnName(i+1));
                    col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GoodsForListing,String>,ObservableValue<String>>()
                    {
                        public ObservableValue<String> call(TableColumn.CellDataFeatures<GoodsForListing, String> param)
                        {
                            if(param.getValue().get(j) != null)
                            {
                                return new SimpleStringProperty(param.getValue().get(j));
                            }
                            else
                            {
                                return new SimpleStringProperty("NULL");
                            }
                        }
                    });

                    goodsTable.getColumns().addAll(col);
                }
            }

            while(resultSet.next())
            {
                GoodsForListing row = new GoodsForListing();
                for(int i = 1 ; i <= resultSet.getMetaData().getColumnCount(); i++)
                {
                    row.addProperty(resultSet.getString(i));
                }
                goodsData.add(row);

            }

            goodsTable.setItems(goodsData);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to get goods from database");
        }
    }

    public void switchToAddNewGoodsWindow(ActionEvent event)
    {
        GoodsType selectedGoodsType = goodsTypeChoiceBox.getSelectionModel().getSelectedItem();

        if(selectedGoodsType != null)
        {
            switchToEditOfGoodsType(null, selectedGoodsType.getIdType());
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Information", "You need to select goods type in order to add new good of that type");
        }
    }

    public void deleteGoods(ActionEvent event)
    {
        GoodsForListing selectedGoods = goodsTable.getSelectionModel().getSelectedItem();
        int index = goodsTable.getSelectionModel().getSelectedIndex();

        if(selectedGoods != null)
        {
            if(AlertUtil.confirmationAlert("Delete", "Are you sure you want to delete selected item?", ""))
            {
                Integer selectedId = selectedGoods.getGoodsId();

                if(selectedId != null && deleteGoodsFromDatabase(selectedId))
                {
                    goodsTable.getItems().remove(index);

                    AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Item was removed from list");
                }
                else
                {
                    AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "This item is ordered");
                }
            }
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing is selected");
        }
    }

    public void editGoods(ActionEvent event)
    {
        GoodsForListing selectedGoods = goodsTable.getSelectionModel().getSelectedItem();

        if(selectedGoods != null)
        {
            Integer selectedId = selectedGoods.getGoodsId();
            Integer typeId = selectedGoods.getGoodsTypeId();

            if(selectedId != null && typeId != null)
            {
                switchToEditOfGoodsType(selectedId, typeId);
            }
            else
            {
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Unable to open item for editing");
            }
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing is selected");
        }
    }

    public void showGoodsOfType(ActionEvent event)
    {
        GoodsType selectedGoodsType = goodsTypeChoiceBox.getSelectionModel().getSelectedItem();

        if(selectedGoodsType != null)
        {
            goodsTable.getItems().clear();
            goodsTable.getColumns().clear();
            this.selectedTableName = selectedGoodsType.getTableName();
            populateTableWithGoods(selectedGoodsType.getTableName());
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Type is not selected");
        }
    }

    private boolean deleteGoodsFromDatabase(int id)
    {
        try
        {
            if(!OrderedGoodsDAO.checkIfGoodsWithIdIsOrdered(id))
            {
                GoodsDAO.deleteGoods(id, selectedTableName);

                return true;
            }

            return false;
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();

            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Unable to delete item from database");
        }

        return false;
    }

    private void switchToEditOfGoodsType(Integer selectedId, int typeId)
    {
        switch(typeId)
        {
            case 1:
                contentSwitcher.switchToComputerEditWindow(selectedId);
                break;
            case 2:
                contentSwitcher.switchToFoodEditWindow(selectedId);
                break;
            case 3:
                contentSwitcher.switchToMonitorEditWindow(selectedId);
                break;
            case 4:
                contentSwitcher.switchToSmartphoneEditWindow(selectedId);
                break;
        }
    }
}
